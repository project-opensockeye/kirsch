
#include <stdint.h>
#include <_timer.h>

#ifndef TIMER_H
#define TIMER_H

extern struct set_timer_data set_timer_data;

/**
 * Knowing the size of C functions or Assembly routines at compile-
 * or even link time is either dark magic or impossible.
 * 
 * Capabilities *require* a size to be effective, so we generously 
 * estimate the size of the set_timer function here.
 */
#define set_timer_size 512

/**
 * Sets the primary timer interrupt for `offset` units into the future.
 * 
 * \param offset The offset to add to the timer
 */
void set_timer(int offset);

#endif
