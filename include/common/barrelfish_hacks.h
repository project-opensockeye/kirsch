#include <stdint.h>
#include <inttypes.h>

// Hack to get around not.. being Barrelfish
typedef uint64_t lpaddr_t;
typedef uint64_t lvaddr_t;
typedef uint64_t genvaddr_t;

#define local_phys_to_mem(x) x

#define PRIx16 "x"
#define PRIx8 "x"

#define my_core_id 0
