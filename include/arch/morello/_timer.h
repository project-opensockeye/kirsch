
#ifndef _TIMER_H
#define _TIMER_H

#include <cheri/cheri_interface.h>

struct set_timer_data {
	volatile capability placeholder;
} __attribute__((packed)) __attribute__((aligned(16)));

#endif
