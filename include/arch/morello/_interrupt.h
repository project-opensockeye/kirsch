
#ifndef _INTERRUPT_H
#define _INTERRUPT_H

#include <stddef.h>
#include <stdint.h>
#include <mackerel_support/mackerel.h>
#include <mackerels/sysreg/sysreg.h>
#include <mackerels/armv8_dev.h>
#include <mackerels/gicd_dev.h>
#include <mackerels/gicr_dev.h>

/**
 * Prints the full status of the GIC.
 * 
 * If a pointer passed to this function is NULL, then the status
 * of that GIC subsystem is not printed out.
 * 
 * \param gic_redist Pointer to an initialized Mackerel gicr_t struct or NULL
 * \param gic_dist Pointer to an initialized Mackerel gicd_t struct or NULL
 */
void print_gic_status(gicr_t *gic_redist, gicd_t *gic_dist);

/**
 * Initializes the GIC on ARM Morello for interrupts.
 * 
 * This must be run *before* running cpu_init!
 *
 * \param vector_base_address Capability to the interrupt vector table
 * \param gic_dist Initialized Mackerel gicd_t struct
 * \param gic_redist Initialized Mackerel gicr_t struct
 * \param armv8 Initialized Mackerel armv8_t struct
 */
void gic_init(capability vector_base_address, gicd_t *gic_dist,
	      gicr_t *gic_redist, armv8_t *armv8);

/**
 * Initializes the CPU for interrupts.
 * 
 * This must be run *after* running gic_init!
 *
 * \param vector_base_address Capability to the interrupt vector table
 * \param gic_dist Initialized Mackerel gicd_t struct
 * \param gic_redist Initialized Mackerel gicr_t struct
 * \param armv8 Initialized Mackerel armv8_t struct
 */
void cpu_init(capability vector_base_address, gicd_t *gic_dist,
	      gicr_t *gic_redist, armv8_t *armv8);

/**
 * Initializes the primary timer CNTHP.
 * 
 * This must be run *after* running cpu_init! 
 *
 * \param armv8 Initialized Mackerel armv8_t struct
 * \param gic_redist Initialized Mackerel gicr_t struct
 */
void timer_init(armv8_t *armv8, gicr_t *gic_redist);

/**
 * Enables the primary timer CNTHP.
 * 
 * This must be run *after* running timer_init! 
 *
 * \param gic_redist Initialized Mackerel gicr_t struct
 */
void timer_enable(gicr_t *gic_redist);

#endif
