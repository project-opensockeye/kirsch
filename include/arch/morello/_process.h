
#ifndef _PROCESS_ARCH_H
#define _PROCESS_ARCH_H

struct process_registers {
	capability cs0;
} __attribute__((packed)) __attribute__((aligned(16)));

#endif
