
#include <stdint.h>
#include <stddef.h>
#include <sys/param.h>
#include <elf.h>

#ifndef _ARCH_BOOTSTRAP_H
#define _ARCH_BOOTSTRAP_H

void arch_bootstrap(const Elf64_Ehdr *elf, const Elf64_Dyn *elf_dyn);

#endif
