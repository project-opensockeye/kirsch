
#ifndef _TIMER_H
#define _TIMER_H

#include <cheri/cheri_interface.h>

struct set_timer_data {
	volatile capability mtime_cap;
	volatile capability mtimecmp_cap;

} __attribute__((packed)) __attribute__((aligned(16)));

#endif
