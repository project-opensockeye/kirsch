
#include <cheri/cheri_helpers.h>

#define csr_write(reg, val) __asm__ volatile("csrw " #reg ", %0" ::"rK"(val))

#define csr_write_dyn(reg, val) __asm__ volatile("csrw " #reg ", %0" ::"i"(val))

#define csr_write_imm(reg, val) \
	__asm__ volatile("csrwi " #reg ", " #val ::"rK"(val))

#define csr_write_dyn_imm(reg, val) \
	__asm__ volatile("csrwi %0, %1" : "i"(reg) : "i"(val))

#define csr_write_skip(reg, val)                                             \
	__asm__ volatile(".align 4\n"                                        \
			 "auipcc ct3, 0\n"                                   \
			 "cincoffset ct3, ct3, 0x10\n"                       \
			 "csrw mtvec, t3 \n"                                 \
			 "csrwi " #reg ", " #val "                       \n" \
			 "1: add t0, t0, 0 \n"                               \
			 ".align 2\n"                                        \
			 :                                                   \
			 : "rK"(val)                                         \
			 : "ct3", "t3")

#define csr_write_skip_reg(reg, val)                                  \
	__asm__ volatile(".align 4\n"                                 \
			 "auipcc ct3, 0\n"                            \
			 "cincoffset ct3, ct3, 0x10\n"                \
			 "csrw mtvec, t3 \n"                          \
			 "csrw " #reg ", %0                       \n" \
			 "add t0, t0, 0 \n"                           \
			 ".align 2\n"                                 \
			 :                                            \
			 : "r"(val)                                   \
			 : "ct3", "t3")

#define csr_write_skip_imm(reg, val)                            \
	__asm__ volatile(".align 4\n"                           \
			 "auipcc ct3, 0\n"                      \
			 "cincoffset ct3, ct3, 0x10\n"          \
			 "csrw mtvec, t3 \n"                    \
			 "csrw %0, %1                       \n" \
			 "1: add t0, t0, 0 \n"                  \
			 ".align 2\n"                           \
			 :                                      \
			 : "i"(reg), "i"(val)                   \
			 : "ct3", "t3")

#define csr_read(reg) csr_read_##reg()

inline unsigned long __attribute__((always_inline)) csr_read_mepc(void)
{
	unsigned long val;
	__asm __volatile("csrr %0, mepc" : "=r"(val));
	return val;
}
inline unsigned long __attribute__((always_inline)) csr_read_mtval(void)
{
	unsigned long val;
	__asm __volatile("csrr %0, mtval" : "=r"(val));
	return val;
}
inline unsigned long __attribute__((always_inline)) csr_read_mstatus(void)
{
	unsigned long val;
	__asm __volatile("csrr %0, mstatus" : "=r"(val));
	return val;
}
