#include <cheri/cheri_interface.h>

#ifndef _PROCESS_ARCH_H
#define _PROCESS_ARCH_H

struct process_registers {
	capability cra;
	capability ct0;
	capability ct1;
	capability ct2;
	capability cs0;
	capability cs1;
	capability ca0;
	capability ca1;
	capability ca2;
	capability ca3;
	capability ca4;
	capability ca5;
	capability ca6;
	capability ca7;
	capability cs2;
	capability cs3;
	capability cs4;
	capability cs5;
	capability cs6;
	capability cs7;
	capability cs8;
	capability cs9;
	capability cs10;
	capability cs11;
	capability ct3;
	capability ct4;
	capability ct5;
	capability ct6;

} __attribute__((packed)) __attribute__((aligned(16)));

#define process_registers_save(addr)                      \
	({                                                \
		cheri_cap_write(cra, 0x0, (addr)->cra);   \
		cheri_cap_write(ct0, 0x0, (addr)->ct0);   \
		cheri_cap_write(ct1, 0x0, (addr)->ct1);   \
		cheri_cap_write(ct2, 0x0, (addr)->ct2);   \
		cheri_cap_write(cs0, 0x0, (addr)->cs0);   \
		cheri_cap_write(cs1, 0x0, (addr)->cs1);   \
		cheri_cap_write(ca0, 0x0, (addr)->ca0);   \
		cheri_cap_write(ca1, 0x0, (addr)->ca1);   \
		cheri_cap_write(ca2, 0x0, (addr)->ca2);   \
		cheri_cap_write(ca3, 0x0, (addr)->ca3);   \
		cheri_cap_write(ca4, 0x0, (addr)->ca4);   \
		cheri_cap_write(ca5, 0x0, (addr)->ca5);   \
		cheri_cap_write(ca6, 0x0, (addr)->ca6);   \
		cheri_cap_write(ca7, 0x0, (addr)->ca7);   \
		cheri_cap_write(cs2, 0x0, (addr)->cs2);   \
		cheri_cap_write(cs3, 0x0, (addr)->cs3);   \
		cheri_cap_write(cs4, 0x0, (addr)->cs4);   \
		cheri_cap_write(cs5, 0x0, (addr)->cs5);   \
		cheri_cap_write(cs6, 0x0, (addr)->cs6);   \
		cheri_cap_write(cs7, 0x0, (addr)->cs7);   \
		cheri_cap_write(cs8, 0x0, (addr)->cs8);   \
		cheri_cap_write(cs9, 0x0, (addr)->cs9);   \
		cheri_cap_write(cs10, 0x0, (addr)->cs10); \
		cheri_cap_write(cs11, 0x0, (addr)->cs11); \
		cheri_cap_write(ct3, 0x0, (addr)->ct3);   \
		cheri_cap_write(ct4, 0x0, (addr)->ct4);   \
		cheri_cap_write(ct5, 0x0, (addr)->ct5);   \
		cheri_cap_write(ct6, 0x0, (addr)->ct6);   \
	})

#define process_registers_load(addr)                     \
	({                                               \
		cheri_cap_read(cra, 0x0, (addr)->cra);   \
		cheri_cap_read(ct0, 0x0, (addr)->ct0);   \
		cheri_cap_read(ct1, 0x0, (addr)->ct1);   \
		cheri_cap_read(ct2, 0x0, (addr)->ct2);   \
		cheri_cap_read(cs0, 0x0, (addr)->cs0);   \
		cheri_cap_read(cs1, 0x0, (addr)->cs1);   \
		cheri_cap_read(ca0, 0x0, (addr)->ca0);   \
		cheri_cap_read(ca1, 0x0, (addr)->ca1);   \
		cheri_cap_read(ca2, 0x0, (addr)->ca2);   \
		cheri_cap_read(ca3, 0x0, (addr)->ca3);   \
		cheri_cap_read(ca4, 0x0, (addr)->ca4);   \
		cheri_cap_read(ca5, 0x0, (addr)->ca5);   \
		cheri_cap_read(ca6, 0x0, (addr)->ca6);   \
		cheri_cap_read(ca7, 0x0, (addr)->ca7);   \
		cheri_cap_read(cs2, 0x0, (addr)->cs2);   \
		cheri_cap_read(cs3, 0x0, (addr)->cs3);   \
		cheri_cap_read(cs4, 0x0, (addr)->cs4);   \
		cheri_cap_read(cs5, 0x0, (addr)->cs5);   \
		cheri_cap_read(cs6, 0x0, (addr)->cs6);   \
		cheri_cap_read(cs7, 0x0, (addr)->cs7);   \
		cheri_cap_read(cs8, 0x0, (addr)->cs8);   \
		cheri_cap_read(cs9, 0x0, (addr)->cs9);   \
		cheri_cap_read(cs10, 0x0, (addr)->cs10); \
		cheri_cap_read(cs11, 0x0, (addr)->cs11); \
		cheri_cap_read(ct3, 0x0, (addr)->ct3);   \
		cheri_cap_read(ct4, 0x0, (addr)->ct4);   \
		cheri_cap_read(ct5, 0x0, (addr)->ct5);   \
		cheri_cap_read(ct6, 0x0, (addr)->ct6);   \
	})

#endif
