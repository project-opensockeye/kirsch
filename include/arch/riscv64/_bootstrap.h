
#include <stdint.h>
#include <stddef.h>
#include <sys/param.h>
#include <elf.h>

#ifndef _ARCH_BOOTSTRAP_H
#define _ARCH_BOOTSTRAP_H

/**
 * RISC-V boostrapping.
 * 
 * \param elf_address The address of the kernel ELF
 * \param cap_relocs_start_addr Address of the __cap_relocs section in the kernel ELF
 * \param cap_relocs_addr_end End address of the __cap_relocs section in the kernel ELF
 */
void arch_bootstrap(const size_t elf_address, size_t cap_relocs_addr_start,
		    size_t cap_relocs_addr_end);

#endif
