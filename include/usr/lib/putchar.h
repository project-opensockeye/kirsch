
#include "proc/print_interface.h"

#ifndef _PUTCHAR_H
#define _PUTCHAR_H

#define _putchar(c) print_putchar(c);

#endif
