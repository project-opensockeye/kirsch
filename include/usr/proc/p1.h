#include <stdint.h>

#ifndef _P1_H
#define _P1_H

void *__capability ccap_p1_send_msg_handler;
void *__capability dcap_p1_send_msg_handler;

struct p1_send_msg_handler_data {
	uint64_t empty;
};

struct p1_send_msg_handler_data p1_send_msg_handler_data;

void p1_send_msg_handler(uint64_t msg);

#endif
