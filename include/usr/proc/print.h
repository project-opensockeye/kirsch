#include <stdint.h>

#ifndef _PRINT_H
#define _PRINT_H

void *__capability ccap_print_putchar;
void *__capability dcap_print_putchar;

struct print_putchar_handler_data {
	uint64_t empty;
};
struct print_putchar_handler_data print_putchar_handler_data;

void print_putchar_handler(char msg);

#endif
