
#include "cheri_interface.h"

#ifndef _PRINT_INTERFACE_H
#define _PRINT_INTERFACE_H

extern void *__capability __attribute__((weak)) ccap_print_putchar;
extern void *__capability __attribute__((weak)) dcap_print_putchar;

void print_putchar(char msg);

#endif
