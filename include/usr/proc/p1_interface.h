
#include "cheri_interface.h"
#include <stdint.h>
#include "printf.h"

#ifndef _P1_INTERFACE
#define _P1_INTERFACE

extern void *__capability __attribute__((weak)) ccap_p1_send_msg_handler;
extern void *__capability __attribute__((weak)) dcap_p1_send_msg_handler;

void send_msg(uint64_t msg)
{
	printf("send_msg\n");

	cheri_cinvoke_asm_1(ccap_p1_send_msg_handler, dcap_p1_send_msg_handler,
			    msg);
}

#endif
