#include <stdint.h>
#include <process.h>
#include <_trap.h>

#ifndef _TRAP_H
#define _TRAP_H

#undef TIMER_INTERRUPT_MASK
#define TIMER_INTERRUPT_MASK _TIMER_INTERRUPT_MASK

struct trap_vector_data {
	uint64_t stack[32];

	void *__capability set_timer;
	void *__capability set_timer_data;
	struct process_descriptor *__capability cur_proc;

	uint64_t cur_proc_index;
	uint64_t num_proc;
	struct process_descriptor processes[32];

} __attribute__((aligned(16)));

extern struct trap_vector_data global_trap_vector_data;

/**
 * Architecture-independent trap vector.
 */
void common_trap_vector(void);

/**
 * Returns from a trap.
 */
extern void trap_return(void);

/**
 * Returns the reason for an interrupt.
 *
 * \return Architecture-dependent integer descriping interrupt reason
 */
uint64_t interrupt_reason_get(void);

/**
 * Enters the generic trap handler 
 */
void __attribute__((naked)) trap_vector_entry(void);

/**
 * Writes the trap return program counter
 */
void trap_return_pc_write(capability return_ptr);

/**
 * Returns the trap return program counter.
 * 
 * \return capability to the return program counter
 */
capability trap_return_pc_read(void);

/**
 * Returns a pointer to the trap vector data struct.
 * 
 * \return capability to the global trap vector data
 */
struct trap_vector_data *trap_vector_data_load_from_context(void);

#endif
