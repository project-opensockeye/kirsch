
/**
 * Runs a cool demo. Requires running alongside seL4 on a Morello FVP.
 */
void __attribute__((noreturn)) demo(void);
