
#ifndef ELF_RELOC_SELF_H
#define ELF_RELOC_SELF_H

#include <elf.h>

/**
 * Initializes CHERI capabilities using the native ELF dynamic section.
 * 
 * Takes pointers to the dynamic section of an ELF, and the ELF itself,
 * and then performs capability relocations as described in the dynamic section.
 * 
 * Currently, only the Morello LLVM toolchain supports native ELF caprelocs.
 * The alternative, implemented in the RISC-V LLVM toolchain is a separate
 * __cap_relocs section in the ELF. See crt_init_globals.c/.h.
 * 
 * The implementation of this function is taken from
 * https://github.com/CTSRD-CHERI/cheribsd/blob/9322a79dea58f31151d2345eeca79d398cbb8e63/sys/arm64/arm64/elf_machdep.c
 * 
 * \param dynp Capability to the dynamic section
 * \param elf Capability to the ELF header
 * \param data_cap Capability to the cap that data capabilities are derived from.
 * \param code_cap Capability to the cap that core/executable capabilities are derived from.
 */
void __nosanitizecoverage novm_elf_reloc_self(const Elf64_Dyn *dynp,
					      const Elf_Ehdr *elf,
					      void *data_cap,
					      const void *code_cap);

#endif
