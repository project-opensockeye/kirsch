
#include <elf.h>
#include <stdint.h>
#include <stddef.h>

/**
 * The format of a single capabiilty relocation in the __cap_relocs section 
 */
struct capreloc {
	uint64_t capability_location;
	uint64_t object;
	uint64_t offset;
	uint64_t size;
	uint64_t permissions;
};

/**
 * \brief Initializes CHERI capabilities using the legacy 
 * __cap_relocs ELF relocation section. 
 * 
 * While the Morello LLVM toolchain can already handle the proper CHERI capability 
 * initialization variant using native ELF relocations, the RISC-V
 * LLVM toolchain only supports this legacy implementation using the custom
 * ELF section "__cap_relocs". 
 * 
 * The implementation to this function is taken from
 * https://github.com/CTSRD-CHERI/llvm-project/blob/master/lld/test/ELF/cheri/__cap_relocs/Inputs/crt_init_globals.c
 * 
 * \param elf Capability to the ELF header
 * \param __start___cap_relocs Starting address of the __cap_relocs ELF section
 * \param __stop___cap_relocs End address of the __cap_relocs ELF section 
 */
void crt_init_globals(const Elf_Ehdr *elf,
		      struct capreloc *__start___cap_relocs,
		      struct capreloc *__stop___cap_relocs);
