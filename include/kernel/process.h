#include <cheri/cheri_interface.h>
#include <_process.h>
#include <stdbool.h>
#include <stdint.h>

#ifndef _PROCESS_H
#define _PROCESS_H

struct process_descriptor {
	bool first_start;
	capability pcc;
	capability csp;
	struct process_registers registers;
	uint8_t stack[12800];
} __attribute__((aligned(16)));

/**
 * Initializes a process descriptor.
 *
 * \param pd Capability to the process descriptor to initialize
 * \param start_addr Address of the first process instruction
 * \param data_addr Address of data to pass to the process
 */
void process_init(struct process_descriptor *pd, uint64_t start_addr,
		  uint64_t data_addr);

/**
 * Initializes the current stack.
 * 
 * \param csp_target Capability to the stack to initialize
 */
void process_stack_init(capability *csp_target);

#endif
