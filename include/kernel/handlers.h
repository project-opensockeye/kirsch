
#ifndef HANDLERS_H
#define HANDLERS_H

/**
 * Handle a timer interrupt. Schedule next process.
 * 
 * Implements very trivial round-robin scheduling.
 */
void __attribute__((noinline)) timer_interrupt_handler(void);

#endif
