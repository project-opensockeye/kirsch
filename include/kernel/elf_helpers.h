#include <stddef.h>
#include <stdint.h>
#include <elf.h>

/**
 * Saves a pointer to the kernel ELF.
 * 
 * Initialized once at boot.
 * 
 * \param elf Capability to the kernel ELF header
 */
void elf_helpers_state_init(const Elf_Ehdr *elf);

/**
 * Returns a pointer to the kernel ELF.
 * 
 * Useful in trap handlers to look up faulting virtual addresses.
 * (see elf_offset_to_va()).
 */
const Elf_Ehdr *elf_current_get(void);

/**
 * Translates an ELF virtual address into an ELF offset.
 * 
 * The virtual address is an *ELF virtual address*, i.e.
 * describes how the ELF imagined it should have been mapped.
 * Cheriette does *not* map ELFs at the virtual addresses they
 * expect to be mapped at.
 * 
 * Instead, when relocating ELF files, ELF virtual addresses 
 * are turned into ELF offsets, which can then be turned into 
 * usable pointers. 
 * 
 * Traps if the VA cannot be translated.
 * 
 * \param elf Capability to the kernel ELF header
 * \param elf_va VA in the kernel ELF to translate to an ELF offset 
 */
size_t elf_va_to_offset(const Elf_Ehdr *elf, size_t elf_va);

/**
 * Translates an ELF offset into an ELF virtual address.
 * 
 * The virtual address is an *ELF virtual address*, i.e.
 * describes how the ELF imagined it should have been mapped.
 * Cheriette does *not* map ELFs at the virtual addresses they
 * expect to be mapped at.
 * 
 * This function is stil useful for debugging.
 * When taking a fault, the kernel can return the ELF virtual address
 * of the fault, which allows looking up the faulting instruction
 * in the disassembly.
 * 
 * Traps if the offset cannot be translated.
 * 
 * \param elf Capability to the kernel ELF header
 * \param elf_offset Offset into the kernel ELF to translate to an ELF VA
 */
size_t elf_offset_to_va(const Elf_Ehdr *elf, size_t elf_offset);
