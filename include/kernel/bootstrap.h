
#include <elf.h>
#include <_bootstrap.h>

#ifndef _BOOTSTRAP_H
#define _BOOTSTRAP_H

/**
 * Architecture-independent bootstrapping.
 * 
 * \param elf Capability to the kernel ELF header
 */
void bootstrap(const Elf_Ehdr *elf);

#endif
