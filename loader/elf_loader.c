#include <elf.h>
#include <stdint.h>
#include <stdbool.h>
#include <standalone/printf.h>
#include <system_interface/system_interface.h>

#include "pt.h"

// Helper to offset ptr by off
#define OFF(ptr, off) ptr = ptr + off

/**
 * Loads and initializes a payload kernel ELF, and then jumps to it.
 * 
 * @param elf The address to the payload ELF.
 */
void __attribute__((noreturn)) __elf_loader(volatile Elf64_Ehdr *elf)
{
	putchar_glas_state_set(false);
	putchar_init();

	// Testing putchar
	putchar('x');

	// Testing printf
	printf("[Cheriette/Loader] Hello!\n");

	printf("[Cheriette/Loader] Loading ELF %x\n", elf);

	printf("[Cheriette/Loader] Loading GLAS page tables...\n");
	activate_page_tables();
	putchar_glas_state_set(true);
	printf("[Cheriette/Loader] GLAS page tables loaded!\n");

	if (!IS_ELF(*elf)) {
		printf("[Cheriette/Loader] ELF is not ELF!\n", elf);
		system_terminate();
	}
	printf("[Cheriette/Loader] ELFness confirmed!\n", elf);

	printf("[Cheriette/Loader] Correcting section header addresses in kernel ELF...\n");
	for (uint64_t i = 0; i < elf->e_shnum; i++) {
		volatile Elf64_Shdr *shdr =
			(volatile Elf64_Shdr *)(((uint8_t *)elf) +
						elf->e_shoff +
						(i * elf->e_shentsize));

		shdr->sh_addr = shdr->sh_offset + (uint64_t)elf;

		printf("[Cheriette/Loader] Correcting section header address: %x\n",
		       shdr->sh_addr);
	}

	printf("[Cheriette/Loader] Correcting program header physical addresses in kernel ELF...\n");
	uint64_t lowest_program_header_offset = 0xFFFFFFFFFFFFFFFF;
	for (uint64_t i = 0; i < elf->e_phnum; i++) {
		volatile Elf64_Phdr *phdr =
			(volatile Elf64_Phdr *)(((uint8_t *)elf) +
						elf->e_phoff +
						(i * elf->e_phentsize));

		// Only correct the physical header address
		// because we need the virtual address later
		// to convert between ELF virtual addresses
		// and ELF offsets
		phdr->p_paddr = phdr->p_offset + (uint64_t)elf;

		if (phdr->p_offset != 0x0 &&
		    phdr->p_offset < lowest_program_header_offset) {
			lowest_program_header_offset = phdr->p_offset;
		}

		printf("[Cheriette/Loader] Correcting program header address: %x\n",
		       phdr->p_paddr);
	}
	if (lowest_program_header_offset == 0xFFFFFFFFFFFFFFFF) {
		printf("ERROR: No program headers found.\n");
		system_terminate();
	}

	// Assume that e_entry is in the program header with the lowest offset
	// And offset e_entry by the elf_address and the address of the lowest
	// program header
	OFF(elf->e_entry, lowest_program_header_offset + (size_t)elf);

	printf("[Cheriette/Loader] Jumping to elf e_entry @ %p\n",
	       elf->e_entry);
	printf("[Cheriette/Loader] First instruction @ elf_entry: %x\n",
	       *(uint64_t *)elf->e_entry);

	size_t elf_base = (size_t)elf;
	printf("[Cheriette/Loader]  start.S(%p)\n", elf_base);

	// Jump to ELF entry
	((void (*)(size_t elf_base))elf->e_entry)(elf_base);

	// In case we DO reach this point, for whatever unholy reason.
	// This also hints to the compiler that this function never returns.
	system_terminate();
}
