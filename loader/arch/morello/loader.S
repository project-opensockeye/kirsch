#include <cheri/cheri_helpers.S>

.section .text.init
.globl __start;
.globl __elf_loader;
.globl __elf_start;
.p2align 2

/**
 * Start function for the loader.
 *
 * Sets up minimal environment (disable FP traps, set up stack)
 * and jumps into generic C at its earliest convenience.
 *
 * Fashioned after the init.o file in the Morello LLVM Bare-Metal toolchain.
 */ 
.align 2
.type __start,@function
__start:

    // Clear registers to dispose of UNKNOWN register state
    CLEAR_REGISTERS

    // Load the address of the Morello UART
    // adrp zeroes out the lower 12 bits
    adrp x8, plat_arm_boot_uart_base
    // :lo12 is a relocation that takes the lower 12 bits of the address
    // We offset our load by those lower 12 bits
    ldr x9, [x8, :lo12:plat_arm_boot_uart_base]

    // Print 'h'
    mov w10, #104
    strb w10, [x9]

    // Print 'e'
    mov w10, #101
    strb w10, [x9]

    // Print 'l'
    mov w10, #108
    strb w10, [x9]

    // Print 'l'
    mov w10, #108
    strb w10, [x9]

    // Print 'o'
    mov w10, #111
    strb w10, [x9]

    // Disable traps for floating point
    // instructions and register accesses
	mrs x1, CPTR_EL2
    orr x1, x1, #(0x1<<20) // CPTR_El2.FPEN = 1
    msr CPTR_El2, x1

    // Set up stack
    adr x9, __temporary_stack_bot 
    sub sp, x9, 16

    // Load address of payload ELF
    // This is the argument of __elf_loader
    ldr x0, =__elf_start

    // __elf_loader(elf_ptr)
    bl __elf_loader

// Calculate the size of the __start function  
.Lfunc___start_end:
    .size __start, .Lfunc___start_end - __start

// The address of the Morello Pl011 UART.
// Saved as data because loading it as immediates
// caused problems.
.type plat_arm_boot_uart_base,@object
.data
.globl plat_arm_boot_uart_base
.p2align 3 
plat_arm_boot_uart_base: 
    .xword 0x2A400000
.size plat_arm_boot_uart_base, 8
