
#include "proc/print_interface.h"
#include "cheri_interface.h"

void print_putchar(char msg)
{
	cheri_cinvoke_asm_1(ccap_print_putchar, dcap_print_putchar, msg);
}
