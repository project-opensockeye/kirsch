
#include <stdint.h>
#include "proc/print.h"
#include "cheri_interface.h"

void init(void *__capability sealing_cap)
{
	ccap_print_putchar = (void *)&print_putchar_handler;
	ccap_print_putchar = cheri_seal(ccap_print_putchar, sealing_cap);

	dcap_print_putchar = (void *)&print_putchar_handler_data;
	dcap_print_putchar = cheri_seal(dcap_print_putchar, sealing_cap);
}

void print_putchar_handler(char msg)
{
}
