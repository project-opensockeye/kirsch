
#include <stdint.h>
#include "proc/p1.h"
#include "printf.h"
#include "cheri_interface.h"

void init(void *__capability sealing_cap)
{
	ccap_p1_send_msg_handler = (void *)&p1_send_msg_handler;
	ccap_p1_send_msg_handler =
		cheri_seal(ccap_p1_send_msg_handler, sealing_cap);

	dcap_p1_send_msg_handler = (void *)&p1_send_msg_handler_data;
	dcap_p1_send_msg_handler =
		cheri_seal(dcap_p1_send_msg_handler, sealing_cap);
}

void p1_send_msg_handler(uint64_t msg)
{
	printf("p1_send_msg_handler\n");
}
