#include "bb.s"

.option capmode

.globl processes, processes_end

processes:
    // FIXME: this gives proc1 a PC capability that covers proc2 and the data (admittedly with no useful data permissions)
    SAVE_BLOCK proc1, end1, p1_data, p1_data_end
    SAVE_BLOCK proc2, proc2_end, p2_data, p2_data_end
processes_end:

// These are invoked with c1 pointing to their data area.
proc1:
    li t0, 0
    // Prepare for real code (that is, C)
    // Give it a stack
    cllc csp, p1_data_end
    csetaddr csp, c1, sp
    // In C-land, c1 is cra, oops, use c3 (cgp) instead
    cmove c3, c1
spin1:
    mv t2, t1
    mv t1, t0
    // addi t0, t0, 1
    ccall add1
    csd t2, 0(c3)
    j spin1
proc1_end:

.align 3
p1_data:
    .dword 0
p1_stack:
    .dword 0, 0, 0, 0, 0, 0, 0, 0
p1_data_end:

proc2:
    li t0, 0x8000000000000000
spin2:
    mv t2, t1
    mv t1, t0
    addi t0, t0, -1
    csd t2, 0(c1)
    j spin2
proc2_end:

.align 3
p2_data:
    .dword 0
p2_data_end:
