/*
 * Copyright (c) 2017 ETH Zurich. All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, CAB F.78, Universitaetstr. 6, CH-8092 Zurich,
 * Attn: Systems Group.
 */

/*
 * NOTE: This file has been automatically generated based on the XML files
 * provided by ARM.
 *
 * Download from:
 * https://developer.arm.com/products/architecture/a-profile/exploration-tools
 *
 * Based on version: SysReg_v82A_xml-00bet3.1.tar.gz
 */

/* Source: ext-gicd_clrspi_nsr.xml */
/* Source: ext-gicd_clrspi_sr.xml */
/* Source: ext-gicd_cpendsgirn.xml */
/* Source: ext-gicd_ctlr.xml */
/* Source: ext-gicd_icactivern.xml */
/* Source: ext-gicd_icactiverne.xml */
/* Source: ext-gicd_icenablern.xml */
/* Source: ext-gicd_icenablerne.xml */
/* Source: ext-gicd_icfgrn.xml */
/* Source: ext-gicd_icfgrne.xml */
/* Source: ext-gicd_icpendrn.xml */
/* Source: ext-gicd_icpendrne.xml */
/* Source: ext-gicd_igrouprn.xml */
/* Source: ext-gicd_igrouprne.xml */
/* Source: ext-gicd_igrpmodrn.xml */
/* Source: ext-gicd_igrpmodrne.xml */
/* Source: ext-gicd_iidr.xml */
/* Source: ext-gicd_inmirn.xml */
/* Source: ext-gicd_inmirne.xml */
/* Source: ext-gicd_ipriorityrn.xml */
/* Source: ext-gicd_ipriorityrne.xml */
/* Source: ext-gicd_isactivern.xml */
/* Source: ext-gicd_isactiverne.xml */
/* Source: ext-gicd_isenablern.xml */
/* Source: ext-gicd_isenablerne.xml */
/* Source: ext-gicd_ispendrn.xml */
/* Source: ext-gicd_ispendrne.xml */
/* Source: ext-gicd_itargetsrn.xml */
/* Source: ext-gicd_nsacrn.xml */
/* Source: ext-gicd_nsacrne.xml */
/* Source: ext-gicd_setspi_nsr.xml */
/* Source: ext-gicd_setspi_sr.xml */
/* Source: ext-gicd_sgir.xml */
/* Source: ext-gicd_spendsgirn.xml */
/* Source: ext-gicd_statusr.xml */
/* Source: ext-gicd_typer.xml */
/* Source: ext-gicd_typer2.xml */
