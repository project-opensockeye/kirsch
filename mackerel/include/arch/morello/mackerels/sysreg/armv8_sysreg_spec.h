/*
 * Copyright (c) 2017 ETH Zurich. All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, CAB F.78, Universitaetstr. 6, CH-8092 Zurich,
 * Attn: Systems Group.
 */

/*
 * NOTE: This file has been automatically generated based on the XML files
 * provided by ARM.
 *
 * Download from:
 * https://developer.arm.com/products/architecture/a-profile/exploration-tools
 *
 * Based on version: SysReg_v82A_xml-00bet3.1.tar.gz
 */

/* Source: AArch64-accdata_el1.xml */
ARMV8_SYSREG_RW(ACCDATA_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-actlr_el1.xml */
ARMV8_SYSREG_RW(ACTLR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-actlr_el2.xml */
ARMV8_SYSREG_RW(ACTLR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-actlr_el3.xml */
ARMV8_SYSREG_RW(ACTLR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-afsr0_el1.xml */
ARMV8_SYSREG_RW(AFSR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-afsr0_el2.xml */
ARMV8_SYSREG_RW(AFSR0_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-afsr0_el3.xml */
ARMV8_SYSREG_RW(AFSR0_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-afsr1_el1.xml */
ARMV8_SYSREG_RW(AFSR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-afsr1_el2.xml */
ARMV8_SYSREG_RW(AFSR1_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-afsr1_el3.xml */
ARMV8_SYSREG_RW(AFSR1_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-aidr_el1.xml */
ARMV8_SYSREG_RW(AIDR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-allint.xml */
ARMV8_SYSREG_RW(ALLINT, S3_3_C3_C3_3, 64)
/* Source: AArch64-amair2_el1.xml */
ARMV8_SYSREG_RW(AMAIR2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-amair2_el2.xml */
ARMV8_SYSREG_RW(AMAIR2_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-amair2_el3.xml */
ARMV8_SYSREG_RW(AMAIR2_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-amair_el1.xml */
ARMV8_SYSREG_RW(AMAIR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-amair_el2.xml */
ARMV8_SYSREG_RW(AMAIR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-amair_el3.xml */
ARMV8_SYSREG_RW(AMAIR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-amuserenr_el0.xml */
ARMV8_SYSREG_RW(AMUSERENR_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-apgakeyhi_el1.xml */
ARMV8_SYSREG_RW(APGAKeyHi_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-apgakeylo_el1.xml */
ARMV8_SYSREG_RW(APGAKeyLo_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-apiakeyhi_el1.xml */
ARMV8_SYSREG_RW(APIAKeyHi_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-apiakeylo_el1.xml */
ARMV8_SYSREG_RW(APIAKeyLo_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-apibkeyhi_el1.xml */
ARMV8_SYSREG_RW(APIBKeyHi_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-apibkeylo_el1.xml */
ARMV8_SYSREG_RW(APIBKeyLo_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-ccsidr2_el1.xml */
ARMV8_SYSREG_RW(CCSIDR2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-ccsidr_el1.xml */
ARMV8_SYSREG_RW(CCSIDR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-clidr_el1.xml */
ARMV8_SYSREG_RW(CLIDR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntfrq_el0.xml */
ARMV8_SYSREG_RW(CNTFRQ_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-cnthctl_el2.xml */
ARMV8_SYSREG_RW(CNTHCTL_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cnthp_ctl_el2.xml */
ARMV8_SYSREG_RW(CNTHP_CTL_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cnthp_cval_el2.xml */
ARMV8_SYSREG_RW(CNTHP_CVAL_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cnthp_tval_el2.xml */
ARMV8_SYSREG_RW(CNTHP_TVAL_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cnthps_ctl_el2.xml */
ARMV8_SYSREG_RW(CNTHPS_CTL_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cnthps_cval_el2.xml */
ARMV8_SYSREG_RW(CNTHPS_CVAL_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cnthps_tval_el2.xml */
ARMV8_SYSREG_RW(CNTHPS_TVAL_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cnthv_ctl_el2.xml */
ARMV8_SYSREG_RW(CNTHV_CTL_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cnthv_cval_el2.xml */
ARMV8_SYSREG_RW(CNTHV_CVAL_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cnthv_tval_el2.xml */
ARMV8_SYSREG_RW(CNTHV_TVAL_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cnthvs_ctl_el2.xml */
ARMV8_SYSREG_RW(CNTHVS_CTL_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cnthvs_cval_el2.xml */
ARMV8_SYSREG_RW(CNTHVS_CVAL_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cnthvs_tval_el2.xml */
ARMV8_SYSREG_RW(CNTHVS_TVAL_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntkctl_el1.xml */
ARMV8_SYSREG_RW(CNTKCTL_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntp_ctl_el0.xml */
ARMV8_SYSREG_RW(CNTP_CTL_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntp_cval_el0.xml */
ARMV8_SYSREG_RW(CNTP_CVAL_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntp_tval_el0.xml */
ARMV8_SYSREG_RW(CNTP_TVAL_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntpct_el0.xml */
ARMV8_SYSREG_RW(CNTPCT_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntpctss_el0.xml */
ARMV8_SYSREG_RW(CNTPCTSS_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntpoff_el2.xml */
ARMV8_SYSREG_RW(CNTPOFF_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntps_ctl_el1.xml */
ARMV8_SYSREG_RW(CNTPS_CTL_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntps_cval_el1.xml */
ARMV8_SYSREG_RW(CNTPS_CVAL_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntps_tval_el1.xml */
ARMV8_SYSREG_RW(CNTPS_TVAL_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntv_ctl_el0.xml */
ARMV8_SYSREG_RW(CNTV_CTL_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntv_cval_el0.xml */
ARMV8_SYSREG_RW(CNTV_CVAL_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntv_tval_el0.xml */
ARMV8_SYSREG_RW(CNTV_TVAL_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntvct_el0.xml */
ARMV8_SYSREG_RW(CNTVCT_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntvctss_el0.xml */
ARMV8_SYSREG_RW(CNTVCTSS_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-cntvoff_el2.xml */
ARMV8_SYSREG_RW(CNTVOFF_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-contextidr_el1.xml */
ARMV8_SYSREG_RW(CONTEXTIDR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-contextidr_el2.xml */
ARMV8_SYSREG_RW(CONTEXTIDR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cpacr_el1.xml */
ARMV8_SYSREG_RW(CPACR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-cptr_el2.xml */
ARMV8_SYSREG_RW(CPTR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-cptr_el3.xml */
ARMV8_SYSREG_RW(CPTR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-csselr_el1.xml */
ARMV8_SYSREG_RW(CSSELR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-ctr_el0.xml */
ARMV8_SYSREG_RW(CTR_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-currentel.xml */
ARMV8_SYSREG_RW(CurrentEL, S3_3_C3_C3_3, 64)
/* Source: AArch64-dacr32_el2.xml */
ARMV8_SYSREG_RW(DACR32_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-daif.xml */
ARMV8_SYSREG_RW(DAIF, S3_3_C3_C3_3, 64)
/* Source: AArch64-dczid_el0.xml */
ARMV8_SYSREG_RW(DCZID_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-disr_el1.xml */
ARMV8_SYSREG_RW(DISR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-dit.xml */
ARMV8_SYSREG_RW(DIT, S3_3_C3_C3_3, 64)
/* Source: AArch64-dlr_el0.xml */
ARMV8_SYSREG_RW(DLR_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-dspsr_el0.xml */
ARMV8_SYSREG_RW(DSPSR_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-elr_el1.xml */
ARMV8_SYSREG_RW(ELR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-elr_el2.xml */
ARMV8_SYSREG_RW(ELR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-elr_el3.xml */
ARMV8_SYSREG_RW(ELR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-erridr_el1.xml */
ARMV8_SYSREG_RW(ERRIDR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-errselr_el1.xml */
ARMV8_SYSREG_RW(ERRSELR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-erxaddr_el1.xml */
ARMV8_SYSREG_RW(ERXADDR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-erxctlr_el1.xml */
ARMV8_SYSREG_RW(ERXCTLR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-erxfr_el1.xml */
ARMV8_SYSREG_RW(ERXFR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-erxgsr_el1.xml */
ARMV8_SYSREG_RW(ERXGSR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-erxmisc0_el1.xml */
ARMV8_SYSREG_RW(ERXMISC0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-erxmisc1_el1.xml */
ARMV8_SYSREG_RW(ERXMISC1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-erxmisc2_el1.xml */
ARMV8_SYSREG_RW(ERXMISC2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-erxmisc3_el1.xml */
ARMV8_SYSREG_RW(ERXMISC3_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-erxpfgcdn_el1.xml */
ARMV8_SYSREG_RW(ERXPFGCDN_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-erxpfgctl_el1.xml */
ARMV8_SYSREG_RW(ERXPFGCTL_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-erxpfgf_el1.xml */
ARMV8_SYSREG_RW(ERXPFGF_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-erxstatus_el1.xml */
ARMV8_SYSREG_RW(ERXSTATUS_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-esr_el1.xml */
ARMV8_SYSREG_RW(ESR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-esr_el2.xml */
ARMV8_SYSREG_RW(ESR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-esr_el3.xml */
ARMV8_SYSREG_RW(ESR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-far_el1.xml */
ARMV8_SYSREG_RW(FAR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-far_el2.xml */
ARMV8_SYSREG_RW(FAR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-far_el3.xml */
ARMV8_SYSREG_RW(FAR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-fgwte3_el3.xml */
ARMV8_SYSREG_RW(FGWTE3_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-fpcr.xml */
ARMV8_SYSREG_RW(FPCR, S3_3_C3_C3_3, 64)
/* Source: AArch64-fpexc32_el2.xml */
ARMV8_SYSREG_RW(FPEXC32_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-fpmr.xml */
ARMV8_SYSREG_RW(FPMR, S3_3_C3_C3_3, 64)
/* Source: AArch64-fpsr.xml */
ARMV8_SYSREG_RW(FPSR, S3_3_C3_C3_3, 64)
/* Source: AArch64-gcr_el1.xml */
ARMV8_SYSREG_RW(GCR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-gcscr_el1.xml */
ARMV8_SYSREG_RW(GCSCR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-gcscr_el2.xml */
ARMV8_SYSREG_RW(GCSCR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-gcscr_el3.xml */
ARMV8_SYSREG_RW(GCSCR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-gcscre0_el1.xml */
ARMV8_SYSREG_RW(GCSCRE0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-gcspr_el0.xml */
ARMV8_SYSREG_RW(GCSPR_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-gcspr_el1.xml */
ARMV8_SYSREG_RW(GCSPR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-gcspr_el2.xml */
ARMV8_SYSREG_RW(GCSPR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-gcspr_el3.xml */
ARMV8_SYSREG_RW(GCSPR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-gmid_el1.xml */
ARMV8_SYSREG_RW(GMID_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-gpccr_el3.xml */
ARMV8_SYSREG_RW(GPCCR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-gptbr_el3.xml */
ARMV8_SYSREG_RW(GPTBR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-hacdbsbr_el2.xml */
ARMV8_SYSREG_RW(HACDBSBR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hacdbscons_el2.xml */
ARMV8_SYSREG_RW(HACDBSCONS_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hacr_el2.xml */
ARMV8_SYSREG_RW(HACR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hcr_el2.xml */
ARMV8_SYSREG_RW(HCR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hcrx_el2.xml */
ARMV8_SYSREG_RW(HCRX_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hdbssbr_el2.xml */
ARMV8_SYSREG_RW(HDBSSBR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hdbssprod_el2.xml */
ARMV8_SYSREG_RW(HDBSSPROD_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hdfgrtr2_el2.xml */
ARMV8_SYSREG_RW(HDFGRTR2_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hdfgrtr_el2.xml */
ARMV8_SYSREG_RW(HDFGRTR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hdfgwtr2_el2.xml */
ARMV8_SYSREG_RW(HDFGWTR2_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hdfgwtr_el2.xml */
ARMV8_SYSREG_RW(HDFGWTR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hfgitr2_el2.xml */
ARMV8_SYSREG_RW(HFGITR2_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hfgitr_el2.xml */
ARMV8_SYSREG_RW(HFGITR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hfgrtr2_el2.xml */
ARMV8_SYSREG_RW(HFGRTR2_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hfgrtr_el2.xml */
ARMV8_SYSREG_RW(HFGRTR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hfgwtr2_el2.xml */
ARMV8_SYSREG_RW(HFGWTR2_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hfgwtr_el2.xml */
ARMV8_SYSREG_RW(HFGWTR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hpfar_el2.xml */
ARMV8_SYSREG_RW(HPFAR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-hstr_el2.xml */
ARMV8_SYSREG_RW(HSTR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_ap0rn_el1.xml */
ARMV8_SYSREG_RW(ICC_AP0R0_EL1, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICC_AP0R1_EL1, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICC_AP0R2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_ap1rn_el1.xml */
ARMV8_SYSREG_RW(ICC_AP1R0_EL1, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICC_AP1R1_EL1, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICC_AP1R2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_asgi1r_el1.xml */
ARMV8_SYSREG_RW(ICC_ASGI1R_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_bpr0_el1.xml */
ARMV8_SYSREG_RW(ICC_BPR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_bpr1_el1.xml */
ARMV8_SYSREG_RW(ICC_BPR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_ctlr_el1.xml */
ARMV8_SYSREG_RW(ICC_CTLR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_ctlr_el3.xml */
ARMV8_SYSREG_RW(ICC_CTLR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_dir_el1.xml */
ARMV8_SYSREG_RW(ICC_DIR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_eoir0_el1.xml */
ARMV8_SYSREG_RW(ICC_EOIR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_eoir1_el1.xml */
ARMV8_SYSREG_RW(ICC_EOIR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_hppir0_el1.xml */
ARMV8_SYSREG_RW(ICC_HPPIR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_hppir1_el1.xml */
ARMV8_SYSREG_RW(ICC_HPPIR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_iar0_el1.xml */
ARMV8_SYSREG_RW(ICC_IAR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_iar1_el1.xml */
ARMV8_SYSREG_RW(ICC_IAR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_igrpen0_el1.xml */
ARMV8_SYSREG_RW(ICC_IGRPEN0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_igrpen1_el1.xml */
ARMV8_SYSREG_RW(ICC_IGRPEN1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_igrpen1_el3.xml */
ARMV8_SYSREG_RW(ICC_IGRPEN1_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_nmiar1_el1.xml */
ARMV8_SYSREG_RW(ICC_NMIAR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_pmr_el1.xml */
ARMV8_SYSREG_RW(ICC_PMR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_rpr_el1.xml */
ARMV8_SYSREG_RW(ICC_RPR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_sgi0r_el1.xml */
ARMV8_SYSREG_RW(ICC_SGI0R_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_sgi1r_el1.xml */
ARMV8_SYSREG_RW(ICC_SGI1R_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_sre_el1.xml */
ARMV8_SYSREG_RW(ICC_SRE_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_sre_el2.xml */
ARMV8_SYSREG_RW(ICC_SRE_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-icc_sre_el3.xml */
ARMV8_SYSREG_RW(ICC_SRE_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-ich_ap0rn_el2.xml */
ARMV8_SYSREG_RW(ICH_AP0R0_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_AP0R1_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_AP0R2_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-ich_ap1rn_el2.xml */
ARMV8_SYSREG_RW(ICH_AP1R0_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_AP1R1_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_AP1R2_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-ich_eisr_el2.xml */
ARMV8_SYSREG_RW(ICH_EISR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-ich_elrsr_el2.xml */
ARMV8_SYSREG_RW(ICH_ELRSR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-ich_hcr_el2.xml */
ARMV8_SYSREG_RW(ICH_HCR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-ich_lrn_el2.xml */
ARMV8_SYSREG_RW(ICH_LR0_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_LR1_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_LR2_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_LR3_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_LR4_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_LR5_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_LR6_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_LR7_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_LR8_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_LR9_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_LR10_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_LR11_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_LR12_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_LR13_EL2, S3_3_C3_C3_3, 64)
ARMV8_SYSREG_RW(ICH_LR14_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-ich_misr_el2.xml */
ARMV8_SYSREG_RW(ICH_MISR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-ich_vmcr_el2.xml */
ARMV8_SYSREG_RW(ICH_VMCR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-ich_vtr_el2.xml */
ARMV8_SYSREG_RW(ICH_VTR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64afr0_el1.xml */
ARMV8_SYSREG_RW(ID_AA64AFR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64afr1_el1.xml */
ARMV8_SYSREG_RW(ID_AA64AFR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64dfr0_el1.xml */
ARMV8_SYSREG_RW(ID_AA64DFR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64dfr1_el1.xml */
ARMV8_SYSREG_RW(ID_AA64DFR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64dfr2_el1.xml */
ARMV8_SYSREG_RW(ID_AA64DFR2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64fpfr0_el1.xml */
ARMV8_SYSREG_RW(ID_AA64FPFR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64isar0_el1.xml */
ARMV8_SYSREG_RW(ID_AA64ISAR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64isar1_el1.xml */
ARMV8_SYSREG_RW(ID_AA64ISAR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64isar2_el1.xml */
ARMV8_SYSREG_RW(ID_AA64ISAR2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64isar3_el1.xml */
ARMV8_SYSREG_RW(ID_AA64ISAR3_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64mmfr0_el1.xml */
ARMV8_SYSREG_RW(ID_AA64MMFR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64mmfr1_el1.xml */
ARMV8_SYSREG_RW(ID_AA64MMFR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64mmfr2_el1.xml */
ARMV8_SYSREG_RW(ID_AA64MMFR2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64mmfr3_el1.xml */
ARMV8_SYSREG_RW(ID_AA64MMFR3_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64mmfr4_el1.xml */
ARMV8_SYSREG_RW(ID_AA64MMFR4_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64pfr0_el1.xml */
ARMV8_SYSREG_RW(ID_AA64PFR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64pfr1_el1.xml */
ARMV8_SYSREG_RW(ID_AA64PFR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64pfr2_el1.xml */
ARMV8_SYSREG_RW(ID_AA64PFR2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64smfr0_el1.xml */
ARMV8_SYSREG_RW(ID_AA64SMFR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_aa64zfr0_el1.xml */
ARMV8_SYSREG_RW(ID_AA64ZFR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_afr0_el1.xml */
ARMV8_SYSREG_RW(ID_AFR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_dfr0_el1.xml */
ARMV8_SYSREG_RW(ID_DFR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_dfr1_el1.xml */
ARMV8_SYSREG_RW(ID_DFR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_isar0_el1.xml */
ARMV8_SYSREG_RW(ID_ISAR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_isar1_el1.xml */
ARMV8_SYSREG_RW(ID_ISAR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_isar2_el1.xml */
ARMV8_SYSREG_RW(ID_ISAR2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_isar3_el1.xml */
ARMV8_SYSREG_RW(ID_ISAR3_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_isar4_el1.xml */
ARMV8_SYSREG_RW(ID_ISAR4_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_isar5_el1.xml */
ARMV8_SYSREG_RW(ID_ISAR5_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_isar6_el1.xml */
ARMV8_SYSREG_RW(ID_ISAR6_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_mmfr0_el1.xml */
ARMV8_SYSREG_RW(ID_MMFR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_mmfr1_el1.xml */
ARMV8_SYSREG_RW(ID_MMFR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_mmfr2_el1.xml */
ARMV8_SYSREG_RW(ID_MMFR2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_mmfr3_el1.xml */
ARMV8_SYSREG_RW(ID_MMFR3_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_mmfr4_el1.xml */
ARMV8_SYSREG_RW(ID_MMFR4_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_mmfr5_el1.xml */
ARMV8_SYSREG_RW(ID_MMFR5_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_pfr0_el1.xml */
ARMV8_SYSREG_RW(ID_PFR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_pfr1_el1.xml */
ARMV8_SYSREG_RW(ID_PFR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-id_pfr2_el1.xml */
ARMV8_SYSREG_RW(ID_PFR2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-ifsr32_el2.xml */
ARMV8_SYSREG_RW(IFSR32_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-isr_el1.xml */
ARMV8_SYSREG_RW(ISR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-lorc_el1.xml */
ARMV8_SYSREG_RW(LORC_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-lorea_el1.xml */
ARMV8_SYSREG_RW(LOREA_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-lorid_el1.xml */
ARMV8_SYSREG_RW(LORID_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-lorn_el1.xml */
ARMV8_SYSREG_RW(LORN_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-lorsa_el1.xml */
ARMV8_SYSREG_RW(LORSA_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-mair2_el1.xml */
ARMV8_SYSREG_RW(MAIR2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-mair2_el2.xml */
ARMV8_SYSREG_RW(MAIR2_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-mair2_el3.xml */
ARMV8_SYSREG_RW(MAIR2_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-mair_el1.xml */
ARMV8_SYSREG_RW(MAIR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-mair_el2.xml */
ARMV8_SYSREG_RW(MAIR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-mair_el3.xml */
ARMV8_SYSREG_RW(MAIR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-mdccint_el1.xml */
ARMV8_SYSREG_RW(MDCCINT_EL1, S3_2_C2_C2_2, 64)
/* Source: AArch64-mdccsr_el0.xml */
ARMV8_SYSREG_RW(MDCCSR_EL0, S3_2_C2_C2_2, 64)
/* Source: AArch64-mdcr_el2.xml */
ARMV8_SYSREG_RW(MDCR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-mdcr_el3.xml */
ARMV8_SYSREG_RW(MDCR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-mdrar_el1.xml */
ARMV8_SYSREG_RW(MDRAR_EL1, S3_2_C2_C2_2, 64)
/* Source: AArch64-mdscr_el1.xml */
ARMV8_SYSREG_RW(MDSCR_EL1, S3_2_C2_C2_2, 64)
/* Source: AArch64-mdselr_el1.xml */
ARMV8_SYSREG_RW(MDSELR_EL1, S3_2_C2_C2_2, 64)
/* Source: AArch64-mdstepop_el1.xml */
ARMV8_SYSREG_RW(MDSTEPOP_EL1, S3_2_C2_C2_2, 64)
/* Source: AArch64-mecid_a0_el2.xml */
ARMV8_SYSREG_RW(MECID_A0_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-mecid_a1_el2.xml */
ARMV8_SYSREG_RW(MECID_A1_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-mecid_p0_el2.xml */
ARMV8_SYSREG_RW(MECID_P0_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-mecid_p1_el2.xml */
ARMV8_SYSREG_RW(MECID_P1_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-mecid_rl_a_el3.xml */
ARMV8_SYSREG_RW(MECID_RL_A_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-mecidr_el2.xml */
ARMV8_SYSREG_RW(MECIDR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-mfar_el3.xml */
ARMV8_SYSREG_RW(MFAR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-midr_el1.xml */
ARMV8_SYSREG_RW(MIDR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-mpidr_el1.xml */
ARMV8_SYSREG_RW(MPIDR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-mvfr0_el1.xml */
ARMV8_SYSREG_RW(MVFR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-mvfr1_el1.xml */
ARMV8_SYSREG_RW(MVFR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-mvfr2_el1.xml */
ARMV8_SYSREG_RW(MVFR2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-nzcv.xml */
ARMV8_SYSREG_RW(NZCV, S3_3_C3_C3_3, 64)
/* Source: AArch64-osdlr_el1.xml */
ARMV8_SYSREG_RW(OSDLR_EL1, S3_2_C2_C2_2, 64)
/* Source: AArch64-osdtrrx_el1.xml */
ARMV8_SYSREG_RW(OSDTRRX_EL1, S3_2_C2_C2_2, 64)
/* Source: AArch64-osdtrtx_el1.xml */
ARMV8_SYSREG_RW(OSDTRTX_EL1, S3_2_C2_C2_2, 64)
/* Source: AArch64-oseccr_el1.xml */
ARMV8_SYSREG_RW(OSECCR_EL1, S3_2_C2_C2_2, 64)
/* Source: AArch64-oslar_el1.xml */
ARMV8_SYSREG_RW(OSLAR_EL1, S3_2_C2_C2_2, 64)
/* Source: AArch64-oslsr_el1.xml */
ARMV8_SYSREG_RW(OSLSR_EL1, S3_2_C2_C2_2, 64)
/* Source: AArch64-pan.xml */
ARMV8_SYSREG_RW(PAN, S3_3_C3_C3_3, 64)
/* Source: AArch64-par_el1.xml */
ARMV8_SYSREG_RW(PAR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-pfar_el1.xml */
ARMV8_SYSREG_RW(PFAR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-pfar_el2.xml */
ARMV8_SYSREG_RW(PFAR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-pir_el1.xml */
ARMV8_SYSREG_RW(PIR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-pir_el2.xml */
ARMV8_SYSREG_RW(PIR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-pir_el3.xml */
ARMV8_SYSREG_RW(PIR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-pire0_el1.xml */
ARMV8_SYSREG_RW(PIRE0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-pire0_el2.xml */
ARMV8_SYSREG_RW(PIRE0_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-por_el0.xml */
ARMV8_SYSREG_RW(POR_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-por_el1.xml */
ARMV8_SYSREG_RW(POR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-por_el2.xml */
ARMV8_SYSREG_RW(POR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-por_el3.xml */
ARMV8_SYSREG_RW(POR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-rcwmask_el1.xml */
ARMV8_SYSREG_RW(RCWMASK_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-rcwsmask_el1.xml */
ARMV8_SYSREG_RW(RCWSMASK_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-revidr_el1.xml */
ARMV8_SYSREG_RW(REVIDR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-rgsr_el1.xml */
ARMV8_SYSREG_RW(RGSR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-rmr_el1.xml */
ARMV8_SYSREG_RW(RMR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-rmr_el2.xml */
ARMV8_SYSREG_RW(RMR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-rmr_el3.xml */
ARMV8_SYSREG_RW(RMR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-rndr.xml */
ARMV8_SYSREG_RW(RNDR, S3_3_C3_C3_3, 64)
/* Source: AArch64-rndrrs.xml */
ARMV8_SYSREG_RW(RNDRRS, S3_3_C3_C3_3, 64)
/* Source: AArch64-rvbar_el1.xml */
ARMV8_SYSREG_RW(RVBAR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-rvbar_el2.xml */
ARMV8_SYSREG_RW(RVBAR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-rvbar_el3.xml */
ARMV8_SYSREG_RW(RVBAR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-s2pir_el2.xml */
ARMV8_SYSREG_RW(S2PIR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-s2por_el1.xml */
ARMV8_SYSREG_RW(S2POR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-scr_el3.xml */
ARMV8_SYSREG_RW(SCR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-sctlr2_el1.xml */
ARMV8_SYSREG_RW(SCTLR2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-sctlr2_el2.xml */
ARMV8_SYSREG_RW(SCTLR2_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-sctlr2_el3.xml */
ARMV8_SYSREG_RW(SCTLR2_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-sctlr_el1.xml */
ARMV8_SYSREG_RW(SCTLR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-sctlr_el2.xml */
ARMV8_SYSREG_RW(SCTLR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-sctlr_el3.xml */
ARMV8_SYSREG_RW(SCTLR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-scxtnum_el0.xml */
ARMV8_SYSREG_RW(SCXTNUM_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-scxtnum_el1.xml */
ARMV8_SYSREG_RW(SCXTNUM_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-scxtnum_el2.xml */
ARMV8_SYSREG_RW(SCXTNUM_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-scxtnum_el3.xml */
ARMV8_SYSREG_RW(SCXTNUM_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-sder32_el2.xml */
ARMV8_SYSREG_RW(SDER32_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-sder32_el3.xml */
ARMV8_SYSREG_RW(SDER32_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-smcr_el1.xml */
ARMV8_SYSREG_RW(SMCR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-smcr_el2.xml */
ARMV8_SYSREG_RW(SMCR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-smcr_el3.xml */
ARMV8_SYSREG_RW(SMCR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-smidr_el1.xml */
ARMV8_SYSREG_RW(SMIDR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-smpri_el1.xml */
ARMV8_SYSREG_RW(SMPRI_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-smprimap_el2.xml */
ARMV8_SYSREG_RW(SMPRIMAP_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-sp_el0.xml */
ARMV8_SYSREG_RW(SP_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-sp_el1.xml */
ARMV8_SYSREG_RW(SP_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-sp_el2.xml */
ARMV8_SYSREG_RW(SP_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-spsel.xml */
ARMV8_SYSREG_RW(SPSel, S3_3_C3_C3_3, 64)
/* Source: AArch64-spsr_abt.xml */
ARMV8_SYSREG_RW(SPSR_abt, S3_3_C3_C3_3, 64)
/* Source: AArch64-spsr_el1.xml */
ARMV8_SYSREG_RW(SPSR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-spsr_el2.xml */
ARMV8_SYSREG_RW(SPSR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-spsr_el3.xml */
ARMV8_SYSREG_RW(SPSR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-spsr_fiq.xml */
ARMV8_SYSREG_RW(SPSR_fiq, S3_3_C3_C3_3, 64)
/* Source: AArch64-spsr_irq.xml */
ARMV8_SYSREG_RW(SPSR_irq, S3_3_C3_C3_3, 64)
/* Source: AArch64-spsr_und.xml */
ARMV8_SYSREG_RW(SPSR_und, S3_3_C3_C3_3, 64)
/* Source: AArch64-ssbs.xml */
ARMV8_SYSREG_RW(SSBS, S3_3_C3_C3_3, 64)
/* Source: AArch64-svcr.xml */
ARMV8_SYSREG_RW(SVCR, S3_3_C3_C3_3, 64)
/* Source: AArch64-tco.xml */
ARMV8_SYSREG_RW(TCO, S3_3_C3_C3_3, 64)
/* Source: AArch64-tcr2_el1.xml */
ARMV8_SYSREG_RW(TCR2_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-tcr2_el2.xml */
ARMV8_SYSREG_RW(TCR2_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-tcr_el1.xml */
ARMV8_SYSREG_RW(TCR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-tcr_el2.xml */
ARMV8_SYSREG_RW(TCR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-tcr_el3.xml */
ARMV8_SYSREG_RW(TCR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-tfsr_el1.xml */
ARMV8_SYSREG_RW(TFSR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-tfsr_el2.xml */
ARMV8_SYSREG_RW(TFSR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-tfsr_el3.xml */
ARMV8_SYSREG_RW(TFSR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-tfsre0_el1.xml */
ARMV8_SYSREG_RW(TFSRE0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-tpidr2_el0.xml */
ARMV8_SYSREG_RW(TPIDR2_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-tpidr_el0.xml */
ARMV8_SYSREG_RW(TPIDR_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-tpidr_el1.xml */
ARMV8_SYSREG_RW(TPIDR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-tpidr_el2.xml */
ARMV8_SYSREG_RW(TPIDR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-tpidr_el3.xml */
ARMV8_SYSREG_RW(TPIDR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-tpidrro_el0.xml */
ARMV8_SYSREG_RW(TPIDRRO_EL0, S3_3_C3_C3_3, 64)
/* Source: AArch64-trbbaser_el1.xml */
ARMV8_SYSREG_RW(TRBBASER_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-trbidr_el1.xml */
ARMV8_SYSREG_RW(TRBIDR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-trblimitr_el1.xml */
ARMV8_SYSREG_RW(TRBLIMITR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-trbmar_el1.xml */
ARMV8_SYSREG_RW(TRBMAR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-trbmpam_el1.xml */
ARMV8_SYSREG_RW(TRBMPAM_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-trbptr_el1.xml */
ARMV8_SYSREG_RW(TRBPTR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-trbtrg_el1.xml */
ARMV8_SYSREG_RW(TRBTRG_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-trfcr_el1.xml */
ARMV8_SYSREG_RW(TRFCR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-trfcr_el2.xml */
ARMV8_SYSREG_RW(TRFCR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-ttbr0_el1.xml */
ARMV8_SYSREG_RW(TTBR0_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-ttbr0_el2.xml */
ARMV8_SYSREG_RW(TTBR0_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-ttbr0_el3.xml */
ARMV8_SYSREG_RW(TTBR0_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-ttbr1_el1.xml */
ARMV8_SYSREG_RW(TTBR1_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-ttbr1_el2.xml */
ARMV8_SYSREG_RW(TTBR1_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-uao.xml */
ARMV8_SYSREG_RW(UAO, S3_3_C3_C3_3, 64)
/* Source: AArch64-vbar_el1.xml */
ARMV8_SYSREG_RW(VBAR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-vbar_el2.xml */
ARMV8_SYSREG_RW(VBAR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-vbar_el3.xml */
ARMV8_SYSREG_RW(VBAR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-vdisr_el2.xml */
ARMV8_SYSREG_RW(VDISR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-vdisr_el3.xml */
ARMV8_SYSREG_RW(VDISR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-vmecid_a_el2.xml */
ARMV8_SYSREG_RW(VMECID_A_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-vmecid_p_el2.xml */
ARMV8_SYSREG_RW(VMECID_P_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-vmpidr_el2.xml */
ARMV8_SYSREG_RW(VMPIDR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-vncr_el2.xml */
ARMV8_SYSREG_RW(VNCR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-vpidr_el2.xml */
ARMV8_SYSREG_RW(VPIDR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-vsesr_el2.xml */
ARMV8_SYSREG_RW(VSESR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-vsesr_el3.xml */
ARMV8_SYSREG_RW(VSESR_EL3, S3_3_C3_C3_3, 64)
/* Source: AArch64-vstcr_el2.xml */
ARMV8_SYSREG_RW(VSTCR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-vsttbr_el2.xml */
ARMV8_SYSREG_RW(VSTTBR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-vtcr_el2.xml */
ARMV8_SYSREG_RW(VTCR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-vttbr_el2.xml */
ARMV8_SYSREG_RW(VTTBR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-zcr_el1.xml */
ARMV8_SYSREG_RW(ZCR_EL1, S3_3_C3_C3_3, 64)
/* Source: AArch64-zcr_el2.xml */
ARMV8_SYSREG_RW(ZCR_EL2, S3_3_C3_C3_3, 64)
/* Source: AArch64-zcr_el3.xml */
ARMV8_SYSREG_RW(ZCR_EL3, S3_3_C3_C3_3, 64)
