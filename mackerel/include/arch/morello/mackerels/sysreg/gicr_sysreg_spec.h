/*
 * Copyright (c) 2017 ETH Zurich. All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, CAB F.78, Universitaetstr. 6, CH-8092 Zurich,
 * Attn: Systems Group.
 */

/*
 * NOTE: This file has been automatically generated based on the XML files
 * provided by ARM.
 *
 * Download from:
 * https://developer.arm.com/products/architecture/a-profile/exploration-tools
 *
 * Based on version: SysReg_v82A_xml-00bet3.1.tar.gz
 */

/* Source: ext-gicr_clrlpir.xml */
/* Source: ext-gicr_ctlr.xml */
/* Source: ext-gicr_icactiver0.xml */
/* Source: ext-gicr_icactiverne.xml */
/* Source: ext-gicr_icenabler0.xml */
/* Source: ext-gicr_icenablerne.xml */
/* Source: ext-gicr_icfgr0.xml */
/* Source: ext-gicr_icfgr1.xml */
/* Source: ext-gicr_icfgrne.xml */
/* Source: ext-gicr_icpendr0.xml */
/* Source: ext-gicr_icpendrne.xml */
/* Source: ext-gicr_igroupr0.xml */
/* Source: ext-gicr_igrouprne.xml */
/* Source: ext-gicr_igrpmodr0.xml */
/* Source: ext-gicr_igrpmodrne.xml */
/* Source: ext-gicr_iidr.xml */
/* Source: ext-gicr_inmir0.xml */
/* Source: ext-gicr_inmirne.xml */
/* Source: ext-gicr_invallr.xml */
/* Source: ext-gicr_invlpir.xml */
/* Source: ext-gicr_ipriorityrn.xml */
/* Source: ext-gicr_ipriorityrne.xml */
/* Source: ext-gicr_isactiver0.xml */
/* Source: ext-gicr_isactiverne.xml */
/* Source: ext-gicr_isenabler0.xml */
/* Source: ext-gicr_isenablerne.xml */
/* Source: ext-gicr_ispendr0.xml */
/* Source: ext-gicr_ispendrne.xml */
/* Source: ext-gicr_mpamidr.xml */
/* Source: ext-gicr_nsacr.xml */
/* Source: ext-gicr_partidr.xml */
/* Source: ext-gicr_pendbaser.xml */
/* Source: ext-gicr_propbaser.xml */
/* Source: ext-gicr_setlpir.xml */
/* Source: ext-gicr_statusr.xml */
/* Source: ext-gicr_syncr.xml */
/* Source: ext-gicr_typer.xml */
/* Source: ext-gicr_vpendbaser.xml */
/* Source: ext-gicr_vpropbaser.xml */
/* Source: ext-gicr_vsgipendr.xml */
/* Source: ext-gicr_vsgir.xml */
/* Source: ext-gicr_waker.xml */
