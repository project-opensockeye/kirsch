
#include <timer.h>
#include <stdint.h>

struct set_timer_data set_timer_data;

void set_timer(int offset)
{
	__asm__ volatile("msr CNTHP_TVAL_EL2, %[offset]"
			 :
			 : [offset] "r"((uint64_t)offset));
}
