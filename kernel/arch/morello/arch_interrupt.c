
#include <stddef.h>
#include <stdint.h>
#include <bf/logging.h>
#include <mackerel_support/mackerel.h>
#include <mackerels/sysreg/sysreg.h>
#include <mackerels/armv8_dev.h>
#include <mackerels/gicd_dev.h>
#include <mackerels/gicr_dev.h>
#include <cheri/cheri_interface.h>

void print_gic_status(gicr_t *gic_redist, gicd_t *gic_dist)
{
	char str[2048];

	if (gic_dist != NULL) {
		gicd_GICD_STATUSR_pr(str, 2048, gic_dist);

		printf("GICD STATUSR:\n");
		printf("%s\n", str);
	}

	if (gic_redist != NULL) {
		gicr_GICR_STATUSR_pr(str, 2048, gic_redist);

		printf("GICR STATUSR:\n");
		printf("%s\n", str);
	}
}

#define gicd_wait_reg(val)                                   \
	printk(LOG_NOTE, "GICD: Waiting for " #val "...\n"); \
	while (gicd_GICD_CTLR_##val##_rdf(gic_dist) != 0) {  \
		__asm__ volatile("\n");                      \
	}                                                    \
	printk(LOG_NOTE, "GICD: " #val "clear!\n");

#define gicr_wait_reg(val)                                    \
	printk(LOG_NOTE, "GICR: Waiting for " #val "...\n");  \
	while (gicr_GICR_CTLR_##val##_rdf(gic_redist) != 0) { \
		__asm__ volatile("\n");                       \
	}                                                     \
	printk(LOG_NOTE, "GICR: " #val "clear!\n");

void gic_changes_commit_and_wait(gicr_t *gic_redist, gicd_t *gic_dist);
void gic_changes_commit_and_wait(gicr_t *gic_redist, gicd_t *gic_dist)
{
	__asm__ volatile("isb\ndsb SY\ndmb SY\n");

	if (gic_dist != NULL) {
		gicd_wait_reg(RWP);
	}

	__asm__ volatile("isb\ndsb SY\ndmb SY\n");

	if (gic_redist != NULL) {
		gicr_wait_reg(RWP);
		gicr_wait_reg(UWP);
	}
}

extern void Vector_table_el2(void);

void gic_init(capability vector_base_address, gicd_t *gic_dist,
	      gicr_t *gic_redist, armv8_t *armv8)
{
	printk(LOG_NOTE, "GIC initialize\n");
	// See https://developer.arm.com/documentation/198123/0302/Configuring-the-Arm-GIC
	// for an introduction on how to configure an ARM GIC.

	printk(LOG_NOTE,
	       "GICD IIDR "
	       "implementer=0x%x, revision=0x%x, variant=0x%x, prodid=0x%x\n",
	       gicd_GICD_IIDR_Implementer_rdf(gic_dist),
	       gicd_GICD_IIDR_Revision_rdf(gic_dist),
	       gicd_GICD_IIDR_Variant_rdf(gic_dist),
	       gicd_GICD_IIDR_ProductID_rdf(gic_dist));

	uint32_t itlines = gicd_GICD_TYPER_ITLinesNumber_rdf(gic_dist);
	itlines = (itlines + 1) * 32;
	if (itlines > 1020)
		itlines = 1020;
	printk(LOG_NOTE, "gic: #INTIDs supported: %" PRIu32 "\n", itlines);

	print_gic_status(NULL, gic_dist);

	// Put all interrupts into Group 1 and disable them
#define MASK_32 0xffffffff
	for (int i = 0; i * 32 < itlines; i++) {
		// Clear
		gicd_GICD_ICACTIVERn_wr(gic_dist, i, MASK_32);
		// Disable all interrupts
		gicd_GICD_ICENABLERn_wr(gic_dist, i, MASK_32);
		// And put in group 1
		gicd_GICD_IGROUPRn_rawwr(gic_dist, i, MASK_32);
	}

	gic_changes_commit_and_wait(NULL, gic_dist);

	// In GICD CTRL
	// Enable Affinity routing (ARE)
	// Configure GICv3 mode
	printk(LOG_NOTE, "Affinity Routing enable...\n");
	gicd_GICD_CTLR_ARE_NS_wrf(gic_dist, 0x1);
	//gicd_GICD_CTLR_DS_wrf(gic_dist, 0x0);

	// Changing EnableGrp* is UNDEFINED if ARE/DS are not stable.
	gic_changes_commit_and_wait(NULL, gic_dist);

	// enable non-secure group 1
	// EnableGrp1NS
	printk(LOG_NOTE, "None Secure Group 1 enable...\n");
	gicd_GICD_CTLR_EnableGrp1NS_wrf(gic_dist, 0x1);
	gicd_GICD_CTLR_EnableGrp0_wrf(gic_dist, 0x1);
	gicd_GICD_CTLR_EnableGrp1S_wrf(gic_dist, 0x1);

	gic_changes_commit_and_wait(NULL, gic_dist);

	// Redistributor configuration
	// mark PE online
	// Clear GICR_WAKER.ProcessorSleep to 0

	if (gicr_GICR_WAKER_ChildrenAsleep_rdf(gic_redist) == 1) {
		printk(LOG_NOTE,
		       "PE interface may be quiescent, enabling...\n");
		printk(LOG_NOTE, "PE mark online...\n");
		gicr_GICR_WAKER_ProcessorSleep_wrf(gic_redist, 0);

		gic_changes_commit_and_wait(gic_redist, gic_dist);

		// Poll GICR_WAKER.ChildrenAsleep until 0
		printk(LOG_NOTE, "PE WAKER wait...\n");
		while (gicr_GICR_WAKER_ChildrenAsleep_rdf(gic_redist) != 0) {
			__asm__ volatile("\n");
		}
	} else {
		printk(LOG_NOTE, "PE interface might be active already.\n");
	}

	printk(LOG_NOTE,
	       "GICR IIDR "
	       "implementer=0x%x, revision=0x%x, variant=0x%x, prodid=0x%x\n",
	       gicr_GICR_IIDR_Implementer_rdf(gic_redist),
	       gicr_GICR_IIDR_Revision_rdf(gic_redist),
	       gicr_GICR_IIDR_Variant_rdf(gic_redist),
	       gicr_GICR_IIDR_ProductID_rdf(gic_redist));

#define BUF_SIZE 16000
	char buf[BUF_SIZE];
	gicr_GICR_TYPER_pr(buf, BUF_SIZE, gic_redist);
	printf("%s\n", buf);

	print_gic_status(gic_redist, gic_dist);

	// UNPREDICTABLE behaviour if CPU is configured before this.
}

void cpu_init(capability vector_base_address, gicd_t *gic_dist,
	      gicr_t *gic_redist, armv8_t *armv8)
{
	__asm__ volatile("dsb SY\nisb\n");
	print_gic_status(gic_redist, gic_dist);

	// CPU SETUP
	// *********

	// Configure CPU interface
	// Enable system register access
	// ICC_SRE_ELn - set SRE bit to enable access to CPU interface registers
	printk(LOG_NOTE, "SRE bit set...\n");
	armv8_ICC_SRE_EL2_SRE_wrf(armv8, 0x1);

	// Priority mask & Binary point values stolen from BF / Linux

	// Priority mask and Binary point registers
	// ICC_PMR_EL1 priority mask register: mnimum priority to be forwarded
	printk(LOG_NOTE, "Priority mask set...\n");
	armv8_ICC_PMR_EL1_Priority_wrf(armv8, 0xF0);

	// ICC_BPRn_EL1 binary point register: priority grouping and preemption
	// Group / Subpriority split (8 bits):
	// 0x0 - ggggggg.s
	// 0x1 - 6 group prio bits, 2 subpriority bits
	// 0x7 - .ssssssss
	printk(LOG_NOTE, "Priority grouping set...\n");
	armv8_ICC_BPR0_EL1_BinaryPoint_wrf(armv8, 0x1);
	armv8_ICC_BPR1_EL1_BinaryPoint_wrf(armv8, 0x1);

	// EOI mode
	// ICC_CTRL_EL1 and ICC_CTRL_EL3 control how completion of interrupt is handled
	//armv8_ICC_CTLR_EL1_t ctlr_el1 = armv8_ICC_CTLR_EL1_rd(armv8);
	printk(LOG_NOTE, "Interrupt completion set...\n");
	armv8_ICC_CTLR_EL1_EOImode_wrf(armv8, 0x0);
	armv8_ICC_CTLR_EL1_CBPR_wrf(armv8, 0x1);

	// Enable signaling of each interrupt group
	// ICC_IGRPEN1_El1 for group 1 interrupts
	// - banked by security state
	// ICC_IGRPEN0_EL1 for group 0 interrupts
	printk(LOG_NOTE, "Interrupt group signaling enable...\n");
	armv8_ICC_IGRPEN1_EL1_Enable_wrf(armv8, 0x1);
	armv8_ICC_IGRPEN0_EL1_Enable_wrf(armv8, 0x1);

	printk(LOG_NOTE, "GICR Initial clear...\n");
	// Initially clear and disable
	gicr_GICR_ICACTIVER0_rawwr(gic_redist, MASK_32);
	// Disable PPIs
	gicr_GICR_ICENABLER0_wr(gic_redist, MASK_32);
	gicr_GICR_IGROUPR0_rawwr(gic_redist, MASK_32);
	gicr_GICR_IGRPMODR0_rawwr(gic_redist, 0);

	gic_changes_commit_and_wait(gic_redist, gic_dist);

	// PE configuration
	// ****************

	// routing control
	// SCR_EL3 and HCR_EL2 routing control interrupts
	// determine exception level in which an exception is taken
	// UNKNOWN at reset

	// HCR
	printk(LOG_NOTE, "Global interrupt enable initialize...\n");
	armv8_HCR_EL2_t hcr_el2 = armv8_HCR_EL2_rd(armv8);
	// Route Physical FIQ to EL2
	printk(LOG_NOTE, " -> Physical FIQ route to EL2 enable...\n");
	hcr_el2 = armv8_HCR_EL2_FMO_insert(hcr_el2, 0x1);

	// Route Physical IRQ to EL2
	printk(LOG_NOTE, " -> Physical IRQ route to EL2 enable...\n");
	hcr_el2 = armv8_HCR_EL2_IMO_insert(hcr_el2, 0x1);

	// Route Physical SError Exceptions to EL2
	printk(LOG_NOTE, " -> Physical SERROR route to EL2 enable...\n");
	hcr_el2 = armv8_HCR_EL2_AMO_insert(hcr_el2, 0x1);

	// Route all exceptions to EL2, even if they were to go to EL1
	printk(LOG_NOTE, " -> Exception route to EL2 enable...\n");
	hcr_el2 = armv8_HCR_EL2_TGE_insert(hcr_el2, 0x1);

	// Enable facilities for EL2 OS
	printk(LOG_NOTE, " -> EL2 OS facilities enable...\n");
	hcr_el2 = armv8_HCR_EL2_E2H_insert(hcr_el2, 0x1);

	// Route all External abort exceptions to EL2
	printk(LOG_NOTE, " -> External abort exception EL2 route enable...\n");
	hcr_el2 = armv8_HCR_EL2_TEA_insert(hcr_el2, 0x1);

	armv8_HCR_EL2_wr(armv8, hcr_el2);

	// Interrupt masks
	// exception bit mask in PSTATE: set bit -> interrupt masked
	// AArch64 puts some of them in DAIF
	// 0x0 = not masked
	// 0x1 = masked
	printk(LOG_NOTE, "Interrupt mask setup...\n");
	armv8_DAIF_t daif = armv8_DAIF_default;

	// Watchpoint, Breakpoint, Software Step Exceptions
	printk(LOG_NOTE, " -> Enabling Watchpoints, Breakpoints, ...\n");
	daif = armv8_DAIF_D_insert(daif, 0x0);

	// SError Exceptions
	printk(LOG_NOTE, " -> Enabling SError...\n");
	daif = armv8_DAIF_A_insert(daif, 0x0);

	// IRQ Exceptions
	printk(LOG_NOTE, " -> Enabling IRQ...\n");
	daif = armv8_DAIF_I_insert(daif, 0x0);

	// FIQ Exceptions
	// Unmasked!
	printk(LOG_NOTE, " -> Enabling IRQ...\n");
	daif = armv8_DAIF_F_insert(daif, 0x0);

	armv8_DAIF_wr(armv8, daif);

	// ALLINT masks all IRQ and FIQ if SCTLR_ELx.NMI is 1
	//printk(LOG_NOTE, "ALLINT disable...");
	//armv8_ALLINT_ALLINT_wrf(armv8, 0x0);

	// Vector table
	// set by VBAR_ELn -> UNKNOWN at reset!
	printk(LOG_NOTE, "Vector table register...\n");
	__asm__ volatile("msr CVBAR_EL2, %[val]\n"
			 "isb \n"
			 :
			 : [val] "C"(cheri_address_set(
				 cheri_pcc_get(), (size_t)&Vector_table_el2)));

	printk(LOG_NOTE, "GIC initialized!");
	print_gic_status(gic_redist, gic_dist);
}

void timer_init(armv8_t *armv8, gicr_t *gic_redist)
{
	printk(LOG_NOTE, "Timer initialize...\n");

	// Configure Interrupt
	// Non-Secure EL2 Physical Timer
	// CNTPH
	// SBSA Recommended INTID: 26
	// Morello PPI 5

	// 1. Set Priority
	// IPRIORITY n = 26 div 4 = 6
	// Priority offset m = 26 mod 4 = 2
	printk(LOG_NOTE, "NS EL2 Phys Timer Interrupt Priority set...\n");
	gicr_GICR_IPRIORITYRn_Priority_offset_2B_wrf(gic_redist, 6, 0x0);

	// 2. Set Interrupt Group
	// Non-secure / group 1
	// 	 modifier bit = 0
	//   status   bit = 1
	printk(LOG_NOTE,
	       "NS EL2 Phys Timer Interrupt Group Status Bit set...\n");
	gicr_GICR_IGROUPR0_Redistributor_group_status_bit26_wrf(gic_redist,
								0x1);
	printk(LOG_NOTE,
	       "NS EL2 Phys Timer Interrupt Modifier Status Bit set...\n");
	gicr_GICR_IGRPMODR0_Group_modifier_bit26_wrf(gic_redist, 0x0);

	gic_changes_commit_and_wait(gic_redist, NULL);

	// 3. Set edge-triggered
	//gicr_GICR_ICFGR1_Int_config5_wrf(gic_redist, 0x3);

	// 4. Enable Interrupt
	printk(LOG_NOTE, "NS EL2 Phys Timer Interrupt enable...\n");
	gicr_GICR_ISENABLER0_Set_enable_bit26_wrf(gic_redist, 0x1);
	gic_changes_commit_and_wait(gic_redist, NULL);
	print_gic_status(gic_redist, NULL);
	printk(LOG_NOTE, "NS EL2 Phys Timer Interrupt Enable wait...\n");
	while (gicr_GICR_ISENABLER0_Set_enable_bit26_rdf(gic_redist) == 0) {
		__asm__ volatile("\n");
	}
	printk(LOG_NOTE, "NS EL2 Phys Timer Interrupt Enable complete!\n");
	uint32_t val = gicr_GICR_ISENABLER0_Set_enable_bit26_rdf(gic_redist);
	printf("\n\n\n%x\n\n\n", val);

	__asm__ volatile("dsb SY\nisb\n");

#define BUF_SIZE 16000
	char buf[BUF_SIZE];
	gicr_GICR_ISENABLER0_pr(buf, BUF_SIZE, gic_redist);
	printf("%s\n", buf);

	__asm__ volatile("dsb SY\nisb\n");

	// Set Timer Value
	printk(LOG_NOTE, "CNTHP timer value set...\n");
	armv8_CNTHP_TVAL_EL2_TimerValue_wrf(armv8, 0x1000);
}

void timer_enable(armv8_t *armv8, gicr_t *gic_redist)
{
	armv8_CNTHP_CTL_EL2_t cnthp_ctl = (armv8_CNTHP_CTL_EL2_t)0x0;

	printk(LOG_NOTE, "CNTHP Interrupt Mask unmask...\n");
	cnthp_ctl = armv8_CNTHP_CTL_EL2_IMASK_insert(cnthp_ctl, 0x0);

	__asm__ volatile("dsb SY\nisb\n");

	printk(LOG_NOTE, "CNTHP interrupt enable...\n");
	cnthp_ctl = armv8_CNTHP_CTL_EL2_ENABLE_insert(cnthp_ctl, 0x1);

	armv8_CNTHP_CTL_EL2_wr(armv8, cnthp_ctl);
}
