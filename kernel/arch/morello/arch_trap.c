#include <trap.h>
#include <standalone/printf.h>
#include <mackerels/sysreg/sysreg.h>
#include <mackerels/armv8_dev.h>
#include <bf/logging.h>
#include <system_interface/system_interface.h>
#include <elf_helpers.h>
#include <handlers.h>

/**
 * Entry function that saves caller-saved registers and then
 * jumps to a C-function trap vector.
 * 
 * The C function can then save the remaining callee-saved registers.
*/
void __attribute__((naked)) trap_vector_entry()
{
	// Save registers I'm pretty sure are callee-saved
	// See https://github.com/ARM-software/abi-aa/blob/2982a9f3b512a5bfdc9e3fea5d3b298f9165c36b/aapcs64-morello/aapcs64-morello.rst
	__asm__ volatile("sub csp, csp, 512\n"
			 "str clr,[csp, 0  ]\n"
			 "str c19,[csp, 16 ]\n"
			 "str c20,[csp, 32 ]\n"
			 "str c21,[csp, 48 ]\n"
			 "str c22,[csp, 64 ]\n"
			 "str c23,[csp, 80 ]\n"
			 "str c24,[csp, 96 ]\n"
			 "str c25,[csp, 112]\n"
			 "str c26,[csp, 128]\n"
			 "str c27,[csp, 144]\n"
			 "str c28,[csp, 160]\n"
			 "str c30,[csp, 176]\n");
	__asm__ volatile("bl timer_interrupt_handler");
	__asm__ volatile("ldr clr, [csp, 0]\n"
			 "ldr c19,[csp, 16 ]\n"
			 "ldr c20,[csp, 32 ]\n"
			 "ldr c21,[csp, 48 ]\n"
			 "ldr c22,[csp, 64 ]\n"
			 "ldr c23,[csp, 80 ]\n"
			 "ldr c24,[csp, 96 ]\n"
			 "ldr c25,[csp, 112]\n"
			 "ldr c26,[csp, 128]\n"
			 "ldr c27,[csp, 144]\n"
			 "ldr c28,[csp, 160]\n"
			 "ldr c30,[csp, 176]\n"
			 "add csp, csp, 512\n");
	__asm__ volatile("eret");
}

void trap_err_simple();
void trap_err_simple()
{
#define BUF_SIZE 32000
	char buf[BUF_SIZE];

	armv8_t armv8_struct;
	armv8_t *armv8 = &armv8_struct;
	armv8_initialize(armv8, 0x0);

	//uint32_t iss2 = armv8_ESR_EL2_ISS2_rdf(armv8);
	//uint32_t iss = armv8_ESR_EL2_ISS_rdf(armv8);
	//uint8_t il = armv8_ESR_EL2_IL_rdf(armv8);
	uint8_t ec = armv8_ESR_EL2_EC_rdf(armv8);

	printk(LOG_ERR, "Trap w/: 0x%x\n\n", ec);
	armv8_ESR_EL2_pr(buf, BUF_SIZE, armv8);
	printf("%s\n", buf);

	switch (ec) {
	case 0x25: // 0b100101

		printk(LOG_PANIC, "Data Abort taken to EL2.\n");

		size_t fault_va = (size_t)trap_return_pc_read();
		printk(LOG_PANIC, " VA: %p\n", fault_va);

		size_t kernel_elf_fault_offset =
			fault_va - (size_t)elf_current_get();
		size_t kernel_elf_relative_va = elf_offset_to_va(
			elf_current_get(), kernel_elf_fault_offset);
		printk(LOG_PANIC, " VA(ELF): %p\n", kernel_elf_relative_va);

		system_terminate();

	default:
		printk(LOG_PANIC, "Unknown Trap EC: %x", ec);
		system_terminate();
		break;
		;
	}
}

void trap_from_lower_el(void);
void trap_from_lower_el(void)
{
	printk(LOG_NOTE, "Trap from lower EL\n");
	timer_interrupt_handler();
}

/**
 * Returns from a trap/interrupt
 */
void trap_return()
{
	__asm__ volatile("eret");
}

/**
 * 
 */
uint64_t interrupt_reason_get()
{
	return 0x0;
}

capability trap_return_pc_read()
{
	capability return_ptr;
	__asm__ volatile("mrs %[return_ptr], CELR_EL2"
			 : [return_ptr] "=C"(return_ptr));
	return return_ptr;
}

void trap_return_pc_write(capability return_ptr)
{
	__asm__ volatile("msr CELR_EL2, %[return_ptr]"
			 :
			 : [return_ptr] "C"(return_ptr));
}

struct trap_vector_data *trap_vector_data_load_from_context(void)
{
	return &global_trap_vector_data;
}
