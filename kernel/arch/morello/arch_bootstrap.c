#include <stddef.h>
#include <stdint.h>
#include <cheri/cheri_interface.h>
#include <bootstrap.h>
#include <system_interface/system_interface.h>
#include <standalone/printf.h>
#include <elf.h>
#include <demo.h>
#include <bf/logging.h>
#include <mackerel_support/mackerel.h>
#include <mackerels/sysreg/sysreg.h>
#include <mackerels/armv8_dev.h>
#include <mackerels/gicd_dev.h>
#include <mackerels/gicr_dev.h>

#include <cheri/elf_reloc_self.h>
#include <interrupt.h>
#include <process.h>
#include <trap.h>

#define PLAT_ARM_BOOT_UART_BASE 0x2A400000

extern capability proc0;
extern capability p0_data;
extern capability proc1;
extern capability p1_data;

void arch_bootstrap(const Elf64_Ehdr *elf, const Elf64_Dyn *elf_dyn)
{
	void *code_cap = cheri_pcc_get();
	void *data_cap = cheri_ddc_get();
	novm_elf_reloc_self(elf_dyn, elf, data_cap, code_cap);

	volatile char *__capability ptr2 =
		cheri_address_set(cheri_ddc_get(), PLAT_ARM_BOOT_UART_BASE);
	*ptr2 = '\n';
	*ptr2 = '\n';
	*ptr2 = '\n';
	*ptr2 = 'h';

	printk(LOG_NOTE, "[Cheriettte] hallo\n");

	printk(LOG_NOTE,
	       "CHERI capability mode on exception entry to EL2 set...\n");
	__asm__ volatile("mrs x1, CCTLR_EL2\n\t"
			 "orr x1, x1, #(0x1<<5)\n\t"
			 "msr CCTLR_EL2, x1\n\t"
			 :
			 :
			 : "x1");

#ifdef DEMO_ENABLE
	demo();
#endif

	//arch_interrupts_disable();

	// Exception return address
	printk(LOG_NOTE, "Exception return address set...\n");
	__asm__ volatile("msr CELR_EL2, %[val]\n"
			 "isb \n"
			 :
			 : [val] "C"(cheri_address_set(cheri_pcc_get(),
						       (size_t)&proc0)));

	mackerel_addr_t gic_dist_addr =
		(mackerel_addr_t)cheri_address_set(cheri_pcc_get(), 0x30000000);
	mackerel_addr_t gic_redist_VLPI_addr =
		(mackerel_addr_t)cheri_address_set(cheri_pcc_get(), 0x42);
	mackerel_addr_t gic_redist_RD_addr =
		(mackerel_addr_t)cheri_address_set(cheri_pcc_get(), 0x300C0000);
	mackerel_addr_t gic_redist_SGI_addr =
		(mackerel_addr_t)cheri_address_set(cheri_pcc_get(), 0x300D0000);

	gicd_t gic_dist;
	//gicd_initialize(&gic_dist, (mackerel_addr_t)0x30000000);
	gicd_initialize(&gic_dist, gic_dist_addr);

	gicr_t gic_redist;
	gicr_initialize(&gic_redist, gic_redist_VLPI_addr, gic_redist_SGI_addr,
			gic_redist_RD_addr);

	armv8_t armv8;
	armv8_initialize(&armv8, 0x0);

	__asm__ volatile("hlt 0x5");

	capability vector_base_address = cheri_ddc_get();
	vector_base_address = cheri_address_set(vector_base_address, 0x0);

	gic_init(vector_base_address, &gic_dist, &gic_redist, &armv8);

	cpu_init(vector_base_address, &gic_dist, &gic_redist, &armv8);

	printk(LOG_NOTE, "Setting up proc0 and prov1...");

	struct trap_vector_data *trap_vector_data_cap =
		&global_trap_vector_data;
	//trap_vector_data_cap = cheri_address_set(
	//	trap_vector_data_cap, (uint64_t)&global_trap_vector_data);
	//trap_vector_data_cap = cheri_bounds_set(
	//	trap_vector_data_cap,
	//	1000 * (uint64_t)sizeof(struct trap_vector_data));

	trap_vector_data_cap->cur_proc_index = 0;
	trap_vector_data_cap->num_proc = 2;

	process_init(&trap_vector_data_cap->processes[0], (uint64_t)&proc0,
		     (uint64_t)&p0_data);
	process_init(&trap_vector_data_cap->processes[1], (uint64_t)&proc1,
		     (uint64_t)&p1_data);
	trap_vector_data_cap->cur_proc =
		&trap_vector_data_cap
			 ->processes[trap_vector_data_cap->cur_proc_index];

	timer_init(&armv8, &gic_redist);

	capability bootstrap_cap = trap_vector_data_cap->cur_proc->pcc;

	printk(LOG_NOTE, "Setting trap return capability...\n");
	printk(LOG_NOTE, " --> %p\n", bootstrap_cap);
	trap_return_pc_write(bootstrap_cap);

	timer_enable(&gic_redist);

	trap_return();

	printk(LOG_NOTE, "trap return failed!\n");

	// Using physical timer CNTHP
	// CNTHP_CTL_EL2
	//uint64_t ctl_mask = 0x3;
	//__asm__ volatile("msr CNTHP_TVAL_EL2, %0" : : "r"(0x1000));
	//__asm__ volatile("msr CNTHP_CTL_EL2, %0" : : "r"(ctl_mask));

	//char **pptr = cheri_address_set(cheri_ddc_get(), 0xFE000000);
	//char *ptr = cheri_address_set(cheri_ddc_get(), 0xFE0000F0);
	//*ptr = 0xE;
	//*pptr = ptr;
	//printf("%x", **pptr);

	system_terminate();
}

/*
void __attribute__((naked)) el1_entry() {
	__asm__ volatile("msr ELR_EL1, %0" : : "C"(proc0))
	__asm__ volatile("eret");
}
*/
