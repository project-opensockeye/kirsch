
#include <process.h>

void process_stack_init(capability *csp_target)
{
	__asm("cspecialw mscratchc, csp");
	cheri_cap_mov_reg_into_cvar(csp, csp_target);
	__asm__ volatile("cincoffset csp, csp, -512\n"
			 "csc c0, 0(csp) \n"
			 "csc c0, 16(csp) \n"
			 "csc c0, 32(csp) \n"
			 "csc c0, 48(csp) \n"
			 "csc c0, 64(csp) \n"
			 "csc c0, 80(csp) \n"
			 "csc c0, 96(csp) \n"
			 "csc c0, 112(csp) \n"
			 "csc c0, 128(csp) \n"
			 "csc c0, 144(csp) \n"
			 "csc c0, 160(csp) \n"
			 "csc c0, 176(csp) \n"
			 "csc c0, 192(csp) \n");
	__asm("cspecialr csp, mscratchc");
}
