#include <trap.h>
#include <handlers.h>
#include <elf_helpers.h>
#include <csr.h>
#include <machine/riscvreg.h>
#include <machine/cherireg.h>
#include <system_interface/system_interface.h>
#include <bf/logging.h>

static void trap_vector(void);

static const char *exccode_descriptions[] = {
	[SCAUSE_INST_MISALIGNED] = "Misaligned instruction",
	[SCAUSE_INST_ACCESS_FAULT] = "Instruction access fault",
	[SCAUSE_ILLEGAL_INSTRUCTION] = "Illegal instruction",
	[SCAUSE_BREAKPOINT] = "Breakpoint",
	[SCAUSE_LOAD_MISALIGNED] = "Load misaligned",
	[SCAUSE_LOAD_ACCESS_FAULT] = "Load access fault",
	[SCAUSE_STORE_MISALIGNED] = "Store misaligned",
	[SCAUSE_STORE_ACCESS_FAULT] = "Store access fault",
	[SCAUSE_ECALL_USER] = "Ecall user",
	[SCAUSE_ECALL_SUPERVISOR] = "Ecall supervisor",
	[SCAUSE_INST_PAGE_FAULT] = "Instruction page fault",
	[SCAUSE_LOAD_PAGE_FAULT] = "Load page fault",
	[SCAUSE_STORE_PAGE_FAULT] = "Store page fault",
#if __has_feature(capabilities)
	[SCAUSE_LOAD_CAP_PAGE_FAULT] = "Load capability page fault",
	[SCAUSE_STORE_AMO_CAP_PAGE_FAULT] = "Store AMO capability page fault",
#endif
	[SCAUSE_CHERI] = "CHERI Exception",
};

// Source of CHERI_EXCCODE entries:
// https://github.com/CTSRD-CHERI/cheribsd/blob/1a050e5fa064f5f6bfa10f0c31ccaf6413da7dda/sys/riscv/cheri/cheri_exception.c
static const char *exccode_descriptions_cheri[] = {
	[CHERI_EXCCODE_NONE] = "none",
	[CHERI_EXCCODE_LENGTH] = "length violation",
	[CHERI_EXCCODE_TAG] = "tag violation",
	[CHERI_EXCCODE_SEAL] = "seal violation",
	[CHERI_EXCCODE_TYPE] = "type violation",
	[CHERI_EXCCODE_PERM_USER] = "user-defined permission violation",
	[CHERI_EXCCODE_IMPRECISE] = "bounds cannot be represented precisely",
	[CHERI_EXCCODE_UNALIGNED_BASE] = "Unaligned PCC base",
	[CHERI_EXCCODE_GLOBAL] = "global violation",
	[CHERI_EXCCODE_PERM_EXECUTE] = "permit execute violation",
	[CHERI_EXCCODE_PERM_LOAD] = "permit load violation",
	[CHERI_EXCCODE_PERM_STORE] = "permit store violation",
	[CHERI_EXCCODE_PERM_LOADCAP] = "permit load capability violation",
	[CHERI_EXCCODE_PERM_STORECAP] = "permit store capability violation",
	[CHERI_EXCCODE_STORE_LOCALCAP] =
		"permit store local capability violation",
	[CHERI_EXCCODE_PERM_SEAL] = "permit seal violation",
	[CHERI_EXCCODE_SYSTEM_REGS] = "access system registers violation",
	[CHERI_EXCCODE_PERM_CINVOKE] = "permit cinvoke violation",
	[CHERI_EXCCODE_PERM_UNSEAL] = "permit unseal violation",
	[CHERI_EXCCODE_PERM_SET_CID] = "permit CSetCID violation",
};

/**
 * Entry function that saves caller-saved registers and then
 * jumps to a C-function trap vector.
 * 
 * The C function can then save the remaining callee-saved registers.
*/
void __attribute__((naked)) trap_vector_entry(void)
{
	__asm__ volatile(
		// Offset CSP
		"cincoffset csp, csp, -512\n"
		// Return address
		"csc cra, 0(csp) \n"
		// Temporary x5-x7
		"csc ct0, 16(csp) \n"
		"csc ct1, 32(csp) \n"
		"csc ct2, 48(csp) \n"
		// Argument x10-x17
		"csc ca0, 64(csp) \n"
		"csc ca1, 80(csp) \n"
		"csc ca2, 96(csp) \n"
		"csc ca3, 112(csp) \n"
		"csc ca4, 128(csp) \n"
		"csc ca5, 144(csp) \n"
		"csc ca6, 160(csp) \n"
		"csc ca7, 176(csp) \n"
		// Temporary x28-x31
		"csc ct3, 192(csp) \n"
		"csc ct4, 208(csp) \n"
		"csc ct5, 224(csp) \n"
		"csc ct6, 240(csp) \n");
	__asm__ volatile("cjalr %0\n" : : "C"(&trap_vector));
	__asm__ volatile(
		// Return address
		"clc cra, 0(csp) \n"
		// Temporary x5-x7
		"clc ct0, 16(csp) \n"
		"clc ct1, 32(csp) \n"
		"clc ct2, 48(csp) \n"
		// Argument x10-x17
		"clc ca0, 64(csp) \n"
		"clc ca1, 80(csp) \n"
		"clc ca2, 96(csp) \n"
		"clc ca3, 112(csp) \n"
		"clc ca4, 128(csp) \n"
		"clc ca5, 144(csp) \n"
		"clc ca6, 160(csp) \n"
		"clc ca7, 176(csp) \n"
		// Temporary x28-x31
		"clc ct3, 192(csp) \n"
		"clc ct4, 208(csp) \n"
		"clc ct5, 224(csp) \n"
		"clc ct6, 240(csp) \n"
		"cincoffset csp, csp, 512\n");
	__asm__ volatile("mret");
}

static void trap_vector(void)
{
	uint64_t interrupt_cause = interrupt_reason_get();

	printk(LOG_NOTE, "Interrupted with 0x%x (%d)\n", interrupt_cause,
	       interrupt_cause);

	if ((interrupt_cause & SCAUSE_INTR) == 0) {
		// Exception handling

		printk(LOG_ERR, "Exception: %s\n",
		       exccode_descriptions[interrupt_cause]);

		size_t fault_va = (size_t)trap_return_pc_read();
		printk(LOG_ERR, "     VA: %p\n", fault_va);

		size_t kernel_elf_fault_offset =
			fault_va - (size_t)elf_current_get();
		size_t kernel_elf_relative_va = elf_offset_to_va(
			elf_current_get(), kernel_elf_fault_offset);
		printk(LOG_ERR, " VA(ELF): %p\n", kernel_elf_relative_va);

		if (interrupt_cause == SCAUSE_CHERI) {
			size_t cheri_cause = TVAL_CAP_CAUSE(csr_read(mtval));
			printk(LOG_NOTE, " CHERI exception with 0x%x (%d):\n",
			       cheri_cause, cheri_cause);

			printk(LOG_ERR, "   %s\n",
			       exccode_descriptions_cheri[cheri_cause]);
		}

		system_terminate();
	} else {
		// Interrupt handling
		switch (interrupt_cause) {
		case TIMER_INTERRUPT_MASK:

			timer_interrupt_handler();

			break;
			;
		default:
			printk(LOG_ERR, "Unknown interrupt ID %d\n",
			       interrupt_cause);
			system_terminate();

			break;
			;
		}
	}
}

/**
 * Returns from a trap/interrupt
 */
void trap_return(void)
{
	__asm__ volatile("mret");
}

uint64_t interrupt_reason_get(void)
{
	uint64_t interrupt_cause;
	__asm__ volatile("csrr %0, mcause" : "+r"(interrupt_cause));
	return interrupt_cause;
}

capability trap_return_pc_read(void)
{
	capability return_ptr;
	cheri_special_read(mepcc, return_ptr);
	return return_ptr;
}
void trap_return_pc_write(capability return_ptr)
{
	cheri_special_write(mepcc, return_ptr);
}

struct trap_vector_data *trap_vector_data_load_from_context(void)
{
	struct trap_vector_data *trap_vector_data;
	cheri_special_read(mtdc, trap_vector_data);
	return trap_vector_data;
}
