
#include <stddef.h>
#include <stdint.h>
#include <elf.h>

#include <cheri/cheri_interface.h>
#include <cheri/cheri_helpers.h>
#include <cheri/cheri_defines.h>
#include <cheri/crt_init_globals.h>
#include <timer.h>
#include <trap.h>

#include <sail_emulator/interface_functions.h>

#include <encoding.h>
#include <bf/logging.h>
#include <elf_helpers.h>
#include <bootstrap.h>

#include <csr.h>
#include <riscvreg.h>
#include <system_interface/system_interface.h>

/**
 * Init RNMI (Resumable Non-Maskable Interrupts)
 *
 * Provides a separate copy of mcause, mepc, etc. prefixed by "mn" instead
 * of "m".
 */
inline void __attribute__((always_inline)) init_rnmi(void)
{
	csr_write_skip_reg(mstatus, csr_read(mstatus) | MNSTATUS_NMIE);
}

/**
 * Init SATP (Virtual-to-physical translation for supervisor mode)
 *
 * Writing 0 disables this.
 */
inline void __attribute__((always_inline)) init_satp(void)
{
	csr_write_skip(satp, 0);
}

inline void __attribute__((always_inline)) init_pmp(void)
{
	uint64_t op =
		((uint64_t)1 << (31 + (__riscv_xlen / 64) * (53 - 31))) - 1;
	uint64_t opts = PMP_NAPOT | PMP_R | PMP_W | PMP_X;

	// Open up the PMP (if we have one?)
	// Sets up PMP to permit all accesses
	csr_write_skip_reg(pmpaddr0, op);
	csr_write_skip_reg(pmpcfg0, opts);
}

inline void __attribute__((always_inline)) disable_trap_delegation(void)
{
	// Disable trap delegation.
	csr_write_imm(mie, 0);

	csr_write_skip(medeleg, 0);
	csr_write_skip(mideleg, 0);
}

void timer_seal(struct trap_vector_data *trap_vector_data_cap)
{
	// Seal the timer function
	//  Lifted from
	//  https://github.com/five-embeddev/riscv-scratchpad/blob/master/baremetal-startup-c/src/timer.h
#define RISCV_MTIMECMP_ADDR (0x2000000 + 0x4000)
#define RISCV_MTIME_ADDR (0x2000000 + 0xBFF8)

#define mtime RISCV_MTIME_ADDR
#define mtimecmp RISCV_MTIMECMP_ADDR

	//  Sealing cap
	void *__capability seal_otype_cap = cheri_ddc_get();
	seal_otype_cap = cheri_address_set(seal_otype_cap, 1);

	struct set_timer_data *__capability set_timer_data_cap =
		(struct set_timer_data *)&set_timer_data;
	set_timer_data_cap = cheri_bounds_set(set_timer_data_cap,
					      sizeof(struct set_timer_data));
	// FIXME: set perms (note, different perms needed now from later when we seal
	// it), also, only used locally

	// Code cap
	void *__capability set_timer_cap = (void *)&set_timer;
	set_timer_cap = cheri_bounds_set(set_timer_cap, set_timer_size);

	uint64_t set_timer_perms = CAP_PERMIT_EXECUTE | CAP_PERMIT_CINVOKE;
	set_timer_cap = cheri_perms_and(set_timer_cap, set_timer_perms);

	void *__capability set_timer_sealed_cap =
		cheri_seal(set_timer_cap, seal_otype_cap);

	// Data cap 1
	void *__capability mtime_cap = cheri_ddc_get();
	mtime_cap = cheri_address_set(mtime_cap, mtime);
	mtime_cap = cheri_bounds_set(mtime_cap, 8);
	mtime_cap = cheri_perms_and(mtime_cap, CAP_PERMIT_LOAD);
	set_timer_data_cap->mtime_cap = mtime_cap;

	// Data cap 2
	void *__capability mtimecmp_cap = cheri_ddc_get();
	mtimecmp_cap = cheri_address_set(mtimecmp_cap, mtimecmp);
	mtimecmp_cap = cheri_bounds_set(mtimecmp_cap, 8);
	mtimecmp_cap = cheri_perms_and(mtimecmp_cap, CAP_PERMIT_STORE);
	set_timer_data_cap->mtimecmp_cap = mtimecmp_cap;

	// Sealed caps can't have execute (but narrow much more)
	uint64_t perms = CAP_PERMIT_LOAD | CAP_PERMIT_LOAD_CAPABILITY |
			 CAP_PERMIT_CINVOKE;
	set_timer_data_cap = cheri_perms_and(set_timer_data_cap, perms);
	void *__capability timer_data_sealed =
		cheri_seal(set_timer_data_cap, seal_otype_cap);
	// Timer function sealed

	// Save it for trap_handler
	trap_vector_data_cap->set_timer = set_timer_sealed_cap;
	trap_vector_data_cap->set_timer_data = timer_data_sealed;
}

extern capability proc0;
extern capability p0_data;
extern capability proc1;
extern capability p1_data;

void arch_bootstrap(size_t elf_address, size_t cap_relocs_addr_start,
		    size_t cap_relocs_addr_end)
{
	Elf_Ehdr *elf = cheri_pcc_get();
	elf = cheri_address_set(elf, elf_address);

	if (!IS_ELF(*elf)) {
		__asm__ volatile("mv a2, a3");
		__builtin_trap();
	}

	// Offset by ELF
	//cap_relocs_addr_start = elf_address + elf_va_to_offset(elf, cap_relocs_addr_start);
	//cap_relocs_addr_end = elf_address + elf_va_to_offset(elf, cap_relocs_addr_end);

	struct capreloc *__capability cap_relocs_start = cheri_pcc_get();
	struct capreloc *__capability cap_relocs_end = cheri_pcc_get();
	cap_relocs_start =
		cheri_address_set(cap_relocs_start, cap_relocs_addr_start);
	cap_relocs_end = cheri_address_set(cap_relocs_end, cap_relocs_addr_end);

	crt_init_globals(elf, cap_relocs_start, cap_relocs_end);

	printk(LOG_NOTE, "Testing putc complete\n");
	printk(LOG_NOTE, "Testing printk complete\n");

	printk(LOG_NOTE, "Architecture specific bootstrapping...\n");

	//__asm__ volatile ("CLEAR_REGISTERS");

	// RNMI not available in OCAML simulator
	init_rnmi();

	init_satp();

	// PMP not available in OCAML simulator
	init_pmp();

	disable_trap_delegation();

	// Make sure we have a powerful capability when we take traps
	// And point it to the t1 save area
	struct trap_vector_data *trap_vector_data_cap =
		&global_trap_vector_data;
	//trap_vector_data_cap = cheri_address_set(
	//	trap_vector_data_cap, (uint64_t)&global_trap_vector_data);
	//trap_vector_data_cap = cheri_bounds_set(
	//	trap_vector_data_cap,
	//	1000 * (uint64_t)sizeof(struct trap_vector_data));

	trap_vector_data_cap->cur_proc_index = 0;
	trap_vector_data_cap->num_proc = 2;

	process_init(&trap_vector_data_cap->processes[0], (uint64_t)&proc0,
		     (uint64_t)&p0_data);
	process_init(&trap_vector_data_cap->processes[1], (uint64_t)&proc1,
		     (uint64_t)&p1_data);
	trap_vector_data_cap->cur_proc =
		&trap_vector_data_cap
			 ->processes[trap_vector_data_cap->cur_proc_index];

	// Set up the timer
	// Don't need to enable machine mode interrupts because they will be enabled
	// when we move to user mode
	// Enable timer interrupt
	csr_write(mie, MIE_MTIE);
	// FIXME: set first timer interrupt?
	// Timer set up

	// WE ARE READY TO GO!

	timer_seal(trap_vector_data_cap);

	// Is store local capability needed?
	uint64_t mtdc_mask =
		CAP_PERMIT_LOAD | CAP_PERMIT_STORE |
		CAP_PERMIT_STORE_CAPABILITY | CAP_PERMIT_LOAD_CAPABILITY |
		CAP_PERMIT_STORE_LOCAL_CAPABILITY | CAP_PERMIT_CINVOKE;
	trap_vector_data_cap = cheri_perms_and(trap_vector_data_cap, mtdc_mask);
	cheri_special_write(mtdc, trap_vector_data_cap);

	// Seal a capability to update the timer
	// FIXME: DDC is too powerful
	// void* __capability ddc2 = cheri_ddc_get();

	extern void trap_vector;
	// volatile void* __capability trap_vector_cap = (void*) &trap_vector;
	void *__capability trap_vector_cap = cheri_pcc_get();
	trap_vector_cap = cheri_address_set(trap_vector_cap,
					    (uint64_t)&trap_vector_entry);

	// Put it in capability mode
	trap_vector_cap = cheri_flags_set(trap_vector_cap, 1);

	// Narrow permissions
	uint64_t trap_vector_perms =
		CAP_PERMIT_EXECUTE | CAP_PERMIT_LOAD_CAPABILITY |
		CAP_PERMIT_LOAD | CAP_PERMIT_ACCESS_SYSTEM_REGISTERS;
	trap_vector_cap = cheri_perms_and(trap_vector_cap, trap_vector_perms);

	// Set length
	// TODO: FIX / Dynamic?
	//trap_vector_cap = cheri_bounds_set(trap_vector_cap, sizeof(trap_vector_c));

	// Set the trap's:w
	cheri_special_write(mtcc, trap_vector_cap);

	// Only enable FPU
	csr_write(mstatus, 0x1000);

	capability bootstrap_cap = trap_vector_data_cap->cur_proc->pcc;

	bootstrap(elf);

	trap_return_pc_write(bootstrap_cap);

	capability fun = trap_vector_data_cap->set_timer;
	capability data = trap_vector_data_cap->set_timer_data;
	// Update timer
	cheri_cinvoke_asm_1(fun, data, 200);

	trap_return();
}
