
#include <timer.h>

struct set_timer_data set_timer_data;

void set_timer(int offset)
{
	__asm__ volatile(
		// Sets the hardware timer on the CLINT device
		// Expects the offset in a0 (== x10)
		// CInvoke will leave the data cap in c31 (== ct6)
		"clc ct1, 0(c31)        \n"
		"cld t1, 0(ct1)         \n"
		"add t1, t1, %0         \n"
		"clc ct2, 16(c31)       \n"
		"csd t1, 0(ct2)         \n"
		// Don't give away caps
		"li x31, 0              \n"
		"li t2, 0               "
		:
		: "r"(offset)
		: "ct1", "t1", "ct2", "t2", "c31", "x31");
}
