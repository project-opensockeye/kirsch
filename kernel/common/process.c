
#include <process.h>
#include <cheri/cheri_interface.h>
#include <cheri/cheri_helpers.h>
#include <cheri/cheri_defines.h>

void process_init(struct process_descriptor *pd, uint64_t start_addr,
		  uint64_t data_addr)
{
	//capability process_pcc; //= cheri_pcc_get();
	//process_pcc = cheri_address_set(process_pcc, start_addr);
	//process_pcc = cheri_flags_set(process_pcc, 1);

	pd->first_start = true;
	//pd->pcc = process_pcc;
	pd->pcc = cheri_pcc_read();
	pd->pcc = cheri_address_set(pd->pcc, start_addr);
	pd->pcc = cheri_perms_and(
		pd->pcc, CAP_PERMIT_STORE_CAPABILITY |
				 CAP_PERMIT_STORE_LOCAL_CAPABILITY |
				 CAP_PERMIT_LOAD_CAPABILITY | CAP_PERMIT_LOAD |
				 CAP_PERMIT_EXECUTE | CAP_PERMIT_CINVOKE);
	pd->csp = &pd->stack;
	// TODO: Fix offsets
	pd->registers.cs0 = cheri_ddc_get();
	pd->registers.cs0 = cheri_address_set(pd->registers.cs0, data_addr);

	process_stack_init(pd->csp);
}
