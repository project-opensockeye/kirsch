
#include <cheri/crt_init_globals.h>
#include <cheri/cheri_helpers.h>
#include <cheri/cheriintrin.h>
#include <stdint.h>
#include <stddef.h>
#include <elf_helpers.h>

// The following code is taken from
// https://github.com/CTSRD-CHERI/llvm-project/blob/master/lld/test/ELF/cheri/__cap_relocs/Inputs/crt_init_globals.c

static const uint64_t function_reloc_flag = 1ULL << 63;
static const uint64_t function_pointer_permissions =
	~0 & ~__CHERI_CAP_PERMISSION_PERMIT_STORE_CAPABILITY__ &
	~__CHERI_CAP_PERMISSION_PERMIT_STORE__;
static const uint64_t global_pointer_permissions =
	~0 & ~__CHERI_CAP_PERMISSION_PERMIT_EXECUTE__;

__attribute__((weak)) extern struct capreloc __start___cap_relocs;
__attribute__((weak)) extern struct capreloc __stop___cap_relocs;

void crt_init_globals(const Elf_Ehdr *elf,
		      struct capreloc *__start___cap_relocs,
		      struct capreloc *__stop___cap_relocs)
{
	void *gdc = __builtin_cheri_global_data_get();
	void *pcc = __builtin_cheri_program_counter_get();

	gdc = __builtin_cheri_perms_and(gdc, global_pointer_permissions);
	pcc = __builtin_cheri_perms_and(pcc, function_pointer_permissions);
	for (struct capreloc *reloc = __start___cap_relocs;
	     reloc < __stop___cap_relocs; reloc++) {
		_Bool isFunction = (reloc->permissions & function_reloc_flag) ==
				   function_reloc_flag;
		size_t dest_addr =
			(size_t)elf +
			elf_va_to_offset(elf, reloc->capability_location);
		void **dest = __builtin_cheri_offset_set(gdc, dest_addr);
		void *base = isFunction ? pcc : gdc;
		size_t src_addr =
			(size_t)elf + elf_va_to_offset(elf, reloc->object);
		void *src = __builtin_cheri_offset_set(base, src_addr);
		if (!isFunction && (reloc->size != 0)) {
			src = __builtin_cheri_bounds_set(src, reloc->size);
		}
		src = __builtin_cheri_offset_increment(src, reloc->offset);
		*dest = src;
	}
}
