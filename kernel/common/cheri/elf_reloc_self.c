
#include <stddef.h>
#include <stdint.h>
#include <cheri/elf_reloc_self.h>
#include <cheri/cheri_interface.h>
#include <elf.h>
#include <elf_helpers.h>

// https://github.com/CTSRD-CHERI/cheribsd/blob/9322a79dea58f31151d2345eeca79d398cbb8e63/sys/arm64/arm64/elf_machdep.c
/**
 * Decodes a single capability fragment, as described in the ELF dynamic section.
 * Extracts the address of the capability, the size, and the permissions.
 */
static void __nosanitizecoverage decode_fragment(Elf_Addr *fragment,
						 Elf_Addr relocbase,
						 Elf_Addr *addrp,
						 Elf_Addr *sizep,
						 uint8_t *permsp);
static void __nosanitizecoverage decode_fragment(Elf_Addr *fragment,
						 Elf_Addr relocbase,
						 Elf_Addr *addrp,
						 Elf_Addr *sizep,
						 uint8_t *permsp)
{
	*addrp = relocbase + fragment[0];
	*sizep = fragment[1] & ((1UL << (8 * sizeof(Elf_Addr) - 8)) - 1);
	*permsp = fragment[1] >> (8 * sizeof(Elf_Addr) - 8);
}

// https://github.com/CTSRD-CHERI/cheribsd/blob/9322a79dea58f31151d2345eeca79d398cbb8e63/sys/arm64/arm64/elf_machdep.c
/**
 * Builds a capability from a set of parameters. 
 * 
 * If the target cap is executable, then the code capbility is taken as the 
 * base cap, otherwise the data capability is chosen.
 */
static uintcap_t __nosanitizecoverage
build_reloc_cap(Elf_Addr addr, Elf_Addr size, uint8_t perms, Elf_Addr offset,
		void *__capability data_cap, const void *__capability code_cap);
static uintcap_t __nosanitizecoverage
build_reloc_cap(Elf_Addr addr, Elf_Addr size, uint8_t perms, Elf_Addr offset,
		void *__capability data_cap, const void *__capability code_cap)
{
	uintcap_t cap;

	cap = perms == MORELLO_FRAG_EXECUTABLE ? (uintcap_t)code_cap :
						 (uintcap_t)data_cap;
	cap = cheri_address_set(cap, addr);

	if (perms == MORELLO_FRAG_EXECUTABLE || perms == MORELLO_FRAG_RODATA) {
		cap = cheri_perms_clear(cap,
					CHERI_PERM_SEAL | CHERI_PERM_STORE |
						CHERI_PERM_STORE_CAP |
						CHERI_PERM_STORE_LOCAL_CAP);
	}
	if (perms == MORELLO_FRAG_RWDATA || perms == MORELLO_FRAG_RODATA) {
		cap = cheri_perms_clear(cap,
					CHERI_PERM_SEAL | CHERI_PERM_EXECUTE);
		cap = cheri_bounds_set(cap, size);
	}
	cap += offset;
	if (perms == MORELLO_FRAG_EXECUTABLE) {
		cap = cheri_sentry_create(cap);
	}
	//KASSERT(cheri_gettag(cap) != 0,
	//    ("Relocation produce invalid capability %#lp",
	//    (void * __capability)cap));
	if (cheri_tag_get(cap) == 0) {
		__builtin_trap();
		//    ("Relocation produce invalid capability %#lp",
		//    (void * __capability)cap));
	}
	return (cap);
}

// https://github.com/CTSRD-CHERI/cheribsd/blob/9322a79dea58f31151d2345eeca79d398cbb8e63/sys/arm64/arm64/elf_machdep.c
/**
 * Builds a capability from a single fragment in the ELF dynamic
 * section.
 */
static uintcap_t __nosanitizecoverage build_cap_from_fragment(
	const Elf_Ehdr *elf, Elf_Addr *fragment, Elf_Addr relocbase,
	Elf_Addr offset, void *__capability data_cap,
	const void *__capability code_cap);
static uintcap_t __nosanitizecoverage build_cap_from_fragment(
	const Elf_Ehdr *elf, Elf_Addr *fragment, Elf_Addr relocbase,
	Elf_Addr offset, void *__capability data_cap,
	const void *__capability code_cap)
{
	Elf_Addr addr, size;
	uint8_t perms;

	decode_fragment(fragment, 0, &addr, &size, &perms);
	addr = (size_t)elf + elf_va_to_offset(elf, addr);
	return (build_reloc_cap(addr, size, perms, offset, data_cap, code_cap));
}

// https://github.com/CTSRD-CHERI/cheribsd/blob/9322a79dea58f31151d2345eeca79d398cbb8e63/sys/arm64/arm64/elf_machdep.c
void __nosanitizecoverage novm_elf_reloc_self(const Elf64_Dyn *dynp,
					      const Elf_Ehdr *elf,
					      void *data_cap,
					      const void *code_cap)
{
	const Elf_Rela *rela = NULL, *rela_end;
	Elf_Addr *fragment;
	uintptr_t cap;
	size_t rela_size = 0;

	for (; dynp->d_tag != DT_NULL; dynp++) {
		switch (dynp->d_tag) {
		case DT_RELA:
			//rela = (const Elf_Rela *)((const char *)data_cap +
			//    elf_va_to_offset(elf, dynp->d_un.d_ptr) + (const size_t)elf);
			rela = cheri_address_set(
				data_cap,
				elf_va_to_offset(elf, dynp->d_un.d_ptr) +
					(const size_t)elf);
			break;
		case DT_RELASZ:
			rela_size = dynp->d_un.d_val;
			break;
		}
	}

	if (rela == NULL || rela_size == 0) {
		__builtin_trap();
	}

	rela = cheri_bounds_set(rela, rela_size);
	rela_end = (const Elf_Rela *)((const char *)rela + rela_size);

	for (; rela < rela_end; rela++) {
		/* Can not panic yet */
		if (ELF_R_TYPE(rela->r_info) != R_MORELLO_RELATIVE) {
			__builtin_trap();
			continue;
		}

		fragment = (Elf_Addr *)cheri_address_set(
			data_cap,
			(size_t)elf + elf_va_to_offset(elf, rela->r_offset));

		cap = build_cap_from_fragment(elf, fragment, 0, rela->r_addend,
					      data_cap, code_cap);
		*((uintptr_t *)fragment) = cap;
	}
}
