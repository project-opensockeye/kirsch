
#include <standalone/printf.h>
#include <stdint.h>
#include <cheri/cheri_interface.h>
#include <demo.h>

/**
 * A very simple CHERI demo. To be run when booting together with seL4.
 */
void demo(void)
{
	// Make some space on the console
	printf("\n");
	printf("\n");
	printf("\n");

	// Set up a pointer to where we'll store a capabilty.
	// This needs to match what seL4 writes to.
	volatile uint64_t **pptr =
		cheri_address_set(cheri_ddc_get(), 0xFE000000);
	// Set up a demo capability to store.
	uint64_t *ptr = cheri_address_set(cheri_ddc_get(), 0xFE0000F0);

	// Give our demo capability a regonizable value to point to.
	*ptr = 0x42;

	// Store the demo capability
	printf("[CHERI] Write cap to  0x%p\n", pptr);
	*pptr = ptr;

	// Read the demo capability back
	// At this point it should still be a capability.
	printf("[CHERI] Read cap from 0x%p\n", pptr);
	printf("[CHERI] Tag:   %d\n", cheri_tag_get(*pptr));
	printf("[CHERI] Cap:   %p\n", *pptr);
	if (cheri_tag_get(*pptr)) {
		printf("[CHERI] Value: 0x%x\n", **pptr);
	}

	volatile uint64_t *ptr_tmp = *pptr;

	// Wait for seL4 to ruin the capability.
	printf("[CHERI] Waiting for seL4...\n");
	while (*pptr == ptr_tmp) {
	}

	// Read the demo capability back
	// At this point it should have lost its tag.
	printf("[CHERI] Read cap from 0x%p\n", pptr);
	printf("[CHERI] Tag:   %d\n", cheri_tag_get(*pptr));
	printf("[CHERI] Cap:   %p\n", *pptr);
	if (cheri_tag_get(*pptr)) {
		printf("[CHERI] Value: 0x%x\n", **pptr);
	}

	while (true) {
	}
}
