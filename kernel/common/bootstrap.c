#include "bootstrap.h"
#include <standalone/printf.h>
#include "process.h"
#include <system_interface/system_interface.h>
#include <elf_helpers.h>

void bootstrap(const Elf_Ehdr *elf)
{
	printf("Generic bootstrapping... \n");

	elf_helpers_state_init(elf);
}
