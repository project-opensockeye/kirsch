
#include <handlers.h>
#include <cheri/cheri_interface.h>
#include <process.h>
#include <process.h>
#include <bf/logging.h>
#include <trap.h>
#include <system_interface/system_interface.h>

#ifdef __aarch64__
#include <timer.h>
#endif

#define SWITCH_TICKS 3

struct trap_vector_data global_trap_vector_data;

void __attribute__((noinline)) timer_interrupt_handler(void)
{
	printk(LOG_NOTE, "Handling timer interrupt...\n");
	//struct process_registers save_area;
	//process_registers_save(&save_area);
	struct trap_vector_data *trap_vector_data =
		trap_vector_data_load_from_context();

	//trap_vector_data->cur_proc->registers = save_area;

	// Store PCC in process struct
	printk(LOG_NOTE, "Reading trap return PC...\n");
	capability return_pc = trap_return_pc_read();
	printk(LOG_NOTE, "Writing trap return PC...\n");
	trap_vector_data->cur_proc->pcc = return_pc;
	//trap_vector_data->cur_proc->process_registers = save_area;

	// Store stack pointer in process struct
	printk(LOG_NOTE, "Reading CSP...\n");
	trap_vector_data->cur_proc->csp = cheri_csp_read();

	printk(LOG_NOTE, "Switching proc %d (@%p)\n",
	       trap_vector_data->cur_proc_index,
	       trap_vector_data->cur_proc->pcc);

	// Switch to next process
	trap_vector_data->cur_proc_index++;
	if (trap_vector_data->cur_proc_index == trap_vector_data->num_proc) {
		trap_vector_data->cur_proc_index = 0;
	}

	trap_vector_data->cur_proc = &(
		trap_vector_data->processes[trap_vector_data->cur_proc_index]);

	printk(LOG_NOTE, "        --> %d (@%p)\n",
	       trap_vector_data->cur_proc_index,
	       trap_vector_data->cur_proc->pcc);

	trap_return_pc_write(trap_vector_data->cur_proc->pcc);

	/*
	 * RISC-V uses memory-mapped registers for its timer,
	 * which can be protected by capabilites.
	 * Calling througha sealed set_timer cap makes sense on RISC-V.
	 * On ARM, setting the timer offset involves a register,
	 * which cannot be protected.
	 * The following is an ugly hack to distinguish between the two cases,
	 * and saves us from having to needlessly seal the set_timer 
	 * capabilities on Morello.
	 */
#ifdef __aarch64__
	set_timer(SWITCH_TICKS);
#else
	capability fun = trap_vector_data->set_timer;
	capability data = trap_vector_data->set_timer_data;
	// Update timer
	cheri_cinvoke_asm_1(fun, data, SWITCH_TICKS);
#endif

	if (trap_vector_data->cur_proc->first_start) {
		trap_vector_data->cur_proc->first_start = true;
		trap_vector_data->cur_proc->csp = cheri_csp_read();

		//proc_init();

		trap_return();
	} else {
		trap_vector_data->cur_proc->csp = cheri_csp_read();
	}
}
