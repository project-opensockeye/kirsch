#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

// RM: Yes, we'll need these. :)
void *malloc(size_t request)
{
	assert(false);
}
void free(void *allocation)
{
	assert(false);
}

/*
#include <gdtoa.h>
// RM: I have no idea where this function comes from.
// It is a #define in gdtoa.h, but __strtodg is never defined,
// and is not part of compiler-rt from what I can tell.
int strtodg(CONST char *a, char **b, FPI *c, Long *d, ULong *e)
{
	assert(false);
}

// We don't have threads/Thread-local-storage or runtime relocation yet.
void _pthread_mutex_lock(pthread_mutex_t *__mutex)
{
	assert(false);
}
void _pthread_mutex_unlock(pthread_mutex_t *__mutex)
{
	assert(false);
}

// Thread-local-storage function called by gdtoa.
void __tls_get_addr()
{
	assert(false);
}
*/
