#include <stddef.h>
#include <stdint.h>
#include <elf.h>

static const Elf_Ehdr *current_elf;

void elf_helpers_state_init(const Elf_Ehdr *elf)
{
	current_elf = elf;
}

const Elf_Ehdr *elf_current_get(void)
{
	return current_elf;
}

size_t elf_va_to_offset(const Elf_Ehdr *elf, size_t elf_va)
{
	if (!IS_ELF(*elf)) {
		//__asm__ volatile("li t0, 0xDEAD");
		__builtin_trap();
	}
	for (uint64_t i = 0; i < elf->e_phnum; i++) {
		const Elf64_Phdr *phdr =
			(const Elf64_Phdr *)(((uint8_t *)elf) + elf->e_phoff +
					     (i * elf->e_phentsize));

		if (phdr->p_vaddr <= elf_va &&
		    elf_va < phdr->p_vaddr + phdr->p_memsz) {
			return phdr->p_offset + (elf_va - phdr->p_vaddr);
		}
	}
	//__asm__ volatile("li t0, 0xDEAD");
	__builtin_trap();
}

size_t elf_offset_to_va(const Elf_Ehdr *elf, size_t elf_offset)
{
	if (!IS_ELF(*elf)) {
		//__asm__ volatile("li t0, 0xDEAD");
		__builtin_trap();
	}
	for (uint64_t i = 0; i < elf->e_phnum; i++) {
		const Elf64_Phdr *phdr =
			(const Elf64_Phdr *)(((uint8_t *)elf) + elf->e_phoff +
					     (i * elf->e_phentsize));

		if (phdr->p_offset <= elf_offset &&
		    elf_offset < phdr->p_offset + phdr->p_memsz) {
			return phdr->p_vaddr + (elf_offset - phdr->p_offset);
		}
	}

	//__asm__ volatile("li t0, 0xDEAD");
	__builtin_trap();
}
