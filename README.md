# Kirsch

A single address space operating system based on hardware capabilities.

This repository is based of a copy of Ben Laurie's `cheriette`.

See the hosted [Handbook](https://sockeye.ethz.ch/kirsch/docs/) and the [Handbook Repository](https://gitlab.inf.ethz.ch/project-opensockeye/kirsch-handbook) for Documentation.

## Building

1. Get the [Toolchain](https://gitlab.inf.ethz.ch/project-opensockeye/kirsch-toolchain).
2. Run `docker run --mount type=bind,source=$(pwd),target=/src --rm -it $(TOOLCHAIN-IMAGE)`
3. Run `ninja -C build` to run the Ninja script in `build/`.

## Wiki

The project wiki is located on the [ETH Confluence](https://unlimited.ethz.ch/display/kirschwiki/Kirsch+Project+Wiki+Home).
