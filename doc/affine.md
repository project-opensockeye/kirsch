# Compiler Friendly Locally Affine Capabilities

How do we allow one compartment to pass memory to another and reliably get it back?

Let's posit two permission bits, Affine and Store Affine. Affine caps can only be written to memory with Store Affine set.

These may be implementable with existing permission bits or may need new ones, depending on what else such permissions are used for.

The OS provides an inter-compartment stack, which is the only place with Write Affine set that is visible to compartments.

Cross-compartment calls use this stack. If Affine caps are passed, the compiler can spill them to the stack as usual. When the compartment returns, the stack is cleared.

Done.

I think.

The stack has to be cleared anyway, presumably, to avoid pointer leakage, so this doesn't add any additional cost.

In practice I think it has to be Not Affine and Store Affine. 