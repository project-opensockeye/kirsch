#!/bin/bash

# Runs the RISC-V 64-bit loader in QEMU.
#
# Trace log in /src/qemu_trace.log

/tools/qemu-system-riscv64cheri \
-m 2048 \
-kernel build/loader_riscv64.bin \
-machine virt \
-bios none \
-D /src/qemu_trace.log \
-d instr \
-serial mon:stdio \
-nographic