#!/bin/bash

# Format

action="$1"
echo $action

# Exit on error
set -e

if [[ "$action" == "check" ]]; then
    clang_format_args="--dry-run --Werror"
elif [[ "$action" == "--help" ]]; then
    echo "Usage: $0 [ACTION]"
    echo "Defaults to \"--format\""
    echo "  --format        Formats in-place all appropriate files"
    echo "  --check         Dry-Run format to check if all files are formatted"
    echo "  --help          Prints this help" 
    exit 1
else
    action="format"
    clang_format_args="-i"
fi

# Formats a single directory
# $1: Directory path
function format_dir() {

    echo "Running $action for $1..."

    user="$(stat -c '%u' "$1")"
    group="$(stat -c '%g' "$1")"

    find ./$1 -iname *.h -o -iname *.c | xargs clang-format $clang_format_args

    chown -R $user:$group $1

    echo "$action for $1 done!"

}

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
pushd $SCRIPT_DIR

format_dir ../include
format_dir ../kernel
format_dir ../loader
format_dir ../lib/kirsch
format_dir ../usr

popd
