#!/bin/bash

# Shortcut script for running the FVP with console output

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null & pwd )

source $SCRIPT_DIR/scripts/fvp-run-console.sh