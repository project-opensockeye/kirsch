#!/bin/bash

# Starts and FVP instance with an ARM TF FIP with UART console output on stdout.
# 
# - Enable/Disable tracing using 
#  > hlt 0x5
# - Trace log in /src/morello.log

fvp_path=/sim/fvp/Morello_Board/
fvp_firmware=/tools/fvp-firmware/
morello_workspace=/tools/morello-firmware-fvp

# Create FIFO for the following construction:
# FVP > FIFO > STDOUT
tmp="$(mktemp -d)"
trap "rm -rf $tmp" EXIT
mkfifo $tmp/port

# For TarmacTrace options, see
# https://developer.arm.com/documentation/100964/1125/Plug-ins-for-Fast-Models/TarmacTrace

$fvp_path/models/Linux64_GCC-6.4/FVP_Morello \
-C "disable_visualisation=true" \
-C "board.terminal_uart0_board.start_telnet=0" \
-C "board.terminal_uart1_board.start_telnet=0" \
-C "css.mcp.terminal_uart0.start_telnet=0" \
-C "css.mcp.terminal_uart1.start_telnet=0" \
-C "css.scp.terminal_uart_aon.start_telnet=0" \
-C "css.terminal_sec_uart_ap.start_telnet=0" \
-C "css.terminal_uart1_ap.start_telnet=0" \
-C "css.terminal_uart_ap.start_telnet=1" \
-C "css.terminal_uart_ap.terminal_command=echo %port > $tmp/port" \
--data Morello_Top.css.scp.armcortexm7ct=$morello_workspace/bsp/rom-binaries/scp_romfw.bin@0x0 \
--data Morello_Top.css.mcp.armcortexm7ct=$morello_workspace/bsp/rom-binaries/mcp_romfw.bin@0x0 \
-C Morello_Top.soc.scp_qspi_loader.fname=$morello_workspace/output/fvp/firmware/scp_fw.bin \
-C Morello_Top.soc.mcp_qspi_loader.fname=$morello_workspace/output/fvp/firmware/mcp_fw.bin \
-C css.scp.armcortexm7ct.INITVTOR=0x0 \
-C css.mcp.armcortexm7ct.INITVTOR=0x0 \
-C css.trustedBootROMloader.fname=$morello_workspace/bsp/rom-binaries/bl1.bin \
-C board.ap_qspi_loader.fname=$morello_workspace/bsp/arm-tf/build/morello/release/fip.bin \
-C css.pl011_uart_ap.out_file=uart0.log \
-C css.scp.pl011_uart_scp.out_file=scp.log \
-C css.mcp.pl011_uart0_mcp.out_file=mcp.log \
-C css.pl011_uart_ap.unbuffered_output=1 \
--plugin $fvp_path/plugins/Linux64_GCC-6.4/TarmacTrace.so \
--plugin $fvp_path/plugins/Linux64_GCC-6.4/ToggleMTIPlugin.so \
-C TRACE.ToggleMTIPlugin.use_hlt=1 \
-C TRACE.ToggleMTIPlugin.hlt_imm16=0x5 \
-C TRACE.ToggleMTIPlugin.disable_mti_from_start=1 \
-C TRACE.ToggleMTIPlugin.diagnostics=1 \
-C Morello_Top.css.cluster0.cpu0.trace_special_hlt_imm16=0x5 \
-C Morello_Top.css.cluster0.cpu1.trace_special_hlt_imm16=0x5 \
-C Morello_Top.css.cluster1.cpu0.trace_special_hlt_imm16=0x5 \
-C Morello_Top.css.cluster1.cpu1.trace_special_hlt_imm16=0x5 \
-C Morello_Top.css.cluster0.cpu0.enable_trace_special_hlt_imm16=1 \
-C Morello_Top.css.cluster0.cpu1.enable_trace_special_hlt_imm16=1 \
-C Morello_Top.css.cluster1.cpu0.enable_trace_special_hlt_imm16=1 \
-C Morello_Top.css.cluster1.cpu1.enable_trace_special_hlt_imm16=1 \
-C TRACE.TarmacTrace.trace-file=/src/morello.log \
-C TRACE.TarmacTrace.trace-inst-stem="Morello_Top.css.cluster" &

# Option to only start tracing after n instructions
# -C TRACE.TarmacTrace.start-instruction-count=0x16d0000 \

# Read FIFO into port
read port < $tmp/port
# Telnet to port
telnet localhost $port
kill -INT %%
wait
exit
