#!/bin/bash

# Starts an FVP instance without ARM TF, running the loader bare.
#
# - Enable/Disable tracing using 
#  > hlt 0x5
# - Trace log in /src/morello.log

fvp_path=/sim/fvp/Morello_Board/
fvp_firmware=/tools/fvp-firmware/

# For TarmacTrace options, see
# https://developer.arm.com/documentation/100964/1125/Plug-ins-for-Fast-Models/TarmacTrace

$fvp_path/models/Linux64_GCC-6.4/FVP_Morello \
--data Morello_Top.css.scp.armcortexm7ct=$fvp_firmware/scp_romfw.bin@0x0 \
--data Morello_Top.css.mcp.armcortexm7ct=$fvp_firmware/mcp_romfw.bin@0x0 \
-C Morello_Top.soc.scp_qspi_loader.fname=$fvp_firmware/scp_fw.bin \
-C Morello_Top.soc.mcp_qspi_loader.fname=$fvp_firmware/mcp_fw.bin \
-C css.scp.armcortexm7ct.INITVTOR=0x0 \
-C css.mcp.armcortexm7ct.INITVTOR=0x0 \
-C soc.scc.boot_gpr_2=0x14000000 \
-C soc.scc.boot_gpr_3=0 \
-C css.cluster0.cpu0.semihosting-stack_base=0xffff0000 \
-C css.cluster0.cpu0.semihosting-stack_limit=0xff000000 \
-C css.cluster0.cpu0.semihosting-heap_limit=0xff000000 \
-C css.cluster0.cpu0.semihosting-heap_base=0 \
--plugin $fvp_path/plugins/Linux64_GCC-6.4/TarmacTrace.so \
--plugin $fvp_path/plugins/Linux64_GCC-6.4/ToggleMTIPlugin.so \
-C TRACE.ToggleMTIPlugin.use_hlt=1 \
-C TRACE.ToggleMTIPlugin.hlt_imm16=0x5 \
-C TRACE.ToggleMTIPlugin.disable_mti_from_start=1 \
-C TRACE.ToggleMTIPlugin.diagnostics=1 \
-C Morello_Top.css.cluster0.cpu0.trace_special_hlt_imm16=0x5 \
-C Morello_Top.css.cluster0.cpu1.trace_special_hlt_imm16=0x5 \
-C Morello_Top.css.cluster1.cpu0.trace_special_hlt_imm16=0x5 \
-C Morello_Top.css.cluster1.cpu1.trace_special_hlt_imm16=0x5 \
-C Morello_Top.css.cluster0.cpu0.enable_trace_special_hlt_imm16=1 \
-C Morello_Top.css.cluster0.cpu1.enable_trace_special_hlt_imm16=1 \
-C Morello_Top.css.cluster1.cpu0.enable_trace_special_hlt_imm16=1 \
-C Morello_Top.css.cluster1.cpu1.enable_trace_special_hlt_imm16=1 \
-C TRACE.TarmacTrace.trace-file=/src/morello.log \
-C TRACE.TarmacTrace.trace-inst-stem="Morello_Top.css.cluster" \
-b 0x14000000 \ 
--data=/src/build/cheriette_morello_loader.bin@0x14000000
