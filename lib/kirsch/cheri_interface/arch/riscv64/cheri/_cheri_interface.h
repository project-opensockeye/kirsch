
#ifndef _ARCH_CHERI_INTERFACE_H
#define _ARCH_CHERI_INTERFACE_H

typedef void *__capability _capability;

#define _cheri_csp_offset(offset) \
	__asm__ volatile("cincoffset csp, csp, " #offset "");

#define _cheri_reg_stack_save(reg) \
	_cheri_csp_offset(-16);    \
	__asm__ volatile("csc " #reg ", 0x0(csp)");

#define _cheri_reg_stack_load(reg)                  \
	__asm__ volatile("clc " #reg ", 0x0(csp)"); \
	_cheri_csp_offset(16);

_capability _cheri_csp_read(void);

_capability _cheri_pcc_read(void);

#define _cheri_cinvoke_asm(ccap, dcap)                                                                                                           \
	__asm__ volatile(                                                                                                                        \
		"cinvoke %0, %1"                                                                                                                 \
		:                                                                                                                                \
		: /* RISC-V CHERI LLVM provides  the "C" constraint to refer to capabilites */                                                   \
		/* Here, we constrain the inputs of the ASM block so that */ /* ccap is the first and dcap is the second operand in `cinvoke`.*/ \
		"C"(ccap), "C"(dcap));

#define _cheri_cinvoke_asm_1(ccap, dcap, param)                                                                                                  \
	_cheri_reg_stack_save(cra);                                                                                                              \
	__asm__ volatile(                                                                                                                        \
		"cspecialr cra, pcc\n"                                                                                                           \
		"cincoffset cra, cra, 14\n"                                                                                                      \
		"mv a0, %2 \n"                                                                                                                   \
		"cinvoke %0, %1 \n"                                                                                                              \
		"add a0, a0, 0 \n"                                                                                                               \
		:                                                                                                                                \
		: /* RISC-V CHERI LLVM provides  the "C" constraint to refer to capabilites */                                                   \
		/* Here, we constrain the inputs of the ASM block so that */ /* ccap is the first and dcap is the second operand in `cinvoke`.*/ \
		"C"(ccap), "C"(dcap), "r"(param)                                                                                                 \
		: "a0", "ca0", "c31", "t6");                                                                                                     \
	_cheri_reg_stack_load(cra);

#define _cheri_special_read(from_reg, to_val) \
	__asm__ volatile("cspecialr %0, " #from_reg "" : "=C"(to_val))

#define _cheri_special_write(to_reg, from_val) \
	__asm__ volatile("cspecialw " #to_reg ", %0" : : "C"(from_val))

#define _cheri_cap_mov_cvar_into_reg_reg(into_reg, from_val) \
	__asm__ volatile("cmove" #into_reg ", %0" : : "C"(from_val) : #into_reg)

#define _cheri_cap_mov_reg_into_cvar(from_reg, into_val) \
	__asm__ volatile("cmove %0, " #from_reg : "=C"(into_val))

#endif
