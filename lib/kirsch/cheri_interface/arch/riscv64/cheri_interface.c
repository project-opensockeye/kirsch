
#include <cheri/cheri_interface.h>

/**
 * Safe cinvoke wrapper that saves and clears all registers before calling
 *
 * This function does *nothing else* and is intended as a clean interface
 * for other system functions to use.
 *
 * @param ccap Sealed Code capability to be used in the `cinvoke`.
 * @param dcap Sealed Data capability to be used in the `cinvoke`.
 */
void cinvoke_safe(void *__capability ccap, void *__capability dcap)
{
}

capability _cheri_csp_read(void)
{
	capability sp;
	cheri_cap_mov_reg_into_cvar(csp, sp);
	return sp;
}

capability _cheri_pcc_read(void)
{
	capability pc;
	cheri_special_read(pcc, pc);
	return pc;
}
