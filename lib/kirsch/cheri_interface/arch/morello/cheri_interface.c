
#include <cheri/cheri_interface.h>

capability _cheri_csp_read()
{
	capability csp;
	__asm__ volatile("mov %[csp], csp" : [csp] "=C"(csp));
	return csp;
}

capability _cheri_pcc_read()
{
	capability pcc;
	__asm__ volatile("adr %[pcc], 0x0" : [pcc] "=C"(pcc));
	return pcc;
}
