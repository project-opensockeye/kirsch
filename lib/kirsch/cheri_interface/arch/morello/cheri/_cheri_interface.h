#ifndef _ARCH_CHERI_INTERFACE_H
#define _ARCH_CHERI_INTERFACE_H

typedef void *__capability _capability;

#define _cheri_csp_offset(offset) __asm__ volatile("hlt 0x1");

#define _cheri_reg_stack_save(reg) __asm__ volatile("hlt 0x1");

#define _cheri_reg_stack_load(reg) __asm__ volatile("hlt 0x1");

#define _cheri_cinvoke_asm(ccap, dcap) __asm__ volatile("hlt 0x1")

#define _cheri_cinvoke_asm_1(ccap, dcap, param) \
	__asm__("hlt 0x1");                     \
	(void)ccap;                             \
	(void)dcap

#define _cheri_special_read(from_reg, to_val) __asm__ volatile("hlt 0x1");

#define _cheri_special_write(to_reg, from_val) __asm__ volatile("hlt 0x1")

#define _cheri_cap_mov_cvar_into_reg_reg(into_reg, from_val) \
	__asm__ volatile("hlt 0x1")

#define _cheri_cap_mov_reg_into_cvar(from_reg, into_val) \
	__asm__ volatile("hlt 0x1")

_capability _cheri_csp_read(void);

_capability _cheri_pcc_read(void);

#endif
