#include <cheri/cheri_helpers.h>
#include <cheri/_cheri_interface.h>
#include <cheri/cheriintrin.h>

#ifndef _CHERI_INTERFACE_H
#define _CHERI_INTERFACE_H

// Define a custom capability type that may have a target specific type.
typedef _capability capability;

/**
 * Add an offset to the capability stack pointer. 
 * @param offset the offset to apply to csp.
 */
#define cheri_csp_offset(offset) _cheri_csp_offset(offset)

/** 
 * Save a register to the stack
 * @param The register to save.
 */
#define cheri_reg_stack_save(reg) _cheri_reg_stack_save(reg)

/** 
 * Load a register to the stack
 * @param The register to load.
 */
#define cheri_reg_stack_load(reg) _cheri_reg_stack_load(reg)

/**
 * Reads the stack pointer capability and returns it as a capability.
 * 
 * @returns The stack pointer capability.
 */
#define cheri_csp_read() _cheri_csp_read()

/**
 * Reads the program counter capability and returns it as a capability.
 * 
 * @returns The program counter capability.
 */
#define cheri_pcc_read() _cheri_pcc_read()

/**
 * Wrapper for `cinvoke` with the sealed code and data cap as
 * provided in the arguments.
 *
 * This function does *nothing else* and is intended as a clean interface
 * for other system functions to use.
 *
 * @param ccap Sealed Code capability to be used in the `cinvoke`.
 * @param dcap Sealed Data capability to be used in the `cinvoke`.
 */
#define cheri_cinvoke_asm(ccap, dcap) _cheri_cinvoke_asm(ccap, dcap)

/**
 * Treat cinvoke as a function call and pass an argument in a0
 * @param ccap Sealed Code capability to be used in the `cinvoke`.
 * @param dcap Sealed Data capability to be used in the `cinvoke`.
 * @param param Paramter to pass in a0
 */
#define cheri_cinvoke_asm_1(ccap, dcap, param) \
	_cheri_cinvoke_asm_1(ccap, dcap, param)

/**
 * Reads a special register into a C variable.
 * @param from_reg Special register to read
 * @param to_val C variable to write to 
 */
#define cheri_special_read(from_reg, to_val) \
	_cheri_special_read(from_reg, to_val)

/**
 * Writes a C variable value to a special register
 * @param to_reg Special register to write 
 * @param from_val C variable to read 
 */
#define cheri_special_write(to_reg, from_val) \
	_cheri_special_write(to_reg, from_val)

/**
 * Copies the value of a C variable into a register
 * @param into_reg Register to copy to
 * @param from_val C variable to copy from
 */
#define cheri_cap_mov_cvar_into_reg(into, from) \
	_cheri_cap_mov_cvar_into_reg(into, from)

/**
 * Copies the value of a register into a C variable
 * @param from_reg Register to copy from
 * @param into_val C variable to copy to 
 */
#define cheri_cap_mov_reg_into_cvar(from, into) \
	_cheri_cap_mov_reg_into_cvar(from, into)

#endif
