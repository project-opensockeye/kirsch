
#include <system_interface/system_interface.h>

/**
 * "Terminates" execution.
 * 
 * "HLT" properly halts ARM processors, so this terminates the
 * tracing.
 */
void __attribute__((noreturn)) system_terminate()
{
	__asm__ volatile("HLT 0x0");
	__builtin_unreachable();
}
