#include <system_interface/system_interface.h>
#include <sail_emulator/interface_functions.h>
#include <csr.h>

/**
 * "Terminates" execution.
 * 
 * The QEMU RISC-V emulation does not have a good way 
 * to terminate, apart from using 
 * - the SBI (supervisor binary interface), which we are not using as it 
 * requires having a "supervisor"/"RISC-V BIOS" running.
 * - HTIF (Host-Target Interface), which can *either* attach to loader or kernel
 * but not both, so the loader would need to initialize it and then pass
 * HTIF addresses to the kernel. TODO.
 * 
 * Instead, we busy loop wait for an interrupt.
 * 
 * This may continue to litter instruction traces, as "wfi" may
 * return immediately.
 */
void __attribute__((noreturn)) system_terminate(void)
{
	__asm__ volatile("li x0, 0xDEAD");
	for (;;)
		__asm__ volatile("wfi");
	__builtin_trap();
}
