
/**
 * Terminates execution in a target-specific way.
 */
void __attribute__((noreturn)) system_terminate(void);
