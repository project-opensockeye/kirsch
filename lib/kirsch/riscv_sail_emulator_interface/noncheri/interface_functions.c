
#include <stdint.h>

extern uint64_t tohost;
extern uint64_t fromhost;
extern void *do_tohost(uint64_t *tohost, uint64_t *fromhost, uint64_t param);
extern struct do_tohost_data do_tohost_data;

void tohost_init(void)
{
}

void tohost_write(uint64_t param)
{
	do_tohost(&tohost, &fromhost, param);
}
