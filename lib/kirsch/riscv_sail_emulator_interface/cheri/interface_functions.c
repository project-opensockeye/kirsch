#include <cheri/cheri_defines.h>
#include <cheri/cheri_interface.h>
#include <stdint.h>
#include <sail_emulator/interface_functions.h>

struct do_tohost_data {
	void *__capability tohost_cap;
	void *__capability fromhost_cap;
} __attribute__((packed)) __attribute__((aligned(16)));

struct tohost_putc_invoke_caps tohost_putc_invoke_caps;
extern uint64_t tohost;
extern uint64_t fromhost;
extern void *do_tohost;
extern struct do_tohost_data do_tohost_data;

void tohost_init(void)
{
	/*
  sealed capability with an object type
      A sealed capability whose capability object type is not one of the
      reserved capability object types. These sealed capability have a
      capability object type derived from their sealing capabilities’s
      address.
  */
	void *__capability sealing_cap = cheri_ddc_get();
	sealing_cap = cheri_address_set(sealing_cap, 1);

	void *__capability pcc = cheri_pcc_get();
	pcc = cheri_perms_and(pcc, CAP_PERMIT_EXECUTE | CAP_PERMIT_CINVOKE);
	pcc = cheri_address_set(pcc, (unsigned long)&do_tohost);
	pcc = cheri_seal(pcc, sealing_cap);

	struct do_tohost_data *ddc = &do_tohost_data;
	void *__capability tohost_cap = (void *__capability)&tohost;
	tohost_cap = cheri_bounds_set(tohost_cap, 8);
	tohost_cap =
		cheri_perms_and(tohost_cap, CAP_PERMIT_LOAD | CAP_PERMIT_STORE);
	ddc->tohost_cap = tohost_cap;
	void *__capability fromhost_cap = (void *__capability)&fromhost;
	fromhost_cap = cheri_bounds_set(fromhost_cap, 8);
	fromhost_cap = cheri_perms_and(fromhost_cap,
				       CAP_PERMIT_LOAD | CAP_PERMIT_STORE);
	ddc->fromhost_cap = fromhost_cap;

	ddc = cheri_perms_and(ddc, CAP_PERMIT_LOAD |
					   CAP_PERMIT_LOAD_CAPABILITY |
					   CAP_PERMIT_CINVOKE);
	ddc = cheri_seal(ddc, sealing_cap);

	tohost_putc_invoke_caps.code_cap = pcc;
	tohost_putc_invoke_caps.data_cap = (void *__capability)ddc;
}

void tohost_write(uint64_t param)
{
	cheri_cinvoke_asm_1(tohost_putc_invoke_caps.code_cap,
			    tohost_putc_invoke_caps.data_cap, param);
}
