
#include <sail_emulator/interface_functions.h>

void tohost_putc(int c)
{
	tohost_write(0x0101000000000000 | (unsigned char)c);
}

void emulator_terminate(void)
{
	tohost_write(0x1);
	__builtin_unreachable();
}
