#include <stdint.h>

#ifndef _EMULATOR_INTERFACE_FUNCTIONS_H
#define _EMULATOR_INTERFACE_FUNCTIONS_H

struct tohost_putc_invoke_caps {
	void *__capability code_cap;
	void *__capability data_cap;
} __attribute__((aligned(16))) __attribute__((packed));

void tohost_init(void);

void tohost_write(uint64_t param);

extern void tohost_putc(int c);

void __attribute__((noreturn)) emulator_terminate(void);

#endif
