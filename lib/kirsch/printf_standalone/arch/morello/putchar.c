#include <stdbool.h>
#include <standalone/printf.h>
#if __has_feature(capabilities)
#include <cheri/cheriintrin.h>
#endif

#define PLAT_ARM_BOOT_UART_BASE 0x2A400000
#define PLAT_ARM_BOOT_UART_BASE_GLAS 0x2A400000

static bool putchar_glas_enabled;

/**
 * Switches putchar from using 1-1 mapping to using GLAS addresses.
 * 
 * @param is_enabled True if the UART is found at a GLAS address. 
 */
void putchar_glas_state_set(bool is_enabled)
{
	putchar_glas_enabled = is_enabled;
	__asm__ volatile("isb");
	__asm__ volatile("dsb ST");
}

/**
 * Configures the UART for single-character putchar.
 */
void putchar_init(void)
{
}

/**
 * Writes a single character to the UART.
 * 
 * Must work both in CHERI and non-CHERI contexts, because
 * the loader is not compiled with CHERI and the kernel is.
 * 
 * @param character The char to write.
 */
void _putchar(char character)
{
#if !__has_feature(capabilities)
	volatile char *ptr = NULL;
#else
	volatile char *__capability ptr = cheri_ddc_get();
#endif

	if (!putchar_glas_enabled) {
#if !__has_feature(capabilities)
		ptr = (volatile char *)PLAT_ARM_BOOT_UART_BASE;
#else
		ptr = cheri_address_set(ptr, PLAT_ARM_BOOT_UART_BASE);
#endif
	} else {
#if !__has_feature(capabilities)
		ptr = (volatile char *)PLAT_ARM_BOOT_UART_BASE_GLAS;
#else
		ptr = cheri_address_set(ptr, PLAT_ARM_BOOT_UART_BASE_GLAS);
#endif
	}
	*ptr = character;
}
