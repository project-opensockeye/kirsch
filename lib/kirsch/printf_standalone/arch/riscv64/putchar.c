#include <stdbool.h>
#include <stddef.h>
#include <mackerel_support/mackerel.h>
#if __has_feature(capabilities)
#include <cheri/cheriintrin.h>
#endif

static mackerel_addr_t ns16550_base = (mackerel_addr_t)0x10000000;
static size_t thr_offset = 0x0;
static size_t ier_offset = 0x1;
static size_t fcr_offset = 0x2;
static size_t lcr_offset = 0x3;
static size_t lsr_offset = 0x5;

static bool putchar_glas_enabled;

/**
 * Switches putchar from using 1-1 mapping to using GLAS addresses.
 * 
 * @param is_enabled True if the UART is found at a GLAS address. 
 */
void putchar_glas_state_set(bool is_enabled)
{
	putchar_glas_enabled = is_enabled;
}

/**
 * Configures the UART for single-character putchar.
 */
void putchar_init(void)
{
	// Disable interrupts
	mackerel_write_addr_8(ns16550_base, ier_offset, 0x0);

	// 8-bit word length
	mackerel_write_addr_8(ns16550_base, lcr_offset, 0x3);

	// Clear receive FIFO
	mackerel_write_addr_8(ns16550_base, fcr_offset, 0x2);
	// Clear transmit FIFO
	mackerel_write_addr_8(ns16550_base, fcr_offset, 0x4);

	// Enable FIFOs
	mackerel_write_addr_8(ns16550_base, fcr_offset, 0x1);

	// Clear receive FIFO
	mackerel_write_addr_8(ns16550_base, fcr_offset, 0x2);
	// Clear transmit FIFO
	mackerel_write_addr_8(ns16550_base, fcr_offset, 0x4);
}

/**
 * Writes a single character to the UART.
 * 
 * Must work both in CHERI and non-CHERI contexts, because
 * the loader is not compiled with CHERI and the kernel is.
 * 
 * @param character The char to write.
 */
void _putchar(char character)
{
#if !__has_feature(capabilities)
	volatile uint8_t *lsr = NULL;
	volatile char *thr = NULL;
#else
	volatile uint8_t *__capability lsr = cheri_ddc_get();
	volatile char *__capability thr = cheri_ddc_get();
#endif

#if !__has_feature(capabilities)
	lsr = (uint8_t *)ns16550_base + lsr_offset;
	thr = (char *)ns16550_base + thr_offset;
#else
	lsr = cheri_address_set(lsr, ((size_t)ns16550_base) + lsr_offset);
	thr = cheri_address_set(thr, ((size_t)ns16550_base) + thr_offset);
#endif

	// Wait until FIFO can hold more characters
	while (!(((*lsr) >> 4) == 0x6))
		;

	*thr = character;
}
