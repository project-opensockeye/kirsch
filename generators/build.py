from pathlib import Path
import os
import sys
import argparse

import build.artifacts.morello_atf_package
from build.lib.cheriette_ninja_writer import CherietteNinjaWriter
from build.helpers import *

import build.artifacts.kernel
import build.artifacts.loader
import build.artifacts.printf_standalone
import build.artifacts.system_interface
import build.artifacts.cheri_interface
import build.artifacts.riscv_sail_emulator_interface
import build.artifacts.msun
import build.artifacts.gdtoa
import build.artifacts.libc
import build.artifacts.compiler_rt
import build.contexts

parser = argparse.ArgumentParser(
    prog='build.py',
    description='Generates a build.ninja file.',
    epilog='See the Kirsch wiki (https://sockeye.ethz.ch/kirsch/docs/) for more information!',
)

parser.add_argument('--demo-enable', required=False, default=False)

args = parser.parse_args()

ninja = CherietteNinjaWriter(statement_prefix="", output=sys.stdout)

ninja.comment("Build file for Kirsch/Cheriette")
ninja.comment("--------------------------------")
ninja.newline()

arch = "morello"
platform = "fvp"
src = os.getcwd()
ninja.variable("src", src)

morello_llvm_dir = Path("/tools/llvm-morello-distribution/bin")
compiler_arch_dict = {
    "riscv64": Path("clang"),
    "morello": morello_llvm_dir / "clang",
}
linker_arch_dict = {
    "riscv64": Path("ld.lld"),
    "morello": morello_llvm_dir / "ld.lld",
}
archiver_arch_dict = {
    "riscv64": Path("ar"),
    "morello": morello_llvm_dir / "llvm-ar",
}

objcopy_arch_dict = {
    "riscv64": Path("llvm-objcopy"),
    "morello": morello_llvm_dir / "llvm-objcopy",
}


def add(arch):

    cc = compiler_arch_dict[arch]
    ld = linker_arch_dict[arch]
    ar = archiver_arch_dict[arch]
    objcopy = objcopy_arch_dict[arch]

    morello_atf_workspace_path = Path("/tools/morello-firmware-fvp")

    target = build.artifacts.kernel.Target(
        arch=arch,
        platform=platform,
        cc=cc,
        ld=ld,
        ar=ar,
        objcopy=objcopy,
        morello_workspace_path=morello_atf_workspace_path,
    )

    contexts = build.contexts.get()

    printf_standalone = build.artifacts.printf_standalone.add(
        ninja=ninja,
        target=target,
        contexts=contexts,
        cheri_enabled=True,
    )
    printf_standalone_noncheri = build.artifacts.printf_standalone.add(
        ninja=ninja,
        target=target,
        contexts=contexts,
        cheri_enabled=False,
    )

    system_interface = build.artifacts.system_interface.add(
        ninja=ninja,
        target=target,
        contexts=contexts,
        cheri_enabled=True,
    )
    system_interface_noncheri = build.artifacts.system_interface.add(
        ninja=ninja,
        target=target,
        contexts=contexts,
        cheri_enabled=False,
    )

    cheri_interface = build.artifacts.cheri_interface.add(
        ninja=ninja,
        target=target,
        contexts=contexts,
    )

    riscv_sail_emulator_interface = build.artifacts.riscv_sail_emulator_interface.add(
        ninja=ninja,
        target=target,
        contexts=contexts,
        cheri_enabled=True,
    )
    riscv_sail_emulator_interface_noncheri = (
        build.artifacts.riscv_sail_emulator_interface.add(
            ninja=ninja,
            target=target,
            contexts=contexts,
            cheri_enabled=False,
        )
    )
    # msun = build.artifacts.msun.add(
    #    ninja=ninja,
    #    target=target,
    #    contexts=contexts,
    # )

    compiler_rt = build.artifacts.compiler_rt.add(
        ninja=ninja, target=target, contexts=contexts, cheri_enabled=True
    )

    compiler_rt_noncheri = build.artifacts.compiler_rt.add(
        ninja=ninja, target=target, contexts=contexts, cheri_enabled=False
    )

    # libc = build.artifacts.libc.add(
    #    ninja=ninja,
    #    target=target,
    #    contexts=contexts,
    # )

    # gdtoa = build.artifacts.gdtoa.add(
    #    ninja=ninja,
    #    target=target,
    #    contexts=contexts,
    # )

    kernel = build.artifacts.kernel.add(
        ninja,
        target=target,
        contexts=contexts,
        printf_standalone=printf_standalone,
        riscv_sail_emulator_interface=riscv_sail_emulator_interface,
        compiler_rt=compiler_rt,
        system_interface=system_interface,
        cheri_interface=cheri_interface,
        demo_enable=args.demo_enable,
    )
    loader_elf = build.artifacts.loader.add(
        ninja,
        target=target,
        contexts=contexts,
        printf_standalone_noncheri=printf_standalone_noncheri,
        riscv_sail_emulator_interface_noncheri=riscv_sail_emulator_interface_noncheri,
        compiler_rt_noncheri=compiler_rt_noncheri,
        system_interface_noncheri=system_interface_noncheri,
        demo_enable=args.demo_enable,
    )

    loader_bin = build.artifacts.loader.add_bin(
        ninja,
        target=target,
        contexts=contexts,
        loader_elf=loader_elf.get_artifact_name(),
        kernel=kernel.get_artifact_name(),
    )

    final_artifact = loader_bin

    if arch == "morello":
        morello_atf_package = build.artifacts.morello_atf_package.add(
            ninja,
            target=target,
            loader_bin=loader_bin.get_artifact_name(),
        )

        final_artifact = morello_atf_package

    ninja.build(
        outputs=arch + "_all",
        rule="depend",
        inputs=final_artifact.get_artifact_name(),
    )

    
ninja.rule(
    name="depend",
    command="echo ''",
    description="Dependency: $in -> $out",
)

add("morello")
add("riscv64")

ninja.default(["morello_all", "riscv64_all"])
