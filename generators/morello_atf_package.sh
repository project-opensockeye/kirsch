#!/bin/bash

# Exit with error on subcommand error
set -e

morello_target=$1
morello_workspace=$2
user_binary_path=`realpath $3`
output_fip_path=`realpath $4`
output_bl1_path=`realpath $5`

enable_morello_cap=1

echo "Packaging binary $user_binary_path into ATF..."

pushd $morello_workspace

make -C "bsp/arm-tf" \
PLAT=morello TARGET_PLATFORM=$morello_target ENABLE_MORELLO_CAP=$enable_morello_cap \
CC="$morello_workspace/tools/clang/bin/clang" clean

MBEDTLS_DIR="$morello_workspace/bsp/deps/mbedtls" \
CROSS_COMPILE="$morello_workspace/tools/clang/bin/llvm-" \
make -C "bsp/arm-tf" \
CC="$morello_workspace/tools/clang/bin/clang" \
LD="$morello_workspace/tools/clang/bin/ld.lld" \
PLAT=morello ARCH=aarch64 TARGET_PLATFORM=$morello_target ENABLE_MORELLO_CAP=$enable_morello_cap \
E=0 TRUSTED_BOARD_BOOT=1 GENERATE_COT=1 ARM_ROTPK_LOCATION="devel_rsa" \
ROT_KEY="plat/arm/board/common/rotpk/arm_rotprivk_rsa.pem" \
BL33="$user_binary_path" \
OPENSSL_DIR="$morello_workspace/output/$morello_target/intermediates/host_openssl/install" \
all fip

echo "Copying fip.bin and bl1.bin..."
cp bsp/arm-tf/build/morello/release/fip.bin $output_fip_path
cp bsp/arm-tf/build/morello/release/bl1.bin $output_bl1_path

popd
