# Generate the boring bits

import sys
from math import log2

REG_ALIASES = {
    "riscv64": {
        "zero": "x0",
        "ra": "x1",
        "sp": "x2",
        "gp": "x3",
        "tp": "x4",
        "t0": "x5",
        "t1": "x6",
        "t2": "x7",
        "s0": "x8",
        "fp": "x8",
        "s1": "x9",
        "a0": "x10",
        "a1": "x11",
        "a2": "x12",
        "a3": "x13",
        "a4": "x14",
        "a5": "x15",
        "a6": "x16",
        "a7": "x17",
        "s2": "x18",
        "s3": "x19",
        "s4": "x20",
        "s5": "x21",
        "s6": "x22",
        "s7": "x23",
        "s8": "x24",
        "s9": "x25",
        "s10": "x26",
        "s11": "x27",
        "t3": "x28",
        "t4": "x29",
        "t5": "x30",
        "t6": "x31",
    },
    "morello": {},
}

CALLER_SAVED_REGS = {
    "riscv64": [
        "ra",
        "t0",
        "t1",
        "t2",
        "a2",
        "a3",
        "a4",
        "a5",
        "a6",
        "a7",
        "t3",
        "t4",
        "t5",
    ],
    "morello": [
        "ra",
        "t0",
        "t1",
        "t2",
        "a2",
        "a3",
        "a4",
        "a5",
        "a6",
        "a7",
        "t3",
        "t4",
        "t5",
    ],
}

PERMS = [
    "GLOBAL",
    "PERMIT_EXECUTE",
    "PERMIT_LOAD",
    "PERMIT_STORE",
    "PERMIT_LOAD_CAPABILITY",
    "PERMIT_STORE_CAPABILITY",
    "PERMIT_STORE_LOCAL_CAPABILITY",
    "PERMIT_SEAL",
    "PERMIT_CINVOKE",
    "PERMIT_UNSEAL",
    "PERMIT_ACCESS_SYSTEM_REGISTERS",
    "PERMIT_SET_CID",
]

## TODO: FIX after Morello introduction
# CREG_ALIASES = {}
# REG_REV_ALIASES = {}
# for a in REG_ALIASES.keys():
#    ca = f'c{a}'
#    r = REG_ALIASES[a]
#    CREG_ALIASES[ca] = f'c{r[1:]}'
#    REG_REV_ALIASES[r] = a


def reg_alias(reg, arch):
    if reg in REG_ALIASES:
        return REG_ALIASES[reg]
    if reg in CREG_ALIASES:
        return CREG_ALIASES[reg]
    return reg


def bs(crit):
    return "; \\" if crit else ""


class Model_c:
    def __init__(self, arch):
        if arch == "riscv64":
            self._nregs = 32
            self._bytes = 16
        elif arch == "morello":
            self._nregs = 30
            self._bytes = 16
        else:
            die("Unknown arch " + arch)
        self.arch = arch

    def ClearRegisters(self):
        print("#define CLEAR_REGISTERS() ({ \\")
        print("__asm__ volatile ( \\")
        for n in range(1, self._nregs):
            if arch == "riscv64":
                print(f'  "li x{n}, 0" \\')
            elif arch == "morello":
                print(f'  "mov x{n}, xzr", \\')
        print("); \\")
        print("})")

    def SaveRegisters(self, reg):
        ra = reg_alias(reg)
        save_offset = -1
        print("#define SAVE_REGISTERS ({ \\")
        print("__asm__ volatile ( \\")
        for n in range(1, self._nregs):
            rn = f"c{n}"
            o = n * self._bytes
            if rn == reg or rn == ra:
                print(f'  "# Skip {rn}{f" == {reg}" if ra != reg else ""}\\n" \\')
                save_offset = o
                continue
            if arch == "riscv64":
                print(f"  csc {rn}, {o}({reg})")
            elif arch == "morello":
                print(f"  str {rn}, [{reg}, #{o}]")
        print("); \\")
        print("})")
        assert save_offset != -1

        n = 0
        print("#define SAVE_CALLER_REGISTERS ({ \\")
        print("__asm__ volatile ( \\")
        for reg in CALLER_SAVED_REGS[self.arch]:
            rn = f"c{reg}"
            o = n * self._bytes
            print(f'  "csc {rn}, {o}(csp) \\n" \\')
            n = n + 1
        print("); \\")
        print("})")
        n = 0
        print("#define LOAD_CALLER_REGISTERS ({ \\")
        print("__asm__ volatile ( \\")
        for reg in CALLER_SAVED_REGS[self.arch]:
            rn = f"c{reg}"
            o = n * self._bytes
            if arch == "riscv64":
                print(f"  clc {rn}, {o}({reg})")
            elif arch == "morello":
                print(f"  ldr {rn}, [{reg}, {o}]")
            n = n + 1
        print("); \\")
        print("})")

        n = 0
        print("/*void __attribute__((naked)) save_caller_registers() {")
        print("__asm__ volatile ( ")
        for reg in CALLER_SAVED_REGS[arch]:
            rn = f"c{reg}"
            o = n * self._bytes
            if arch == "riscv64":
                print(f"  csc {rn}, {o}({reg})")
            elif arch == "morello":
                print(f"  str {rn}, [{reg}, #{o}]")
            n = n + 1
        print("); \\")
        print("}")
        n = 0
        print("void __attribute__((naked)) load_caller_registers() {")
        print('__asm__ volatile (" ')
        for reg in CALLER_SAVED_REGS[arch]:
            rn = f"c{reg}"
            o = n * self._bytes
            if arch == "riscv64":
                print(f"  clc {rn}, {o}({reg})")
            elif arch == "morello":
                print(f'  "ldr {rn}, [{reg}, {o}]"')
            n = n + 1
        print("); ")
        print("}*/")

        print("#define LOAD_REGISTERS ({ \\")
        print("__asm__ volatile ( \\")
        for n in range(1, self._nregs):
            rn = f"c{n}"
            o = n * self._bytes
            if rn == reg or rn == ra:
                print(f'  "# Skip {rn}{f" == {reg}" if ra != reg else ""}\\n" \\')
                continue
            if arch == "riscv64":
                print(f"  clc {rn}, {o}({reg})")
            elif arch == "morello":
                print(f"  ldr {rn}, [{reg}, {o}]")
        print("); \\")
        print("})")

        ss = self._nregs * self._bytes

        print(
            f"#define SAVE_SIZE {ss}  // {self._nregs} registers, size of register save area including unused slot 0 (used for PC elsewhere)"
        )
        print(f"#define SAVE_OFFSET {save_offset}\n")

        # Unported
        # print('#define SAVE_BLOCK(code_start, code_end, data_start, datat_end) ({ \\')
        # print('__asm__ volatile ( \\')
        # print('  .dword \\code_start, \\code_end - \\code_start  // 0')
        # print('  .dword \\data_start, \\data_end - \\data_start  // 16')
        # for n in range(2, self._nregs):
        #    print(f'  .dword 0, 0  // {n * self._bytes}')
        # print('.endm\n')

        for n in range(1, self._nregs):
            print(f'#define SAVE_C{REG_REV_ALIASES[f"x{n}"].upper()} {n * self._bytes}')
        print()

    def CapabilityBits(self):
        for n in range(len(PERMS)):
            print(f"#define CAP_B_{PERMS[n]} {n}")
            print(f"#define CAP_{PERMS[n]} 1 << CAP_B_{PERMS[n]}")


class Model_asm:
    def __init__(self, arch):
        if arch == "riscv64":
            self._nregs = 32
            self._bytes = 16
        elif arch == "morello":
            self._nregs = 30
            self._bytes = 16
        else:
            die("Unknown arch " + arch)
        self.arch = arch
        pass

    def ClearRegisters(self):
        print(".macro CLEAR_REGISTERS")
        for n in range(1, self._nregs):
            if arch == "riscv64":
                print(f"  li x{n}, 0")
            elif arch == "morello":
                print(f"  mov x{n}, 0")
        print(".endm\n")

    def SaveRegisters(self, reg):
        ra = reg_alias(reg)
        save_offset = -1
        print(f".macro SAVE_REGISTERS")
        for n in range(1, self._nregs):
            rn = f"c{n}"

            o = n * self._bytes
            if rn == reg or rn == ra:
                print(f'  // Skip {rn}{f" == {reg}" if ra != reg else ""}')
                save_offset = o
                continue
            if arch == "riscv64":
                print(f"  csc {rn}, {o}({reg})")
            elif arch == "morello":
                print(f"  str {rn}, [{reg}, #{o}]")
        print(".endm\n")
        assert save_offset != -1

        print(f".macro LOAD_REGISTERS")
        for n in range(1, self._nregs):
            rn = f"c{n}"
            o = n * self._bytes
            if rn == reg or rn == ra:
                print(f'  // Skip {rn}{f" == {reg}" if ra != reg else ""}')
                continue
            if arch == "riscv64":
                print(f"  clc {rn}, {o}({reg})")
            elif arch == "morello":
                print(f"  ldr {rn}, [{reg}, {o}]")
        print(".endm\n")

    def CapabilityBits(self):
        if arch == "riscv64":
            for n in range(len(PERMS)):
                print(f".equ CAP_B_{PERMS[n]}, {n}")
                print(f".equ CAP_{PERMS[n]}, 1 << CAP_B_{PERMS[n]}")


arch = sys.argv[1]

if len(sys.argv) == 2 or sys.argv[2] == "asm":
    m = Model_asm(arch)
    if sys.argv[3] == "macros":
        m.ClearRegisters()
        # m.SaveRegisters("ct0")
    if sys.argv[3] == "defines":
        m.CapabilityBits()
elif sys.argv[2] == "c":
    m = Model_c(arch)
    if sys.argv[3] == "macros":
        print("#ifndef __BB_H")
        print("#define __BB_H")
        m.ClearRegisters()
        # m.SaveRegisters("ct0")
    if sys.argv[3] == "defines":
        print("#ifndef __BB_DEFINES_H")
        print("#define __BB_DEFINES_H")
        m.CapabilityBits()
    print("#endif")
else:
    print('Please choose between "asm" and "c"')
    sys.exit(1)
