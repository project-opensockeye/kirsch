from pathlib import Path
from build.helpers import *

from build.lib.models import *

from  build.lib.cheriette_ninja_writer import CherietteNinjaWriter

"""
This file provides base artifacts to base more advanced artifacts off of.

Every individual, generated artifact is described by its own class and file in 
artifacts/."""


class Artifact:
    """
    A base artifact.

    Every artifact goes through three steps:
     1. Initialization. The artifact is initialized without context,
     so completely independent of architecture and build environment.
     2. Contextualization. The artifact is contextualized with the
     architectural target and build context (rule parameters, etc.).
     3. Finally, the artifact is added to the Ninja build file.
    """

    def get_artifact_name(self) -> str:
        """Returns the context-free name of the artifact"""
        return self.name

    def get_rule_name(self) -> str:
        """Returns the name of the rule that generates this artifact"""
        return self.target.arch + "_" + self.name

    def __init__(
        self,
        name: str,
    ):
        self.name: str = name
        self.object_file_prefix: str = ""

    def add_context(
        self,
        target: Target,
        context: Context,
    ):
        """Adds a context to this artifact"""

        self.target: Target = target

        self.object_file_prefix = self.object_file_prefix + context.name + "/"

    #
    def contextualize(
        self,
        target: Target,
        contexts: List[Context] = [],
    ):
        """Puts this artifact in a compilation context"""

        self.target = target

        for context in contexts:
            self.add_context(target, context=context)

        self.object_file_prefix = "/".join(
            sorted(self.object_file_prefix.split("/"))
        )

        self.object_file_prefix = self.target.arch + "/" + self.object_file_prefix

    def add(self, ninja: CherietteNinjaWriter):
        """Add this artifact to the Ninja build file"""
        return


class ArtifactCompiled(Artifact):
    """
    A compiled Artifact is made up of source files compiled by "cc".
    The arguments for "cc" are provided through the compilation context.
    """

    def __init__(
        self,
        name: str,
        source_dirs: List[Path] = [],
        source_files: List[Path] = [],
        source_dirs_arch_dependent: Dict[str, List[Path]] = {},
        source_files_arch_dependent: Dict[str, List[Path]] = {},
    ):
        """
        Initializes the compiled artifact.

        _dirs parameters are searched for source files.
        _arch_dependent dicts are keyed by the target architecture the
        artifact is built for. The target architecture is provided in the
        contextualization step.
        """

        self.source_dirs = source_dirs
        self.source_files = source_files
        self.source_dirs_arch_dependent = source_dirs_arch_dependent
        self.source_files_arch_dependent = source_files_arch_dependent
        self.object_file_prefix = ""
        self.includes: List[Path] = []
        self.cflags: List[str] = []
        self.ldflags: List[str] = []
        super().__init__(
            name=name,
        )

    def contextualize(
        self,
        target: Target,
        contexts: List[Context],
    ):
        """
        Contextualizes the compiled artifact.

        Compiles a list of .c and .S file that are part of the artifact.
        """

        super().contextualize(target, contexts)

        # Add architecture dependent source directories matching selected architecture target
        self.source_dirs = self.source_dirs + list(
            self.source_dirs_arch_dependent.get(self.target.arch, [])
        )

        # Turn directory strings into paths
        self.source_dirs = [Path(x) for x in self.source_dirs]

        # Find all .S/.c files in source directories
        # This is a list of lists
        self.source_dirs_files = [x.rglob(src_pattern) for x in self.source_dirs]

        # Flatten source_dirs_files
        self.source_dirs_files = [x for xs in self.source_dirs_files for x in xs]

        # Add architecture dependent sources matching selected architecture target
        self.source_files = self.source_files + [
            x for x in list(self.source_files_arch_dependent.get(self.target.arch, []))
        ]

        # Turn source strings into paths
        self.source_files = [Path(x) for x in self.source_files]

        # Add files from directories to sources
        self.source_files = self.source_files + self.source_dirs_files

    def add_context(
        self,
        target: Target,
        context: Context,
    ):
        """
        Adds a context to the compiled artifact.

        Compiles a list of the cc and ld flags, plus include folders from the contexts provided.
        """

        super().add_context(target, context)

        self.cflags = (
            self.cflags + context.cflags + context.cflags_arch.get(target.arch, [])
        )
        self.includes = (
            self.includes
            + context.includes
            + context.includes_arch.get(target.arch, [])
        )

    def add(
        self,
        ninja: CherietteNinjaWriter,
        implicit_dependencies: List[str] = [],
    ):
        """
        Adds the compiled artifact to the Ninja build file.

        Adds a single
        """

        super().add(ninja=ninja)

        ninja.rule_compiler_cheriette(
            name=self.get_rule_name(),
            cc=self.target.cc,
            includes=self.includes,
            cflags=self.cflags,
            description="Compile $out with " + self.name + " context",
        )

        (object_files, source_object_file_map) = srcs_to_objs(
            self.source_files, self.object_file_prefix
        )
        self.object_files = object_files

        for src, obj in source_object_file_map:
            ninja.build(
                outputs=str(obj),
                rule="cc-" + self.get_rule_name(),
                inputs=[str(src)],
                implicit=implicit_dependencies,
                # order_only_deps=[obj.name + ".d"],
                variables=[
                    ("arch", self.target.arch),
                    ("depfile", str(obj) + ".d"),
                ],
            )


class ArtifactElf(ArtifactCompiled):
    """
    An ELF artifact is first compiled (see ArtifactCompiled),
    and then linked using a linker "ld".
    """

    def get_artifact_name(self) -> str:
        return super().get_artifact_name() + "_" + self.target.arch + ".elf"

    def __init__(
        self,
        name: str,
        source_dirs: List[Path] = [],
        source_files: List[Path] = [],
        source_dirs_arch_dependent: Dict[str, List[Path]] = {},
        source_files_arch_dependent: Dict[str, List[Path]] = {},
    ):
        super().__init__(
            name=name,
            source_dirs=source_dirs,
            source_files=source_files,
            source_dirs_arch_dependent=source_dirs_arch_dependent,
            source_files_arch_dependent=source_files_arch_dependent,
        )

    def add_context(
        self,
        target: Target,
        context: Context,
    ):
        """
        Adds a context to the ELF artifact.

        Compiles a list of the ld flags from the context provided.
        """

        super().add_context(target, context)

        self.ldflags = (
            self.ldflags + context.ldflags + context.ldflags_arch.get(target.arch, [])
        )

    def add(
        self,
        ninja: CherietteNinjaWriter,
        implicit_dependencies: List[str] = [],
        libraries: List[str] = [],
    ):
        """
        Adds this ELF artifact to the Ninja build file.

        Accepts a list of static library artifacts (see
        ArtifactStaticLib) to link into the ELF.

        Adds a linker rule for this artifact,
        and a build statement to link the object's source files. (See ArtifactCompiled)
        """

        super().add(ninja=ninja, implicit_dependencies=implicit_dependencies)

        ninja.rule_linker(
            name=self.get_rule_name(),
            ld=self.target.ld,
            ldflags=self.ldflags,
            description="Link $out with " + self.name + " context",
        )

        ninja.build_linker(
            outputs=self.get_artifact_name(),
            rule="ld-" + self.get_rule_name(),
            inputs=list(map(str, self.object_files)),
            libraries=list(map(str, libraries)),
        )


class ArtifactStaticLib(ArtifactCompiled):
    """
    A static library artifact is a collection of compiled objects (see
    ArtifactCompiled), grouped together by the "ar" archive tool.
    """

    def get_artifact_name(self) -> str:
        return "lib" + super().get_artifact_name() + "_" + self.target.arch + ".a"

    def get_artifact(self) -> str:
        return self.target.arch + "/lib/" + self.get_artifact_name()

    def __init__(
        self,
        name: str,
        source_dirs: List[Path] = [],
        source_files: List[Path] = [],
        source_dirs_arch_dependent: Dict[str, List[Path]] = {},
        source_files_arch_dependent: Dict[str, List[Path]] = {},
    ):
        super().__init__(
            name=name,
            source_dirs=source_dirs,
            source_files=source_files,
            source_dirs_arch_dependent=source_dirs_arch_dependent,
            source_files_arch_dependent=source_files_arch_dependent,
        )

    def add(
        self,
        ninja: CherietteNinjaWriter,
        implicit_dependencies: List[str] = [],
    ):
        """
        Adds the static library artifact to the Ninja build file.

        Adds a "ar" rule for this artifact, and a corresponding build statement.
        """

        super().add(ninja=ninja, implicit_dependencies=implicit_dependencies)

        ninja.rule_flagged_command(
            name="ar-" + self.get_rule_name(),
            command=str(self.target.ar),
            flags=["r", "$out", "$in"],
            description="Archive $in > $out with context " + self.name,
        )

        ninja.build(
            outputs=self.get_artifact(),
            rule="ar-" + self.get_rule_name(),
            inputs=list(map(str, self.object_files)),
        )


class ArtifactBinary(Artifact):
    """
    A binary artifact is a special:
     - It transforms a loader ELF artifact that is generated independently.
     - It copies a second, payload ELF file into the loader ELF file.
     - It then strips the loader ELF with payload of its ELF-ness (
     preserving the payload ELF), such that the first byte in the resulting
     loader binary is the first byte of the first instruction to execute.
    """

    def get_intermediate_artifact_name(self) -> str:
        return super().get_artifact_name() + "_" + self.target.arch + ".elf"

    def get_artifact_name(self) -> str:
        return super().get_artifact_name() + "_" + self.target.arch + ".bin"

    def __init__(
        self,
        name: str,
        source_file: Path,
        payload_file: Path,
        payload_target_section_name: str,
    ):
        super().__init__(name=name)
        self.object_file_prefix = ""
        self.source_file: Path = source_file
        self.payload_file: Path = payload_file
        self.payload_target_section_name: str = payload_target_section_name
        self.objcopyflags: List[str] = []

    def contextualize(
        self,
        target: Target,
        contexts: List[Context],
    ):

        super().contextualize(target, contexts)

    def add_context(
        self,
        target: Target,
        context: Context,
    ):

        super().add_context(target, context)

        self.objcopyflags = (
            self.objcopyflags
            + context.objcopyflags
            + context.objcopyflags_arch.get(target.arch, [])
        )

    def add(
        self,
        ninja: CherietteNinjaWriter,
        implicit_dependencies: List[str] = [],
    ):
        """
        Adds the binary artifact to the Ninja build file.

        Adds two objcopy rules:
         1. Removes the ELF header
         2. Copies the payload ELF into the loader ELF.

        It then adds two build statements with dependencies such that
        the payload ELF is first copied into the loader ELF, and then
        the ELF header removed from the loader+payload ELF.
        """
        super().add(ninja=ninja)

        ninja.rule_objcopy(
            name=self.get_rule_name() + "-remove-elf-header",
            objcopy=str(self.target.objcopy),
            objcopyflags=[
                "--output-target binary",
                "$in",
                "$out",
            ],
            description="Remove .elf header from $in > $out",
        )

        ninja.rule_objcopy(
            name=self.get_rule_name() + "-copy-into-section",
            objcopy=str(self.target.objcopy),
            objcopyflags=[
                "--update-section $elf_section=$section_file",
                "$in",
                "$out",
            ],
            description="Copy file $section_file into section $elf_section of $out",
        )

        ninja.build(
            outputs=self.get_intermediate_artifact_name(),
            rule="objcopy-" + self.get_rule_name() + "-copy-into-section",
            inputs=str(self.source_file),
            variables=[
                ("elf_section", self.payload_target_section_name),
                ("section_file", str(self.payload_file)),
            ],
            implicit=str(self.payload_file),
        )

        ninja.build(
            outputs=self.get_artifact_name(),
            rule="objcopy-" + self.get_rule_name() + "-remove-elf-header",
            inputs=self.get_intermediate_artifact_name(),
        )
