from pathlib import Path
from build.helpers import *


class Architecture:
    """
    Essentially a strong typedef/enum.
    """

    def __init__(
        self,
        arch: str,
    ):
        self.arch: str = arch


class Target(Architecture):
    """
    Every build target has an architecture string associated with it
    and paths to build tools.
    """

    def __init__(
        self,
        arch: str,
        platform: str,
        cc: Path,
        ld: Path,
        ar: Path,
        objcopy: Path,
        morello_workspace_path: Path,
    ):
        self.platform: str = platform
        self.cc: Path = cc
        self.ld: Path = ld
        self.ar: Path = ar
        self.objcopy: Path = objcopy
        self.morello_workspace_path: Path = morello_workspace_path
        super().__init__(arch=arch)


class Context:
    """
    A context defines various parameters that can be passed to, and used by, 
    artifacts.
    """

    def __init__(
        self,
        name: str,
        includes: List[Path] = [],
        includes_arch: Dict[str, List[Path]] = {},
        cflags: List[str] = [],
        cflags_arch: Dict[str, List[str]] = {},
        ldflags: List[str] = [],
        ldflags_arch: Dict[str, List[str]] = {},
        arflags: List[str] = [],
        arflags_arch: Dict[str, List[str]] = {},
        objcopyflags: List[str] = [],
        objcopyflags_arch: Dict[str, List[str]] = {},
    ):
        self.name = name
        self.includes = includes
        self.includes_arch = includes_arch
        self.cflags = cflags
        self.cflags_arch = cflags_arch
        self.ldflags = ldflags
        self.ldflags_arch = ldflags_arch
        self.arflags = arflags
        self.arflags_arch = arflags_arch
        self.objcopyflags = objcopyflags
        self.objcopyflags_arch = objcopyflags_arch

    def __str__(self) -> str:
        """
        Helper to make sorting a list of contexts by name easier.
        """
        return self.name

    def includes_only(self):
        """
        Returns a context that has the same includes, but no tooling flags.
        """
        return Context(
            name=self.name,
            includes=self.includes,
            includes_arch=self.includes_arch,
        )

    def flags_only(self):
        """
        Returns a context that has the same tooling flags, but no includes.
        """
        return Context(
            name=self.name,
            cflags=self.cflags,
            cflags_arch=self.cflags_arch,
            ldflags=self.ldflags,
            ldflags_arch=self.ldflags_arch,
            arflags=self.arflags,
            arflags_arch=self.arflags_arch,
        )
