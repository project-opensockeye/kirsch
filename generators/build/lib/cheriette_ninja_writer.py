import re
import textwrap
from io import TextIOWrapper
from typing import Dict, List, Match, Optional, Tuple, Union
from build.lib.ninja_syntax import Writer as NinjaWriter
from build.helpers import *


class CherietteNinjaWriter(NinjaWriter):
    """
    Extends the default NinjaWriter, improving readability of the final
    Ninja file, and adds some Cheriette helpers.
    """

    def __init__(self, statement_prefix: str, output: TextIOWrapper, width: int = 78):
        self.variable_prefix = ""
        self.rule_prefix = statement_prefix
        super().__init__(output, width)

    def variable(
        self,
        key: str,
        value: Optional[Union[bool, int, float, str, List[str]]],
        indent: int = 0,
    ):
        super().variable(key=self.variable_prefix + key, value=value, indent=indent)

    def rule(
        self,
        name: str,
        command: str,
        description: Optional[str] = None,
        depfile: Optional[str] = None,
        generator: bool = False,
        pool: Optional[str] = None,
        restat: bool = False,
        rspfile: Optional[str] = None,
        rspfile_content: Optional[str] = None,
        deps: Optional[Union[str, List[str]]] = None,
    ):
        """
        More legible version of the same function from ninja_syntax.py
        """
        super().newline()
        super().rule(
            name=self.rule_prefix + name,
            command=command,
            description=description,
            depfile=depfile,
            generator=generator,
            pool=pool,
            restat=restat,
            rspfile=rspfile,
            rspfile_content=rspfile_content,
            deps=deps,
        )

    def build(
        self,
        outputs: Union[str, List[str]],
        rule: str,
        inputs: Optional[Union[str, List[str]]] = None,
        implicit: Optional[Union[str, List[str]]] = None,
        order_only: Optional[Union[str, List[str]]] = None,
        variables: Optional[
            Union[
                List[Tuple[str, Optional[Union[str, List[str]]]]],
                Dict[str, Optional[Union[str, List[str]]]],
            ]
        ] = None,
        implicit_outputs: Optional[Union[str, List[str]]] = None,
        pool: Optional[str] = None,
        dyndep: Optional[str] = None,
    ) -> List[str]:
        """
        More legible version of the same function from ninja_syntax.py
        """
        super().newline()
        return super().build(
            outputs=outputs,
            rule=self.rule_prefix + rule,
            inputs=inputs,
            implicit=implicit,
            order_only=order_only,
            variables=variables,
            implicit_outputs=implicit_outputs,
            pool=pool,
            dyndep=dyndep,
        )

    def rule_flagged_command(
        self,
        name: str,
        command: str | Path,
        flags: List[str],
        description: Optional[str] = None,
        depfile: Optional[str] = None,
        generator: bool = False,
        pool: Optional[str] = None,
        restat: bool = False,
        rspfile: Optional[str] = None,
        rspfile_content: Optional[str] = None,
        deps: Optional[Union[str, List[str]]] = None,
    ):
        """
        Rule that appends flags to a command string
        """
        command_flagged: str = " ".join([str(command), " ".join(flags)])
        self.rule(
            name=name,
            command=command_flagged,
            description=description,
            depfile=depfile,
            generator=generator,
            pool=pool,
            restat=restat,
            rspfile=rspfile,
            rspfile_content=rspfile_content,
            deps=deps,
        )

    def rule_compiler(
        self,
        name: str,
        cc: Path,
        includes: List[Path],
        cflags: List[str],
        description: Optional[str] = None,
        depfile: Optional[str] = "$out.d",
        generator: bool = False,
        pool: Optional[str] = None,
        restat: bool = False,
        rspfile: Optional[str] = None,
        rspfile_content: Optional[str] = None,
        deps: Optional[Union[str, List[str]]] = None,
    ):
        """
        Rule for a compiler invocation to turn source file(s) into object file(s).

        Prefixes the name of the generated rule with "cc". 

        Provides arguments for includes and custom flags.
        """
        name = "cc-" + name
        for include in includes:
            cflags.append("-I " + "$src/" + str(include))

        self.rule_flagged_command(
            name=name,
            command=cc,
            flags=cflags,
            description=description,
            depfile=depfile,
            generator=generator,
            pool=pool,
            restat=restat,
            rspfile=rspfile,
            rspfile_content=rspfile_content,
            deps=deps,
        )

    def rule_linker(
        self,
        name: str,
        ld: Path,
        ldflags: List[str],
        description: Optional[str] = None,
        depfile: Optional[str] = "$out.d",
        generator: bool = False,
        pool: Optional[str] = None,
        restat: bool = False,
        rspfile: Optional[str] = None,
        rspfile_content: Optional[str] = None,
        deps: Optional[Union[str, List[str]]] = None,
    ):
        """
        Rule for a linker invocation to turn object file(s) into ELF binaries.

        Prefixes the name of the generated rule with "ld". 

        Provides arguments for linker flags.
        """
        name = "ld-" + name
        if depfile is not None:
            ldflags.append("--dependency-file=" + depfile)
        ldflags.append("$library_flags")

        self.rule_flagged_command(
            name=name,
            command=ld,
            flags=ldflags,
            description=description,
            depfile=depfile,
            generator=generator,
            pool=pool,
            restat=restat,
            rspfile=rspfile,
            rspfile_content=rspfile_content,
            deps=deps,
        )

    def rule_objcopy(
        self,
        name: str,
        objcopy: str,
        objcopyflags: List[str],
        description: Optional[str] = None,
        depfile: Optional[str] = None,
        generator: bool = False,
        pool: Optional[str] = None,
        restat: bool = False,
        rspfile: Optional[str] = None,
        rspfile_content: Optional[str] = None,
        deps: Optional[Union[str, List[str]]] = None,
    ):
        """
        Rule for a objcopy invocation to turn ELF binaries into more complex
        ELF binaries or binary binaries.

        Prefixes the name of the generated rule with "objcopy". 

        Provides arguments for objcopy flags.
        """
        name = "objcopy-" + name

        self.rule_flagged_command(
            name=name,
            command=objcopy,
            flags=objcopyflags,
            description=description,
            depfile=depfile,
            generator=generator,
            pool=pool,
            restat=restat,
            rspfile=rspfile,
            rspfile_content=rspfile_content,
            deps=deps,
        )

    def rule_compiler_cheriette(
        self,
        name: str,
        cc: Path,
        includes: List[Path],
        cflags: List[str],
        description: Optional[str] = None,
        depfile: Optional[str] = "$out.d",
        generator: bool = False,
        pool: Optional[str] = None,
        restat: bool = False,
        rspfile: Optional[str] = None,
        rspfile_content: Optional[str] = None,
        deps: Optional[Union[str, List[str]]] = None,
    ):
        """
        Rule for a Cheriette compiler invocation.
        Appends default flags to the cflags passed.

        Provides arguments for includes and flags.
        """

        cflags = [
            "-falign-functions=16",
            "-falign-loops=16",
            "-mstack-alignment=16",
            "-nostdinc",
            "-nostdlib",
            "-nodefaultlibs",
            "-ffreestanding",
            "-Wcheri",
            "-Werror",
            "-pedantic",
            # stdlib headers use the _Nonnull from Clang extensions
            "-Wno-nullability-extension",
            # (Hopefully) disable deletion of NULL pointer checks
            # Yes, clang/GCC do that.
            "-fno-delete-null-pointer-checks",
            # Trap on integer overflow.
            # clang/GCC consider integer overflows undefined behaviour.
            "-ftrapv",
            # "-fwrapv",
        ] + cflags

        self.rule_compiler(
            name=name,
            cc=cc,
            includes=includes,
            cflags=cflags,
            description=description,
            depfile=depfile,
            generator=generator,
            pool=pool,
            restat=restat,
            rspfile=rspfile,
            rspfile_content=rspfile_content,
            deps=deps,
        )

    def build_linker(
        self,
        outputs: Union[str, List[str]],
        rule: str,
        libraries: List[str] = [],
        inputs: Optional[Union[str, List[str]]] = None,
        implicit_arg: Optional[Union[str, List[str]]] = [],
        order_only: Optional[Union[str, List[str]]] = None,
        variables: Optional[
            Union[
                List[Tuple[str, Optional[Union[str, List[str]]]]],
                Dict[str, Optional[Union[str, List[str]]]],
            ]
        ] = None,
        implicit_outputs: Optional[Union[str, List[str]]] = None,
        pool: Optional[str] = None,
        dyndep: Optional[str] = None,
    ) -> List[str]:
        """
        Build statement for default cheriette linker invocation.
        
        Supports passing a list of libraries to link against.
        """
        
        implicit = []

        if type(implicit_arg) is str:
            implicit = [implicit_arg]

        library_flags = ""
        for library in libraries:
            library_flags += (
                "-l "
                + Path(library).stem.replace(
                    "lib",
                    "",
                    1,
                )
                + " "
            )
            implicit.append(library)

        if variables is None:
            variables = []

        variables.append(("library_flags", library_flags))

        return self.build(
            outputs=outputs,
            rule=rule,
            inputs=inputs,
            implicit=implicit,
            order_only=order_only,
            variables=variables,
            implicit_outputs=implicit_outputs,
            pool=pool,
            dyndep=dyndep,
        )
