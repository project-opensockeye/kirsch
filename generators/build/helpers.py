import sys
from pathlib import Path
from typing import Dict, List, Match, Optional, Tuple, Union

src_pattern = "*.[Sc]"


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def srcs_to_objs(srcs, prefix_path):
    src_obj_pairs = []
    objs = []
    for src in srcs:
        obj = prefix_path / src.parents[0] / Path(src.name + ".o")
        src_obj_pairs.append(("$src" / src, obj))
        objs.append(obj)

    return (objs, src_obj_pairs)


def abs_srcs(srcs_rel: List[Path]):
    srcs_abs = []

    for src_rel in srcs_rel:
        src_rel = Path("$src") / src_rel
        srcs_abs.append(src_rel)

    return srcs_abs
