from pathlib import Path
from build.helpers import *

from build.lib.artifacts import *

from build.lib.cheriette_ninja_writer import CherietteNinjaWriter


def add(
    ninja: CherietteNinjaWriter,
    target: Target,
    contexts: Dict[str, Context],
    cheri_enabled: bool,
):
    """
    Adds the standalone printf library artifact.

    This artifact can be compiled with optional CHERI support.
    """

    cheri_ctx = None
    name_suffix = ""

    if cheri_enabled:
        cheri_ctx = contexts["cheri"]
        name_suffix = "_cheri"
    else:
        cheri_ctx = contexts["noncheri"]
        name_suffix = "_noncheri"

    printf_standalone_path = Path("lib/kirsch/printf_standalone")

    printf_standalone = ArtifactStaticLib(
        name="printf_standalone" + name_suffix,
        source_files=[
            printf_standalone_path / "printf.c",
        ],
        source_files_arch_dependent={
            "morello": [
                printf_standalone_path / "arch/morello/putchar.c",
            ],
            "riscv64": [
                printf_standalone_path / "arch/riscv64/putchar.c",
            ],
        },
    )

    printf_standalone.contextualize(
        target=target,
        contexts=[
            contexts["printf_standalone"],
            contexts["common"],
            contexts["arch"],
            cheri_ctx,
            contexts["freebsd"].includes_only(),
        ],
    )

    printf_standalone.add(ninja, implicit_dependencies=[])

    return printf_standalone
