from pathlib import Path
from build.helpers import *

from build.lib.artifacts import *

from build.lib.cheriette_ninja_writer import CherietteNinjaWriter


def add(
    ninja: CherietteNinjaWriter,
    target: Target,
    contexts: Dict[str, Context],
):
    """
    Adds the Cheri Interface library artifact.
    """

    cheri_ctx = None
    name_suffix = ""

    cheri_ctx = contexts["cheri"]
    name_suffix = "_cheri"

    cheri_interface_path = Path("lib/kirsch/cheri_interface")

    cheri_interface = ArtifactStaticLib(
        name="cheri_interface" + name_suffix,
        source_files_arch_dependent={
            "morello": [
                cheri_interface_path / "arch/morello/cheri_interface.c",
            ],
            "riscv64": [
                cheri_interface_path / "arch/riscv64/cheri_interface.c",
            ],
        },
    )

    cheri_interface.contextualize(
        target=target,
        contexts=[
            contexts["cheri_interface"],
            contexts["common"],
            contexts["arch"],
            cheri_ctx,
            contexts["freebsd"].includes_only(),
        ],
    )

    cheri_interface.add(
        ninja,
        implicit_dependencies=[
            "$src/include/arch/" + target.arch + "/cheri/cheri_defines.S",
            "$src/include/arch/" + target.arch + "/cheri/cheri_helpers.S",
            "$src/include/arch/" + target.arch + "/cheri/cheri_defines.h",
            "$src/include/arch/" + target.arch + "/cheri/cheri_helpers.h",
        ],
    )

    return cheri_interface
