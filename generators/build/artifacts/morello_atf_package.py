from pathlib import Path
from build.helpers import *

from build.lib.artifacts import *


from build.lib.cheriette_ninja_writer import CherietteNinjaWriter
class ArtifactMorelloATF(Artifact):
    """
    A special artifact describing the build process of
    the ARM Morello artifacts.
    """

    def __init__(
        self,
        name,
        loader_bin,
    ):
        super().__init__(
            name=name,
        )
        self.loader_bin = loader_bin

    def add(self, ninja):
        super().add(
            ninja,
        )
        ninja.rule_flagged_command(
            name="morello-arm-tf-package-" + self.target.arch,
            command="bash",
            description="Package Morello loader into ARM TF (This may take a while...)",
            flags=[
                "$src/generators/morello_atf_package.sh",
                # Target (fvp/soc)
                self.target.platform,
                # Morello workspace path
                str(self.target.morello_workspace_path),
                # Input user binary path
                "$src/build/$in",
                # Output fip path
                "$output_fip_path",
                # Output bl1 path
                "$output_bl1_path",
            ],
        )

        atf_outputs = [
            self.target.arch
            + "/cheriette_morello_atf_"
            + self.target.platform
            + "_fip.bin",
            self.target.arch
            + "/cheriette_morello_atf_"
            + self.target.platform
            + "_bl1.bin",
        ]
        ninja.build(
            outputs=atf_outputs,
            rule="morello-arm-tf-package-" + self.target.arch,
            inputs=self.loader_bin,
            variables=[
                (
                    "output_fip_path",
                    self.target.arch
                    + "/cheriette_morello_atf_"
                    + self.target.platform
                    + "_fip.bin",
                ),
                (
                    "output_bl1_path",
                    self.target.arch
                    + "/cheriette_morello_atf_"
                    + self.target.platform
                    + "_bl1.bin",
                ),
            ],
        )
        ninja.build(
            outputs=self.get_artifact_name(),
            inputs=atf_outputs,
            rule="depend",
        )


def add(
    ninja: CherietteNinjaWriter,
    target: Target,
    loader_bin: str,
):

    morello_atf_package = ArtifactMorelloATF(
        name="morello_atf",
        loader_bin=loader_bin,
    )

    morello_atf_package.contextualize(target=target)

    morello_atf_package.add(ninja)

    return morello_atf_package
