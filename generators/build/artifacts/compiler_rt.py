from pathlib import Path
from build.helpers import *

from build.lib.artifacts import *

from build.lib.cheriette_ninja_writer import CherietteNinjaWriter

def add(
    ninja: CherietteNinjaWriter,
    target: Target,
    contexts: Dict[str, Context],
    cheri_enabled: bool,
):
    """
    Adds the LLVM Compiler Runtime library artifact.

    This artifact can be built with optional CHERI support.
    """

    cheri_context = None
    name_suffix = ""
    if cheri_enabled:
        cheri_context = contexts["cheri"]
        name_suffix = "_cheri"
    else:
        cheri_context = contexts["noncheri"]
        name_suffix = "_noncheri"

    compiler_rt = ArtifactStaticLib(
        name="compiler_rt" + name_suffix,
        source_files=[
            Path("lib/foreign/freebsd/contrib/subrepo-cheri-compiler-rt")
            / "lib"
            / "builtins"
            / file
            for file in [
                # "absvdi2.c",
                # "absvsi2.c",
                # "absvti2.c",
                "adddf3.c",
                # "addsf3.c",
                # "addtf3.c",
                # "addvdi3.c",
                # "addvsi3.c",
                # "addvti3.c",
                # "apple_versioning.c",
                # "ashldi3.c",
                # "ashlti3.c",
                # "ashrdi3.c",
                # "ashrti3.c",
                # ACTUAL ERR
                # "atomic.c", # error: redefinition of 'atomic_thread_fence'
                # "atomic_flag_clear.c",
                # "atomic_flag_clear_explicit.c",
                # "atomic_flag_test_and_set.c",
                # "atomic_flag_test_and_set_explicit.c",
                # "atomic_signal_fence.c",
                # "atomic_thread_fence.c",
                # ICN
                # "bswapdi2.c",
                # "bswapsi2.c",
                # "clear_cache.c",
                # "clzdi2.c",
                # "clzsi2.c",
                # "clzti2.c",
                # "cmpdi2.c",
                # "cmpti2.c",
                "comparedf2.c",
                # "comparesf2.c",
                # "comparetf2.c",
                # "cpu_model.c",
                # "ctzdi2.c",
                # "ctzsi2.c",
                # "ctzti2.c",
                # "divdc3.c",
                "divdf3.c",
                # "divdi3.c",
                # "divmoddi4.c",
                # "divmodsi4.c",
                # "divmodti4.c",
                # "divsc3.c",
                # "divsf3.c",
                # "divsi3.c",
                # "divtf3.c",
                # "divti3.c",
                # "divxc3.c",
                # "emutls.c",
                # ACTUAL ERR
                # "enable_execute_stack.c", # Copy barrelfish
                # INC
                # "eprintf.c",
                # "extenddftf2.c",
                # "extendhfsf2.c",
                # "extendhftf2.c",
                # "extendsfdf2.c",
                # "extendsftf2.c",
                # "ffsdi2.c",
                # "ffssi2.c",
                # "ffsti2.c",
                # "fixdfdi.c",
                "fixdfsi.c",
                # "fixdfti.c",
                # "fixsfdi.c",
                # "fixsfsi.c",
                # "fixsfti.c",
                # "fixtfdi.c",
                # "fixtfsi.c",
                # "fixtfti.c",
                "fixunsdfdi.c",
                "fixunsdfsi.c",
                # "fixunsdfti.c",
                # "fixunssfdi.c",
                # "fixunssfsi.c",
                # "fixunssfti.c",
                # "fixunstfdi.c",
                # "fixunstfsi.c",
                # "fixunstfti.c",
                # "fixunsxfdi.c",
                # "fixunsxfsi.c",
                # "fixunsxfti.c",
                # "fixxfdi.c",
                # "fixxfti.c",
                # "floatdidf.c",
                # "floatdisf.c",
                # "floatditf.c",
                # "floatdixf.c",
                "floatsidf.c",
                # "floatsisf.c",
                # "floatsitf.c",
                # "floattidf.c",
                # "floattisf.c",
                # "floattitf.c",
                # "floattixf.c",
                "floatundidf.c",
                # "floatundisf.c",
                # "floatunditf.c",
                # "floatundixf.c",
                "floatunsidf.c",
                # "floatunsisf.c",
                # "floatunsitf.c",
                # "floatuntidf.c",
                # "floatuntisf.c",
                # "floatuntitf.c",
                # "floatuntixf.c",
                "fp_mode.c",
                # ERR
                # "gcc_personality_v0.c", # fatal error: 'unwind.h' file not found #include <unwind.h>
                # INC
                # "int_util.c",
                # "lshrdi3.c",
                # "lshrti3.c",
                # "moddi3.c",
                # "modsi3.c",
                # "modti3.c",
                # "muldc3.c",
                "muldf3.c",
                # "muldi3.c",
                # "mulodi4.c",
                # "mulosi4.c",
                # "muloti4.c",
                # "mulsc3.c",
                # "mulsf3.c",
                # "multf3.c",
                # "multi3.c",
                # "mulvdi3.c",
                # "mulvsi3.c",
                # "mulvti3.c",
                # "mulxc3.c",
                # "negdf2.c",
                # "negdi2.c",
                # "negsf2.c",
                # "negti2.c",
                # "negvdi2.c",
                # "negvsi2.c",
                # "negvti2.c",
                # "os_version_check.c",
                # "paritydi2.c",
                # "paritysi2.c",
                # "parityti2.c",
                # "popcountdi2.c",
                # "popcountsi2.c",
                # "popcountti2.c",
                # "powidf2.c",
                # "powisf2.c",
                # "powitf2.c",
                # "powixf2.c",
                "subdf3.c",
                # "subsf3.c",
                # "subtf3.c",
                # "subvdi3.c",
                # "subvsi3.c",
                # "subvti3.c",
                # "trampoline_setup.c",
                # "truncdfbf2.c",
                # "truncdfhf2.c",
                # "truncdfsf2.c",
                # "truncsfbf2.c",
                # "truncsfhf2.c",
                # "trunctfdf2.c",
                # "trunctfhf2.c",
                # "trunctfsf2.c",
                # "ucmpdi2.c",
                # "ucmpti2.c",
                # "udivdi3.c",
                # "udivmoddi4.c",
                # "udivmodsi4.c",
                # "udivmodti4.c",
                # "udivsi3.c",
                # "udivti3.c",
                # "umoddi3.c",
                # "umodsi3.c",
                # "umodti3.c",
            ]
        ],
        source_files_arch_dependent={
            "riscv64": [],
            "morello": [
                Path("lib/foreign/freebsd/contrib/subrepo-cheri-compiler-rt/")
                / "lib"
                / "builtins"
                / "aarch64"
                / "chkstk.S",
                Path("lib/foreign/freebsd/contrib/subrepo-cheri-compiler-rt/")
                / "lib"
                / "builtins/divtc3.c",
                Path("lib/foreign/freebsd/contrib/subrepo-cheri-compiler-rt/")
                / "lib"
                / "builtins/multc3.c",
            ],
        },
    )

    compiler_rt.contextualize(
        target=target,
        contexts=[
            contexts["compiler_rt"],
            contexts["common"],
            contexts["arch"],
            cheri_context,
            contexts["freebsd"].includes_only(),
        ],
    )
    compiler_rt.add(ninja, implicit_dependencies=[])

    return compiler_rt
