from pathlib import Path
from build.helpers import *

from build.lib.artifacts import *

from build.lib.cheriette_ninja_writer import CherietteNinjaWriter

def add(
    ninja: CherietteNinjaWriter,
    target: Target,
    contexts: Dict[str, Context],
):
    """
    Adds the libc dependency msun library artifact.
    """

    msun_arch_dict = {"morello": "aarch64", "riscv64": "riscv"}
    msun_arch = msun_arch_dict[target.arch]
    msun_dir = Path("lib/foreign/freebsd/msun")
    msun = ArtifactStaticLib(
        name="msun",
        source_files=[
            msun_dir / Path("bsdsrc") / file
            for file in [
                "b_tgamma.c",
            ]
        ]
        + [
            msun_dir / Path(msun_arch) / file
            for file in [
                "fenv.c",
            ]
        ]
        + [
            msun_dir / Path("src") / file
            for file in [
                "e_acos.c",
                "e_acosf.c",
                "e_acosh.c",
                "e_acoshf.c",
                "e_asin.c",
                "e_asinf.c",
                "e_atan2.c",
                "e_atan2f.c",
                "e_atanh.c",
                "e_atanhf.c",
                "e_cosh.c",
                "e_coshf.c",
                "e_exp.c",
                "e_expf.c",
                "e_fmod.c",
                "e_fmodf.c",
                "e_gamma.c",
                "e_gamma_r.c",
                "e_gammaf.c",
                "e_gammaf_r.c",
                "e_hypot.c",
                "e_hypotf.c",
                "e_j0.c",
                "e_j0f.c",
                "e_j1.c",
                "e_j1f.c",
                "e_jn.c",
                "e_jnf.c",
                "e_lgamma.c",
                "e_lgamma_r.c",
                "e_lgammaf.c",
                "e_lgammaf_r.c",
                "e_log.c",
                "e_log10.c",
                "e_log10f.c",
                "e_log2.c",
                "e_log2f.c",
                "e_logf.c",
                "e_pow.c",
                "e_powf.c",
                "e_rem_pio2.c",
                "e_rem_pio2f.c",
                "e_remainder.c",
                "e_remainderf.c",
                "e_scalb.c",
                "e_scalbf.c",
                "e_sinh.c",
                "e_sinhf.c",
                "e_sqrt.c",
                "e_sqrtf.c",
                "k_cos.c",
                "k_cosf.c",
                "k_exp.c",
                "k_expf.c",
                "k_rem_pio2.c",
                "k_sin.c",
                "k_sinf.c",
                "k_tan.c",
                "k_tanf.c",
                "s_asinh.c",
                "s_asinhf.c",
                "s_atan.c",
                "s_atanf.c",
                "s_carg.c",
                "s_cargf.c",
                "s_cargl.c",
                "s_cbrt.c",
                "s_cbrtf.c",
                "s_ceil.c",
                "s_ceilf.c",
                "s_clog.c",
                "s_clogf.c",
                "s_copysign.c",
                "s_copysignf.c",
                "s_cos.c",
                "s_cosf.c",
                "s_csqrt.c",
                "s_csqrtf.c",
                "s_erf.c",
                "s_erff.c",
                "s_exp2.c",
                "s_exp2f.c",
                "s_expm1.c",
                "s_expm1f.c",
                "s_fabsf.c",
                "s_fdim.c",
                "s_finite.c",
                "s_finitef.c",
                "s_floor.c",
                "s_floorf.c",
                "s_fma.c",
                "s_fmaf.c",
                "s_fmax.c",
                "s_fmaxf.c",
                "s_fmin.c",
                "s_fminf.c",
                "s_frexp.c",
                "s_frexpf.c",
                "s_ilogb.c",
                "s_ilogbf.c",
                "s_ilogbl.c",
                "s_isfinite.c",
                "s_isnan.c",
                "s_isnormal.c",
                "s_llrint.c",
                "s_llrintf.c",
                "s_llround.c",
                "s_llroundf.c",
                "s_llroundl.c",
                "s_log1p.c",
                "s_log1pf.c",
                "s_logb.c",
                "s_logbf.c",
                "s_lrint.c",
                "s_lrintf.c",
                "s_lround.c",
                "s_lroundf.c",
                "s_lroundl.c",
                "s_modff.c",
                "s_nan.c",
                "s_nearbyint.c",
                "s_nextafter.c",
                "s_nextafterf.c",
                "s_nexttowardf.c",
                "s_remquo.c",
                "s_remquof.c",
                "s_rint.c",
                "s_rintf.c",
                "s_round.c",
                "s_roundf.c",
                "s_scalbln.c",
                "s_scalbn.c",
                "s_scalbnf.c",
                "s_signbit.c",
                "s_signgam.c",
                "s_significand.c",
                "s_significandf.c",
                "s_sin.c",
                "s_sincos.c",
                "s_sincosf.c",
                "s_sinf.c",
                "s_tan.c",
                "s_tanf.c",
                "s_tanh.c",
                "s_tanhf.c",
                "s_tgammaf.c",
                "s_trunc.c",
                "s_truncf.c",
                "w_cabs.c",
                "w_cabsf.c",
                "w_drem.c",
                "w_dremf.c",
            ]
        ]
        + [
            # IEEE-754 2008 and ISO/IEC TS 18661-4 half-cycle trignometric functions
            msun_dir / Path("src") / file
            for file in [
                "s_cospi.c",
                "s_cospif.c",
                "s_sinpi.c",
                "s_sinpif.c",
                "s_tanpi.c",
                "s_tanpif.c",
            ]
        ]
        + [
            # C99 long double functions
            msun_dir / Path("src") / file
            for file in [
                "s_copysignl.c",
                "s_fabsl.c",
                "s_llrintl.c",
                "s_lrintl.c",
                "s_modfl.c",
            ]
        ]
        + [
            # If long double != double use these; otherwise, we alias the double versions.
            msun_dir / Path("ld128") / file
            for file in [
                "b_tgammal.c",
                "e_lgammal_r.c",
                "e_powl.c",
                "invtrig.c",
                "k_cosl.c",
                "k_sinl.c",
                "k_tanl.c",
                "s_cexpl.c",
                "s_cospil.c",
                "s_erfl.c",
                "s_expl.c",
                "s_exp2l.c",
                "s_logl.c",
                "s_nanl.c",
                "s_sinpil.c",
                "s_tanpil.c",
            ]
        ]
        + [
            msun_dir / Path("src") / file
            for file in [
                "catrigl.c",
                "e_acoshl.c",
                "e_acosl.c",
                "e_asinl.c",
                "e_atan2l.c",
                "e_atanhl.c",
                "e_coshl.c",
                "e_fmodl.c",
                "e_hypotl.c",
                "e_lgammal.c",
                "e_remainderl.c",
                "e_sinhl.c",
                "e_sqrtl.c",
                "s_asinhl.c",
                "s_atanl.c",
                "s_cbrtl.c",
                "s_ceill.c",
                "s_cosl.c",
                "s_cprojl.c",
                "s_csqrtl.c",
                "s_clogl.c",
                "s_floorl.c",
                "s_fmal.c",
                "s_fmaxl.c",
                "s_fminl.c",
                "s_frexpl.c",
                "s_logbl.c",
                "s_nextafterl.c",
                "s_nexttoward.c",
                "s_remquol.c",
                "s_rintl.c",
                "s_roundl.c",
                "s_scalbnl.c",
                "s_sinl.c",
                "s_sincosl.c",
                "s_tanhl.c",
                "s_tanl.c",
                "s_truncl.c",
                "w_cabsl.c",
            ]
        ]
        + [
            # C99 complex functions
            msun_dir / Path("src") / file
            for file in [
                "catrig.c",
                "catrigf.c",
                "s_ccosh.c",
                "s_ccoshf.c",
                "s_cexp.c",
                "s_cexpf.c",
                "s_cimag.c",
                "s_cimagf.c",
                "s_cimagl.c",
                "s_conj.c",
                "s_conjf.c",
                "s_conjl.c",
                "s_cpow.c",
                "s_cpowf.c",
                "s_cpowl.c",
                "s_cproj.c",
                "s_cprojf.c",
                "s_creal.c",
                "s_crealf.c",
                "s_creall.c",
                "s_csinh.c",
                "s_csinhf.c",
                "s_ctanh.c",
                "s_ctanhf.c",
            ]
        ],
    )

    msun.contextualize(
        target=target,
        contexts=[
            contexts["msun"],
            contexts["common"],
            contexts["arch"],
            contexts["cheri"],
            contexts["libc"].includes_only(),
            contexts["freebsd"].includes_only(),
        ],
    )

    msun.add(ninja, implicit_dependencies=[])

    return msun
