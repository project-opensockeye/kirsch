from pathlib import Path
from build.helpers import *

from build.lib.artifacts import *

from build.lib.cheriette_ninja_writer import CherietteNinjaWriter

def add(
    ninja: CherietteNinjaWriter,
    target: Target,
    contexts: Dict[str, Context],
):
    """
    Adds the libc gdtoa library artifact.
    """

    base_path = Path("lib/foreign/freebsd/contrib/gdtoa")
    gdtoa = ArtifactStaticLib(
        name="gdtoa",
        source_files=[
            Path(base_path) / file
            for file in [
                "dmisc.c",
                "dtoa.c",
                "g_Qfmt.c",
                "g__fmt.c",
                "g_ddfmt.c",
                "g_dfmt.c",
                "g_ffmt.c",
                "g_xLfmt.c",
                "g_xfmt.c",
                "gdtoa.c",
                "gethex.c",
                "gmisc.c",
                "hd_init.c",
                "hexnan.c",
                "misc.c",
                "smisc.c",
                "strtoIQ.c",
                "strtoId.c",
                "strtoIdd.c",
                "strtoIf.c",
                "strtoIg.c",
                "strtoIx.c",
                "strtoIxL.c",
                "strtod.c",
                "strtodI.c",
                "strtodg.c",
                "strtof.c",
                "strtopQ.c",
                "strtopd.c",
                "strtopdd.c",
                "strtopf.c",
                "strtopx.c",
                "strtopxL.c",
                "strtorQ.c",
                "strtord.c",
                "strtordd.c",
                "strtorf.c",
                "strtorx.c",
                "strtorxL.c",
                "sum.c",
                "ulp.c",
            ]
        ],
    )

    gdtoa.contextualize(
        target=target,
        contexts=[
            contexts["gdtoa"],
            contexts["common"].includes_only(),
            contexts["arch"].includes_only(),
        ],
    )
