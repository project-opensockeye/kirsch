from pathlib import Path
from build.helpers import *

from build.lib.artifacts import *

from build.lib.cheriette_ninja_writer import CherietteNinjaWriter

def add(
    ninja: CherietteNinjaWriter,
    target: Target,
    contexts: Dict[str, Context],
):
    """
    Adds the libc library artifact.
    """

    base_path = Path("lib/foreign/freebsd/libc")

    libc = ArtifactStaticLib(
        name="libc",
        source_dirs_arch_dependent={
            "morello": [],
            "riscv64": [
                base_path / "riscv/softfloat",
            ],
        },
        source_files_arch_dependent={
            "morello": [
                base_path / "aarch64" / "gen" / "fabs.S",
                base_path / "aarch64" / "gen" / "flt_rounds.c",
                base_path / "aarch64" / "gen" / "fpsetmask.c",
                base_path / "aarch64" / "gen" / "fpgetmask.c",
            ],
            "riscv64": [
                base_path / "riscv" / "gen" / "fabs.S",
                base_path / "riscv" / "gen" / "flt_rounds.c",
                base_path / "riscv" / "gen" / "_setjmp.S",
                base_path / "riscv" / "gen" / "setjmp.S",
            ],
        },
        source_dirs=[
            base_path / x
            for x in [
                "stdio",
                "string",
                "sys",
                "gdtoa",
                "libc_sys",
                "vis",
                # "libc_stdtime",
                "tzcode",
                # "inet",
                # "libc_inet",
                "nls",
                # "libc_nls",
            ]
        ],
        source_files=[
            # We don't include libc malloc, we list files explicitly
            Path(base_path) / "stdlib" / file
            for file in [
                "a64l.c",
                "getopt.c",
                "heapsort_b.c",
                "atoll.c",
                "qsort_r.c",
                "atof.c",
                "strtoul.c",
                "llabs.c",
                "heapsort.c",
                "radixsort.c",
                "strtoq.c",
                "qsort_r_compat.c",
                "system.c",
                "ldiv.c",
                "strtoull.c",
                "exit.c",
                "twalk.c",
                "cxa_thread_atexit.c",
                "reallocarray.c",
                "merge.c",
                "hsearch_r.c",
                "strtold.c",
                "atol.c",
                "atoi.c",
                "abort.c",
                "hcreate_r.c",
                "qsort_s.c",
                "ptsname.c",
                "labs.c",
                "set_constraint_handler_s.c",
                "_Exit.c",
                "strfmon.c",
                "abs.c",
                "tfind.c",
                "rand.c",
                "hdestroy_r.c",
                "atexit.c",
                "insque.c",
                "reallocf.c",
                "tsearch.c",
                "lldiv.c",
                "cxa_thread_atexit_impl.c",
                "bsearch.c",
                "realpath.c",
                "strtoimax.c",
                "bsearch_b.c",
                "tdelete.c",
                "mergesort_b.c",
                "remque.c",
                "hcreate.c",
                "lsearch.c",
                "strtoumax.c",
                "l64a.c",
                "random.c",
                "imaxdiv.c",
                "strtouq.c",
                "quick_exit.c",
                "div.c",
                "getsubopt.c",
                "qsort.c",
                "getenv.c",
                "getopt_long.c",
                "strtol.c",
                "strtonum.c",
                "imaxabs.c",
                "strtoll.c",
            ]
        ]
        + [
            Path(base_path) / "gen" / file
            for file in [
                "assert.c",
                "err.c",
                "errlst.c",
                "arc4random_uniform.c",
                "isinf.c",
            ]
        ]
        + [
            Path(base_path) / ".." / "contrib" / "libc-vis" / file
            for file in ["vis.c", "unvis.c"]
        ]
        + [
            Path(base_path) / "locale" / file
            for file in [
                "ascii.c",
                "big5.c",
                "btowc.c",
                "collate.c",
                "collcmp.c",
                "euc.c",
                "fix_grouping.c",
                "gb18030.c",
                "gb2312.c",
                "gbk.c",
                "ctype.c",
                "isctype.c",
                "iswctype.c",
                "ldpart.c",
                "lmessages.c",
                "lmonetary.c",
                "lnumeric.c",
                "localeconv.c",
                "mblen.c",
                "mbrlen.c",
                "mbrtowc.c",
                "mbsinit.c",
                "mbsnrtowcs.c",
                "mbsrtowcs.c",
                "mbtowc.c",
                "mbstowcs.c",
                "mskanji.c",
                "nextwctype.c",
                "nl_langinfo.c",
                "nomacros.c",
                "none.c",
                "rpmatch.c",
                "rune.c",
                "runetype.c",
                "setlocale.c",
                "setrunelocale.c",
                "table.c",
                "tolower.c",
                "toupper.c",
                "utf8.c",
                "wcrtomb.c",
                "wcsnrtombs.c",
                "wcsrtombs.c",
                "wcsftime.c",
                "wcstof.c",
                "wcstod.c",
                "wcstoimax.c",
                "wcstol.c",
                "wcstold.c",
                "wcstoll.c",
                "wcstombs.c",
                "wcstoul.c",
                "wcstoull.c",
                "wcstoumax.c",
                "wctob.c",
                "wctomb.c",
                "wctrans.c",
                "wctype.c",
                "wcwidth.c",
                "xlocale.c",
            ]
        ],
    )

    libc.contextualize(
        target,
        contexts=[
            contexts["libc"],
            contexts["common"],
            contexts["arch"],
            contexts["cheri"],
            contexts["msun"].includes_only(),
            contexts["gdtoa"].includes_only(),
            contexts["freebsd"].includes_only(),
        ],
    )

    libc.add(ninja, implicit_dependencies=[])
