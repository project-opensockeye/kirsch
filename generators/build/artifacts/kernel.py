from pathlib import Path
from build.helpers import *

from build.lib.artifacts import *

from build.lib.cheriette_ninja_writer import CherietteNinjaWriter


class kernelKernelArtifact(ArtifactElf):
    """
    A kernel artifact describing a Kirsch kernel ELF.
    """

    def __init__(
        self,
        name: str,
        source_dirs: List[Path] = [],
        source_files: List[Path] = [],
        source_dirs_arch_dependent: Dict[str, List[Path]] = {},
        source_files_arch_dependent: Dict[str, List[Path]] = {},
    ):
        super().__init__(
            name=name,
            source_dirs=source_dirs,
            source_files=source_files,
            source_dirs_arch_dependent=source_dirs_arch_dependent,
            source_files_arch_dependent=source_files_arch_dependent,
        )

    def add(
        self,
        ninja: CherietteNinjaWriter,
        libraries: List[str] = [],
    ):
        super().add(
            ninja,
            implicit_dependencies=[
                "$src/include/arch/" + self.target.arch + "/cheri/cheri_defines.S",
                "$src/include/arch/" + self.target.arch + "/cheri/cheri_helpers.S",
                "$src/include/arch/" + self.target.arch + "/cheri/cheri_defines.h",
                "$src/include/arch/" + self.target.arch + "/cheri/cheri_helpers.h",
                "$src/mackerel/include/arch/"
                + self.target.arch
                + "/mackerels/armv8_dev.h",
                "$src/mackerel/include/arch/"
                + self.target.arch
                + "/mackerels/gicd_dev.h",
                "$src/mackerel/include/arch/"
                + self.target.arch
                + "/mackerels/gicr_dev.h",
            ],
            libraries=libraries,
        )

        ninja.rule_flagged_command(
            name=self.get_rule_name() + "-mackerel",
            command="/tools/mackerel2",
            flags=["--input-file", "$in", "--output-file", "$out"],
            description="Mackerel: compile $in to $out",
        )

        ninja.rule_flagged_command(
            name=self.get_rule_name() + "-cheri_helpers",
            command="python3 $src/generators/cheri_helpers.py",
            flags=[self.target.arch, "$lang", "$type", "> $out"],
            description="Generate CHERI helper header $out",
        )

        ninja.build(
            outputs="$src/mackerel/include/arch/"
            + self.target.arch
            + "/mackerels/armv8_dev.h",
            inputs=["$src/mackerel/devices/armv8.dev"],
            rule=self.get_rule_name() + "-mackerel",
            variables=[
                ("filename", "armv8_dev.h"),
            ],
        )
        ninja.build(
            outputs="$src/mackerel/include/arch/"
            + self.target.arch
            + "/mackerels/gicd_dev.h",
            inputs=["$src/mackerel/devices/gicd.dev"],
            rule=self.get_rule_name() + "-mackerel",
            variables=[
                ("filename", "gicd_dev.h"),
            ],
        )
        ninja.build(
            outputs="$src/mackerel/include/arch/"
            + self.target.arch
            + "/mackerels/gicr_dev.h",
            inputs=["$src/mackerel/devices/gicr.dev"],
            rule=self.get_rule_name() + "-mackerel",
            variables=[
                ("filename", "gicr_dev.h"),
            ],
        )

        ninja.build(
            outputs="$src/include/arch/" + self.target.arch + "/cheri/cheri_helpers.h",
            rule=self.get_rule_name() + "-cheri_helpers",
            variables=[
                ("lang", "c"),
                ("type", "macros"),
            ],
        )
        ninja.build(
            outputs="$src/include/arch/" + self.target.arch + "/cheri/cheri_helpers.S",
            rule=self.get_rule_name() + "-cheri_helpers",
            variables=[
                ("lang", "asm"),
                ("type", "macros"),
            ],
        )
        ninja.build(
            outputs="$src/include/arch/" + self.target.arch + "/cheri/cheri_defines.h",
            rule=self.get_rule_name() + "-cheri_helpers",
            variables=[
                ("lang", "c"),
                ("type", "defines"),
            ],
        )
        ninja.build(
            outputs="$src/include/arch/" + self.target.arch + "/cheri/cheri_defines.S",
            rule=self.get_rule_name() + "-cheri_helpers",
            variables=[
                ("lang", "asm"),
                ("type", "defines"),
            ],
        )


def add(
    ninja: CherietteNinjaWriter,
    target: Target,
    contexts: Dict[str, Context],
    printf_standalone,
    system_interface,
    riscv_sail_emulator_interface,
    compiler_rt,
    cheri_interface,
    demo_enable=False,
):
    """
    Adds the Kirsch kernel ELF artifact.
    """

    kernel = kernelKernelArtifact(
        name="kernel",
        # List of directories to include *all* .S/.c files from.
        source_dirs=[
            Path("kernel/common"),
        ],
        # List of individual source files
        source_files=[],
        # List of individual, architecture dependent files to include
        source_files_arch_dependent={
            "morello": [
                # "drivers/pl011.c",
                Path("usr/morello_test_processes.S"),
            ],
            "riscv64": [
                Path("usr/riscv64_test_processes.S"),
            ],
        },
        # List of architecture dependent directories to include *all*
        # .S/.c files from.
        source_dirs_arch_dependent={
            "morello": [
                Path("kernel/arch/morello"),
            ],
            "riscv64": [
                Path("kernel/arch/riscv64"),
            ],
        },
    )

    default_contexts= [
        contexts["common"],
        contexts["arch"],
        contexts["cheri"],
        contexts["kernel"],
        contexts["printf_standalone"].includes_only(),
        contexts["system_interface"].includes_only(),
        contexts["riscv_sail_emulator_interface"].includes_only(),
        contexts["cheri_interface"].includes_only(),
        contexts["freebsd"].includes_only(),
        # contexts["gdtoa"].includes_only(),
        # contexts["libc"].includes_only(),
        contexts["usr"].includes_only(),
    ]

    pass_contexts = default_contexts

    if demo_enable:
        pass_contexts.append(contexts["demo"])


    kernel.contextualize(
        target,
        contexts=pass_contexts
    )

    kernel.add(
        ninja,
        libraries=[
            printf_standalone.get_artifact(),
            compiler_rt.get_artifact(),
            riscv_sail_emulator_interface.get_artifact(),
            system_interface.get_artifact(),
            cheri_interface.get_artifact(),
        ],
    )

    return kernel
