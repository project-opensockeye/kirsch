from pathlib import Path
from build.helpers import *

from build.lib.artifacts import *

from build.lib.cheriette_ninja_writer import CherietteNinjaWriter


def add(
    ninja: CherietteNinjaWriter,
    target: Target,
    contexts: Dict[str, Context],
    cheri_enabled: bool,
):
    """
    Adds the RISC-V SAIL emulator interface library.

    This artifact can be compiled with optional CHERI support.

    Currently not in use. RISC-V development has moved to QEMU.
    """

    cheri_ctx = None
    name_suffix = ""

    if cheri_enabled:
        cheri_ctx = contexts["cheri"]
        name_suffix = "_cheri"
    else:
        cheri_ctx = contexts["noncheri"]
        name_suffix = "_noncheri"

    riscv_sail_emulator_interface_path = Path(
        "lib/kirsch/riscv_sail_emulator_interface"
    )

    srcs = [
        riscv_sail_emulator_interface_path / "shared_interface.c",
    ]
    if cheri_enabled:
        srcs += [
            riscv_sail_emulator_interface_path / "cheri/emulator_interface.S",
            riscv_sail_emulator_interface_path / "cheri/interface_functions.c",
        ]
    else:
        srcs += [
            riscv_sail_emulator_interface_path / "noncheri/emulator_interface.S",
            riscv_sail_emulator_interface_path / "noncheri/interface_functions.c",
        ]

    riscv_sail_emulator_interface = ArtifactStaticLib(
        name="riscv_sail_emulator_interface" + name_suffix,
        source_files_arch_dependent={"riscv64": srcs},
    )

    riscv_sail_emulator_interface.contextualize(
        target=target,
        contexts=[
            contexts["riscv_sail_emulator_interface"],
            contexts["common"],
            contexts["arch"],
            cheri_ctx,
            contexts["freebsd"].includes_only(),
            contexts["cheri_interface"].includes_only(),
        ],
    )

    riscv_sail_emulator_interface.add(
        ninja,
        implicit_dependencies=[
            "$src/include/arch/" + target.arch + "/cheri/cheri_defines.S",
            "$src/include/arch/" + target.arch + "/cheri/cheri_helpers.S",
            "$src/include/arch/" + target.arch + "/cheri/cheri_defines.h",
            "$src/include/arch/" + target.arch + "/cheri/cheri_helpers.h",
        ],
    )

    return riscv_sail_emulator_interface
