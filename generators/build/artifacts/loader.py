from pathlib import Path
from build.helpers import *

from build.lib.artifacts import *

from build.lib.cheriette_ninja_writer import CherietteNinjaWriter


def add(
    ninja: CherietteNinjaWriter,
    target: Target,
    contexts: Dict[str,Context],
    printf_standalone_noncheri,
    riscv_sail_emulator_interface_noncheri,
    compiler_rt_noncheri,
    system_interface_noncheri,
    demo_enable=False,
):
    """
    Adds the Kirsch loader ELF and Binary artifacts.
    """

    loader_path = Path("loader/")

    loader = ArtifactElf(
        name="loader_empty",
        source_files=[
            loader_path / "elf_loader.c",
        ],
        source_files_arch_dependent={
            "morello": [
                loader_path / "arch/morello/loader.S",
            ],
            "riscv64": [
                loader_path / "arch/riscv64/loader.S",
            ],
        },
    )

    default_contexts = [ contexts["common"],
            contexts["arch"],
            contexts["loader"],
            contexts["noncheri"],
            contexts["printf_standalone"].includes_only(),
            contexts["system_interface"].includes_only(),
            contexts["freebsd"].includes_only(),
        ]

    pass_contexts = default_contexts

    if demo_enable:
        pass_contexts.append(contexts["loader-linker-demo"])
    else:
        pass_contexts.append(contexts["loader-linker-default"])

    loader.contextualize(
        target,
        contexts=pass_contexts,
    )

    loader.add(
        ninja,
        implicit_dependencies=[],
        libraries=[
            printf_standalone_noncheri.get_artifact(),
            compiler_rt_noncheri.get_artifact(),
            riscv_sail_emulator_interface_noncheri.get_artifact(),
            system_interface_noncheri.get_artifact(),
        ],
    )

    return loader


def add_bin(
    ninja,
    target,
    contexts,
    loader_elf,
    kernel,
):

    loader_bin = ArtifactBinary(
        name="loader",
        source_file=loader_elf,
        payload_file=kernel,
        payload_target_section_name=".kernel_elf",
    )

    loader_bin.contextualize(
        target,
        contexts=[
            contexts["loader"],
        ],
    )

    loader_bin.add(ninja, implicit_dependencies=[])

    return loader_bin
