from pathlib import Path
from build.helpers import *

from build.lib.artifacts import *

from build.lib.cheriette_ninja_writer import CherietteNinjaWriter


def add(
    ninja: CherietteNinjaWriter,
    target: Target,
    contexts: Dict[str, Context],
    cheri_enabled: bool,
):
    """
    Adds the Kirsch system interface library artifact.

    This artifact can be compiled with optional CHERI support.
    """

    cheri_ctx = None
    name_suffix = ""

    if cheri_enabled:
        cheri_ctx = contexts["cheri"]
        name_suffix = "_cheri"
    else:
        cheri_ctx = contexts["noncheri"]
        name_suffix = "_noncheri"

    system_interface_path = Path("lib/kirsch/system_interface/arch")

    system_interface = ArtifactStaticLib(
        name="system_interface" + name_suffix,
        source_files_arch_dependent={
            "morello": [
                system_interface_path / "morello/system_interface.c",
            ],
            "riscv64": [
                system_interface_path / "riscv64/system_interface.c",
            ],
        },
    )

    system_interface.contextualize(
        target=target,
        contexts=[
            contexts["system_interface"],
            contexts["common"],
            contexts["arch"],
            contexts["riscv_sail_emulator_interface"].includes_only(),
            cheri_ctx,
            contexts["freebsd"].includes_only(),
        ],
    )

    implicit_dependencies = []

    if cheri_enabled:
        implicit_dependencies = [
            "$src/include/arch/" + target.arch + "/cheri/cheri_defines.S",
            "$src/include/arch/" + target.arch + "/cheri/cheri_helpers.S",
            "$src/include/arch/" + target.arch + "/cheri/cheri_defines.h",
            "$src/include/arch/" + target.arch + "/cheri/cheri_helpers.h",
        ]

    system_interface.add(
        ninja,
        implicit_dependencies=implicit_dependencies,
    )

    return system_interface
