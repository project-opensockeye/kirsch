from pathlib import Path
from build.helpers import *

from build.lib.models import *


def get() -> Dict[str, Context]:
    libc_base_path = Path("lib/foreign/freebsd/libc/")
    contexts: Dict[str, Context] = {
        "demo": Context(
            name="demo",
            cflags=[
                "-D DEMO_ENABLE",
            ],
        ),
        "common": Context(
            name="common",
            includes=[
                Path("include/common"),
                Path("compat/include/freebsd/include"),
                Path("compat/include/freebsd/sys"),
                Path("compat/include/freebsd/sys/sys"),
                Path("lib/foreign/freebsd/libc/include"),
            ],
            cflags_arch={
                "morello": [
                    "-mstrict-align",
                ],
            },
            cflags=[
                "-fno-builtin",
            ],
        ),
        "cheri": Context(
            name="cheri",
            cflags_arch={
                "riscv64": [
                    "-DRISCV_XLEN=64",
                    "-target riscv64-none-elf",
                    "-march=rv64imafdcxcheri",
                    "-mabi=l64pc128d",
                    "-mxcheri-norvc ",
                    "-mno-relax ",
                ],
                "morello": [
                    "-target aarch64-none-elf",
                    "-march=morello",
                    "-mabi=purecap",
                    "-fwritable-strings",
                ],
            },
        ),
        "noncheri": Context(
            name="noncheri",
            cflags_arch={
                "riscv64": [
                    "-target riscv64-none-elf",
                    "-mno-relax ",
                ],
                "morello": [
                    "-target aarch64-none-elf",
                ],
            },
        ),
        "arch": Context(
            name="arch",
            includes_arch={
                "riscv64": [
                    Path("include/arch/riscv64"),
                ],
                "morello": [
                    Path("include/arch/morello"),
                ],
            },
        ),
        "freebsd": Context(
            name="freebsd",
            includes=[
                Path("compat/freebsd/sys"),
            ],
            includes_arch={
                "riscv64": [
                    Path("compat/include/freebsd/sys/riscv/"),
                    Path("compat/include/freebsd/sys/riscv/machine/"),
                ],
                "morello": [
                    Path("compat/include/freebsd/sys/arm64/"),
                    Path("compat/include/freebsd/sys/arm64/machine/"),
                ],
            },
        ),
        "kernel": Context(
            name="kernel",
            includes=[
                Path("include/kernel"),
                Path("mackerel/include"),
            ],
            includes_arch={
                "riscv64": [
                    Path("mackerel/include/arch/riscv64/"),
                ],
                "morello": [
                    Path("mackerel/include/arch/morello/"),
                ],
            },
            cflags=[
                "-c",
                "-MT $out -MD -MP -MF $out.d",
                "-ffreestanding ",
                "-g",
                "-O0",
                "-fPIE",
                "-fPIC",
                "-o $out $in",
                "-Wall",
                "-fno-builtin",
                "-fno-unwind-tables",
                "-nostdinc",
                "-Wno-overlength-strings",
            ],
            ldflags=[
                "-o $out $in",
                "--no-gc-sections",
                "-pie",
                "--apply-dynamic-relocs",
                "--nostdlib",
                "--error-limit=0",
            ],
            ldflags_arch={
                "morello": [
                    # ELF caprelocs are only implemented for the AArch64/Morello
                    # target.
                    "--local-caprelocs=elf",
                    "-L " + str(Path("$src/build/morello/lib")),
                    "-T$src/link/morello/link-kernel.ld",
                ],
                "riscv64": [
                    "-L " + str(Path("$src/build/riscv64/lib")),
                    "-T$src/link/riscv64/link-kernel.ld",
                ],
            },
        ),
        "usr": Context(
            name="usr",
            includes=[
                Path("include/usr"),
            ],
            includes_arch={
                "morello": [
                    Path("include/morello"),
                ],
            },
            cflags=[
                "-c",
                "-MD -MF $out.d",
                "-g",
                "-O0 ",
                "-Wall",
            ],
        ),
        "gdtoa": Context(
            name="gdtoa",
            includes=[
                Path("lib/foreign/freebsd/contrib/gdtoa"),
            ],
        ),
        "libc": Context(
            name="libc",
            cflags=[
                "-c",
                "-MT $out -MD -MP -MF $out.d",
                "-g",
                "-O0 ",
                "-fPIC",
                "-o $out $in",
                "-Wmissing-prototypes",
                "-Wmissing-declarations",
                "-Wimplicit-function-declaration",
                "-Wno-visibility",
                "-Wno-pointer-sign",
                "-Werror",
                "-nostdinc",
                "-DFLOAT128",
            ],
            includes=[
                Path("lib/foreign/freebsd/libc/include"),
                Path("lib/foreign/freebsd/libc/locale"),
                Path("lib/foreign/freebsd/libc/softfloat"),
                Path("lib/foreign/freebsd/contrib/libc-vis"),
                Path("lib/foreign/freebsd/libc"),
            ],
            includes_arch={
                "riscv64": [
                    libc_base_path / "riscv",
                    libc_base_path / "riscv" / "softfloat",
                ],
                "morello": [
                    libc_base_path / "aarch64",
                    libc_base_path / "arm" / "softfloat",
                ],
            },
        ),
        "compiler_rt": Context(
            name="compiler_rt",
            cflags=[
                "-std=c11",
                # Complains about "_Complex" numbers being an extension in C99
                # Even though we compile with -std=c11
                "-no-pedantic",
                "-c",
                "-MT $out -MD -MP -MF $out.d",
                "-g",
                "-O0",
                "-fPIC",
                "-fPIE",
                "-fno-builtin",
                "-fvisibility=hidden",
                "-fomit-frame-pointer",
                "-o $out $in",
                "-Wno-missing-prototypes",
                "-Wno-missing-declarations",
                "-Wno-strict-prototypes",
                "-Wno-old-style-definition",
                "-Wno-redundant-decls",
                "-D__FreeBSD__",
                "-D__ELF__",
            ],
            ldflags=[
                "-static",
                "--no-undefined-version",
                "-o $out $in",
            ],
            ldflags_arch={
                "morello": [
                    "-T$src/link/morello/link-compiler_rt.ld",
                ]
            },
        ),
        "libc_vis": Context(
            name="libc_vis",
            includes=[
                Path("lib/foreign/freebsd/contrib/libc_vis"),
            ],
        ),
        "printf_standalone": Context(
            name="printf_standalone",
            includes=[
                Path("lib/kirsch/printf_standalone"),
                Path("mackerel/include"),
            ],
            cflags=[
                "-c",
                "-MT $out -MD -MP -MF $out.d",
                "-O3",
                "-g",
                "-o $out $in",
                "-ffreestanding",
                "-fPIC",
                "-fPIE",
                "-Wall",
            ],
            arflags=[
                "r",
                "$out",
                "$in",
            ],
        ),
        "system_interface": Context(
            name="system_interface",
            includes=[
                Path("lib/kirsch/system_interface"),
            ],
            cflags=[
                "-c",
                "-MT $out -MD -MP -MF $out.d",
                "-O3",
                "-g",
                "-o $out $in",
                "-ffreestanding",
                "-fPIC",
                "-fPIE",
                "-Wall",
            ],
            arflags=[
                "r",
                "$out",
                "$in",
            ],
        ),
        "cheri_interface": Context(
            name="cheri_interface",
            includes=[
                Path("lib/kirsch/cheri_interface"),
            ],
            includes_arch={
                "morello": [
                    Path("lib/kirsch/cheri_interface/arch/morello"),
                ],
                "riscv64": [
                    Path("lib/kirsch/cheri_interface/arch/riscv64"),
                ],
            },
            cflags=[
                "-c",
                "-MT $out -MD -MP -MF $out.d",
                "-O3",
                "-g",
                "-o $out $in",
                "-ffreestanding",
                "-fPIC",
                "-fPIE",
                "-Wall",
            ],
            arflags=[
                "r",
                "$out",
                "$in",
            ],
        ),
        "riscv_sail_emulator_interface": Context(
            name="riscv_sail_emulator_interface",
            includes=[
                Path("lib/kirsch/riscv_sail_emulator_interface"),
            ],
            cflags=[
                "-c",
                "-MT $out -MD -MP -MF $out.d",
                "-o $out $in",
                "-ffreestanding",
                "-fPIC",
                "-fPIE",
                "-Wall",
            ],
            arflags=[
                "r",
                "$out",
                "$in",
            ],
        ),
        "msun": Context(
            name="msun",
            cflags=[
                "-c",
                "-MT $out -MD -MP -MF $out.d",
                "-ffreestanding ",
                "-g",
                "-O0 ",
                "-fPIC",
                "-std=c99",
                "-o $out $in",
                "-Wmissing-prototypes",
                "-Wmissing-declarations",
                "-Wimplicit-function-declaration",
                "-Wno-visibility",
                "-Wno-pointer-sign",
                "-Wno-ignored-pragmas",
                "-Werror",
                "-DPIC",
                "-nostdinc",
            ],
            ldflags=[
                "-static",
                "--no-undefined-version",
                "-o $out $in",
            ],
            includes=[
                Path("lib/foreign/freebsd/msun") / "src",
            ],
            includes_arch={
                "morello": [
                    Path("lib/foreign/freebsd/msun/") / "aarch64",
                    Path("lib/foreign/freebsd/msun/") / "ld80",
                ],
                "riscv64": [
                    Path("lib/foreign/freebsd/msun/") / "riscv",
                    Path("lib/foreign/freebsd/msun/") / "ld128",
                ],
            },
        ),
        "loader": Context(
            name="loader",
            includes=[
                Path("loader/"),
            ],
            includes_arch={
                "morello": [
                    Path("loader/arch/morello"),
                ],
                "riscv64": [
                    Path("loader/arch/riscv64"),
                ],
            },
            cflags=[
                "-c",
                "-MT $out -MD -MP -MF $out.d",
                "-ffreestanding",
                "-g",
                "-O3",
                "-fPIC",
                "-fPIE",
                "-o $out $in",
                "-Wall",
                "-falign-functions=16",
                "-falign-loops=16",
                "-mstack-alignment=16",
                "-nostdinc",
                "-nostdlib",
                "-nodefaultlibs",
                "-Wcheri",
                "-Werror",
                "-Wstrict-prototypes",
                "-pedantic",
                # stdlib headers use the _Nonnull from Clang extensions
                "-Wno-nullability-extension",
                # (Hopefully) disable deletion of NULL pointer checks
                # Yes, clang/GCC do that.
                "-fno-delete-null-pointer-checks",
                # Trap on integer overflow.
                # clang/GCC consider integer overflows undefined behaviour.
                "-ftrapv",
                # "-fwrapv",
            ],
            ldflags=[
                "-o $out $in",
                "--no-gc-sections",
                # PIE would be a sensible choice,
                # but relocation fails:
                # "R_AARCH64_ABS64 cannot be used against symbol _elf_start"
                # ld.lld then advises to "recompile with -fPIC", which we
                # already do.
                # "-pie",
                # "--apply-dynamic-relocs",
                "--nostdlib",
                "--error-limit=0",
            ],
            ldflags_arch={
                "morello": [
                    "-L " + str(Path("$src/build/morello/lib")),
                ],
                "riscv64": [
                    "-L " + str(Path("$src/build/riscv64/lib")),
                ],
            },
        ),
        "loader-linker-default": Context(
            name="loader-linker-default",
            ldflags_arch={
                "morello": [
                    "-L " + str(Path("$src/build/morello/lib")),
                    "-T$src/link/morello/link-loader.ld",
                ],
                "riscv64": [
                    "-L " + str(Path("$src/build/riscv64/lib")),
                    "-T$src/link/riscv64/link-loader.ld",
                ],
            },
        ),
        "loader-linker-demo": Context(
            name="loader-linker-demo",
            ldflags_arch={
                "morello": [
                    "-L " + str(Path("$src/build/morello/lib")),
                    "-T$src/link/morello/link-loader-demo.ld",
                ],
                "riscv64": [
                    "-L " + str(Path("$src/build/riscv64/lib")),
                    "-T$src/link/riscv64/link-loader.ld",
                ],
            },
        ),
    }
    return contexts
